import math
import numpy as np
from .math import *
from .opengl import *
from .objmesh import ObjMesh

# Map of color names to RGB values
COLORS = {
    'black': np.array([0.0, 0.0, 0.0]),
    'red': np.array([1.0, 0.0, 0.0]),
    'green': np.array([0.0, 1.0, 0.0]),
    'blue': np.array([0.0, 0.0, 1.0]),
    'purple': np.array([0.44, 0.15, 0.76]),
    'yellow': np.array([1.00, 1.00, 0.00]),
    'grey': np.array([0.39, 0.39, 0.39]),
    'white': np.array([1.0, 1.0, 1.0])
}

# List of color names, sorted alphabetically
COLOR_NAMES = sorted(list(COLORS.keys()))


class Entity:
    def __init__(self):
        # World position
        # Note: for most entities, the position is at floor level
        self.pos = None

        # Direction/orientation angle in radians
        self.dir = None

        # Radius for bounding circle/cylinder
        self.radius = 0

        # Height of bounding cylinder
        self.height = 0

        self.check_collisions = False
        self.carriable = False

    def randomize(self, params, rng):
        """
        Set the domain randomization parameters
        """
        pass

    def render(self):
        """
        Draw the object
        """
        raise NotImplementedError

    def step(self, delta_time):
        """
        Update the state of the object
        """
        pass

    def draw_bound(self):
        """
        Draw the bounding circle
        Used for debugging purposes
        """

        x, _, z = self.pos

        glColor3f(1, 0, 0)
        glBegin(GL_LINES)

        for i in range(60):
            a = i * 2 * math.pi / 60
            cx = x + self.radius * math.cos(a)
            cz = z + self.radius * math.sin(a)
            glVertex3f(cx, 0.01, cz)

        glEnd()

    @property
    def dir_vec(self):
        """
        Vector pointing in the direction of forward movement
        """

        x = math.cos(self.dir)
        z = -math.sin(self.dir)
        return np.array([x, 0, z])

    @property
    def right_vec(self):
        """
        Vector pointing to the right of the agent
        """

        x = math.sin(self.dir)
        z = math.cos(self.dir)
        return np.array([x, 0, z])

    @property
    def is_static(self):
        """
        True for objects that cannot move or animate
        (can be rendered statically)
        """
        return False

    @property
    def is_carryable(self):
        """
        True for objects that cannot move or animate
        (can be rendered statically)
        """
        return False


class MeshEnt(Entity):
    """
    Entity whose appearance is defined by a mesh file

    height -- scale the model to this height
    static -- flag indicating this object cannot move
    """

    def __init__(
            self,
            mesh_name,
            height,
            static=True,
            carriable= False,
            check_collisions=True,
            radius=None,
            dir=None,
            carry_scaling_factor=2,
            carry_offset=np.array([1, 1, 1]),
            carry_pitch=0,
            carry_yaw=0,
            static_pitch=0,
            render_trans=np.array([0, 0, 0])
    ):
        super().__init__()

        self.carriable = carriable
        self.static = static
        self.check_collisions = check_collisions
        self.dir = dir
        self.carry_scaling_factor = carry_scaling_factor
        self.carry_offset = carry_offset
        self.carry_pitch = carry_pitch
        self.carry_yaw = carry_yaw
        self.static_pitch = static_pitch
        self.mesh_name = mesh_name
        self.render_trans = render_trans

        # Load the mesh
        self.mesh = ObjMesh.get(mesh_name)

        # Get the mesh extents
        sx, sy, sz = self.mesh.max_coords

        # Compute the mesh scaling factor
        self.scale = height / sy
        self.carry_scale = self.scale / self.carry_scaling_factor

        # Compute the radius and height
        if radius == None:
            self.radius = math.sqrt(sx * sx + sz * sz) * self.scale
        else:
            self.radius = radius
        self.height = height

    def render(self):
        """
        Draw the object
        """
        glPushMatrix()
        glTranslatef(*(self.pos + self.render_trans))
        glScalef(self.scale, self.scale, self.scale)
        glRotatef(self.dir * 180 / math.pi, 0, 1, 0)
        glRotatef(self.static_pitch * 180 / math.pi, 0, 0, 1)
        glColor3f(1, 1, 1)

        self.mesh.render()
        glPopMatrix()

    def render_carry(self):
        """
        Draw the object
        """

        glPushMatrix()
        glTranslatef(*(self.pos + self.render_trans))
        glScalef(self.carry_scale, self.carry_scale, self.carry_scale)

        glRotatef((self.dir + self.carry_yaw) * 180 / math.pi, 0, 1, 0)

        glRotatef(self.carry_pitch * 180 / math.pi, 0, 0, 1)
        glColor3f(1, 1, 1)

        glDepthRange(0, 0.01)
        self.mesh.render()
        glDepthRange(0.01, 1.0)
        glPopMatrix()

    @property
    def is_static(self):
        return self.static

    @property
    def is_carryable(self):
        """
        True for objects that cannot move or animate
        (can be rendered statically)
        """
        return self.carriable

    def set_mesh(self, mesh_name):
        self.mesh_name = mesh_name
        self.mesh = ObjMesh.get(mesh_name)


class MeshEntToggle(MeshEnt):
    def __init__(self, mesh_name, height, static, check_collisions, dir, carriable):
        super().__init__(mesh_name, height, static, carriable, check_collisions, dir=dir)

    def toggle(self):
        raise NotImplementedError


class MeshEntBtn(MeshEntToggle):
    def __init__(self,
                 mesh_name,
                 mesh_name_toggled,
                 height,
                 static=True,
                 check_collisions=True, ):
        super().__init__(mesh_name,
                         height,
                         static,
                         check_collisions)
        self.mesh_name1 = mesh_name
        self.mesh_name2 = mesh_name_toggled
        self.toggled = False

    def toggle(self):
        if self.mesh_name == self.mesh_name2:
            self.set_mesh(self.mesh_name1)
            self.toggled = False
        else:
            self.set_mesh(self.mesh_name2)
            self.toggled = True


class DoubleDoor(MeshEntToggle):
    def __init__(self, locked, height, static, check_collisions, dir =None, pos=None, carriable = False, open = False):
        mesh_name = 'wooden_door/wooden_door' if locked == False else 'wooden_door_red/wooden_door_red'
        super().__init__(mesh_name, height, static, check_collisions, dir, carriable)
        self.pos = pos
        self.open = open
        if self.open:
            self.locked = False
            self.check_collisions = False
        else:
            self.locked = locked


    @property
    def left_door_pos(self):
        left_door_pos = self.pos - 0.5 * self.dir_vec
        if self.open:
            left_door_pos -= 0.5 * self.dir_vec
            left_door_pos += 0.5 *self.right_vec
        return left_door_pos

    @property
    def right_door_pos(self):
        right_door_pos = self.pos + 0.5 * self.dir_vec
        if self.open:
            right_door_pos += 0.5 * self.dir_vec
            right_door_pos += 0.5 *self.right_vec
        return right_door_pos
    @property
    def left_door_dir(self):
        left_door_dir = self.dir
        if self.open:
            left_door_dir -= np.pi/2
        return left_door_dir

    @property
    def right_door_dir(self):
        right_door_dir = self.dir + np.pi
        if self.open:
            right_door_dir += np.pi/2
        return right_door_dir

    def render(self):
        """
        Draw the object
        """
        glPushMatrix()
        glTranslatef(*(self.left_door_pos))
        glScalef(self.scale, self.scale, self.scale)
        glRotatef(self.left_door_dir * 180 / math.pi, 0, 1, 0)
        glColor3f(1, 1, 1)
        self.mesh.render()
        glPopMatrix()

        glPushMatrix()
        glTranslatef(*(self.right_door_pos))
        glScalef(self.scale, self.scale, self.scale)
        glRotatef((self.right_door_dir * 180) / math.pi, 0, 1, 0)
        glColor3f(1, 1, 1)
        self.mesh.render()
        glPopMatrix()

    def toggle(self):
        if not self.open and not self.locked:
            self.open = True
            #self.pos += 5 * self.dir_vec
            self.check_collisions = False
        else:
            # self.pos += self.change_in_pos * np.array([np.sin(self.dir), 0, np.cos(self.dir)])
            self.open = False
            # self.dir += -self.change_in_dir
            self.check_collisions = True

    def unlock(self):
        self.locked = False
        self.mesh = ObjMesh.get('wooden_door/wooden_door')

    def lock(self):
        self.open = False
        self.check_collisions = True
        self.locked = True
        self.mesh = ObjMesh.get('wooden_door_red/wooden_door_red')


class Door(MeshEntToggle):
    def __init__(self, mesh_name, height, static, check_collisions, dir, left_door):
        super().__init__(mesh_name, height, static, check_collisions, dir)
        self.open = False
        self.change_in_dir = np.pi / 2 if left_door else -np.pi / 2
        self.change_in_pos = 0.5 if left_door else -0.5

    def toggle(self):
        if not self.open:
            self.open = True
            self.dir += self.change_in_dir
            self.pos -= self.change_in_pos * np.array([np.sin(self.dir), 0, np.cos(self.dir)])
            self.check_collisions = False
        else:
            self.pos += self.change_in_pos * np.array([np.sin(self.dir), 0, np.cos(self.dir)])
            self.open = False
            self.dir += -self.change_in_dir
            self.check_collisions = True


class ImageFrame(Entity):
    """
    Frame to display an image on a wall
    Note: the position is in the middle of the frame, on the wall
    """

    def __init__(self, pos, dir, tex_name, width, depth=0.05):
        super().__init__()

        self.pos = pos
        self.dir = dir

        # Load the image to be displayed
        self.tex = Texture.get(tex_name)

        self.width = width
        self.depth = depth
        self.height = (float(self.tex.height) / self.tex.width) * self.width

    @property
    def is_static(self):
        return True

    def render(self):
        """
        Draw the object
        """

        x, y, z = self.pos

        # sx is depth
        # Frame points towards +sx
        sx = self.depth
        hz = self.width / 2
        hy = self.height / 2

        glPushMatrix()
        glTranslatef(*self.pos)
        glRotatef(self.dir * (180 / math.pi), 0, 1, 0)

        # Bind texture for front
        glColor3f(1, 1, 1)
        glEnable(GL_TEXTURE_2D)
        self.tex.bind()

        # Front face, showing image
        glBegin(GL_QUADS)
        glNormal3f(1, 0, 0)
        glTexCoord2f(1, 1)
        glVertex3f(sx, +hy, -hz)
        glTexCoord2f(0, 1)
        glVertex3f(sx, +hy, +hz)
        glTexCoord2f(0, 0)
        glVertex3f(sx, -hy, +hz)
        glTexCoord2f(1, 0)
        glVertex3f(sx, -hy, -hz)
        glEnd()

        # Black frame/border
        glDisable(GL_TEXTURE_2D)
        glColor3f(0, 0, 0)

        glBegin(GL_QUADS)

        # Left
        glNormal3f(0, 0, -1)
        glVertex3f(0, +hy, -hz)
        glVertex3f(+sx, +hy, -hz)
        glVertex3f(+sx, -hy, -hz)
        glVertex3f(0, -hy, -hz)

        # Right
        glNormal3f(0, 0, 1)
        glVertex3f(+sx, +hy, +hz)
        glVertex3f(0, +hy, +hz)
        glVertex3f(0, -hy, +hz)
        glVertex3f(+sx, -hy, +hz)

        # Top
        glNormal3f(0, 1, 0)
        glVertex3f(+sx, +hy, +hz)
        glVertex3f(+sx, +hy, -hz)
        glVertex3f(0, +hy, -hz)
        glVertex3f(0, +hy, +hz)

        # Bottom
        glNormal3f(0, -1, 0)
        glVertex3f(+sx, -hy, -hz)
        glVertex3f(+sx, -hy, +hz)
        glVertex3f(0, -hy, +hz)
        glVertex3f(0, -hy, -hz)

        glEnd()

        glPopMatrix()


class TextFrame(Entity):
    """
    Frame to display text or numbers on a wall
    Note: the position is in the middle of the frame, on the wall
    """

    def __init__(self, pos, dir, str, height=0.15, depth=0.05):
        super().__init__()

        self.pos = pos
        self.dir = dir

        self.str = str

        self.depth = depth
        self.height = height
        self.width = len(str) * height

    @property
    def is_static(self):
        return True

    def randomize(self, params, rng):
        self.texs = []
        for ch in self.str:
            try:
                if ch == ' ':
                    self.texs.append(None)
                else:
                    tex_name = 'chars/ch_' + ch
                    self.texs.append(Texture.get(tex_name, rng))
            except:
                raise 'only alphanumerical characters supported in TextFrame'

    def render(self):
        """
        Draw the object
        """

        x, y, z = self.pos

        # sx is depth
        # Frame points towards +sx
        sx = 0.05
        hz = self.width / 2
        hy = self.height / 2

        glPushMatrix()
        glTranslatef(*self.pos)
        glRotatef(self.dir * (180 / math.pi), 0, 1, 0)

        # Bind texture for front
        glColor3f(1, 1, 1)

        # For each character
        for idx, ch in enumerate(self.str):
            tex = self.texs[idx]
            if tex:
                glEnable(GL_TEXTURE_2D)
                self.texs[idx].bind()
            else:
                glDisable(GL_TEXTURE_2D)

            char_width = self.height
            z_0 = hz - char_width * (idx + 1)
            z_1 = z_0 + char_width

            # Front face, showing image
            glBegin(GL_QUADS)
            glNormal3f(1, 0, 0)
            glTexCoord2f(1, 1)
            glVertex3f(sx, +hy, z_0)
            glTexCoord2f(0, 1)
            glVertex3f(sx, +hy, z_1)
            glTexCoord2f(0, 0)
            glVertex3f(sx, -hy, z_1)
            glTexCoord2f(1, 0)
            glVertex3f(sx, -hy, z_0)
            glEnd()

        # Black frame/border
        glDisable(GL_TEXTURE_2D)
        glColor3f(0, 0, 0)

        glBegin(GL_QUADS)

        # Left
        glNormal3f(0, 0, -1)
        glVertex3f(0, +hy, -hz)
        glVertex3f(+sx, +hy, -hz)
        glVertex3f(+sx, -hy, -hz)
        glVertex3f(0, -hy, -hz)

        # Right
        glNormal3f(0, 0, 1)
        glVertex3f(+sx, +hy, +hz)
        glVertex3f(0, +hy, +hz)
        glVertex3f(0, -hy, +hz)
        glVertex3f(+sx, -hy, +hz)

        # Top
        glNormal3f(0, 1, 0)
        glVertex3f(+sx, +hy, +hz)
        glVertex3f(+sx, +hy, -hz)
        glVertex3f(0, +hy, -hz)
        glVertex3f(0, +hy, +hz)

        # Bottom
        glNormal3f(0, -1, 0)
        glVertex3f(+sx, -hy, -hz)
        glVertex3f(+sx, -hy, +hz)
        glVertex3f(0, -hy, +hz)
        glVertex3f(0, -hy, -hz)

        glEnd()

        glPopMatrix()


class Box_Rect(Entity):
    """
    Colored box object
    """

    def __init__(self, color, size_x=0.8, size_y=0.8, size_z=0.8):
        super().__init__()

        size = np.array([size_x, size_y, size_z])
        size = np.array(size)
        sx, sy, sz = size

        self.color = color
        self.size = size

        self.radius = math.sqrt(sx * sx + sz * sz) / 2
        self.height = sy

    def randomize(self, params, rng):
        self.color_vec = COLORS[self.color] + params.sample(rng, 'obj_color_bias')
        self.color_vec = np.clip(self.color_vec, 0, 1)

    def render(self):
        """
        Draw the object
        """

        sx, sy, sz = self.size

        glDisable(GL_TEXTURE_2D)
        glColor3f(*self.color_vec)

        glPushMatrix()
        glTranslatef(*self.pos)
        glRotatef(self.dir * (180 / math.pi), 0, 1, 0)

        drawBox(
            x_min=-sx / 2,
            x_max=+sx / 2,
            y_min=0,
            y_max=sy,
            z_min=-sz / 2,
            z_max=+sz / 2
        )

        glPopMatrix()


class Box(Entity):
    """
    Colored box object
    """

    def __init__(self, color, size=0.8):
        super().__init__()

        if type(size) is int or type(size) is float:
            size = np.array([size, 2 * size, size])
        size = np.array(size)
        sx, sy, sz = size

        self.color = color
        self.size = size

        self.radius = math.sqrt(sx * sx + sz * sz) / 2
        self.height = sy

    def randomize(self, params, rng):
        self.color_vec = COLORS[self.color] + params.sample(rng, 'obj_color_bias')
        self.color_vec = np.clip(self.color_vec, 0, 1)

    def render(self):
        """
        Draw the object
        """

        sx, sy, sz = self.size

        glDisable(GL_TEXTURE_2D)
        glColor3f(*self.color_vec)

        glPushMatrix()
        glTranslatef(*self.pos)
        glRotatef(self.dir * (180 / math.pi), 0, 1, 0)

        drawBox(
            x_min=-sx / 2,
            x_max=+sx / 2,
            y_min=0,
            y_max=sy,
            z_min=-sz / 2,
            z_max=+sz / 2
        )

        glPopMatrix()


class Key(MeshEnt):
    """
    Key the agent can pick up, carry, and use to open doors
    """

    def __init__(self, color):
        assert color in COLOR_NAMES
        super().__init__(
            mesh_name='key_{}'.format(color),
            height=0.35,
            static=False,
            check_collisions=False
        )


class Ball(MeshEnt):
    """
    Ball (sphere) the agent can pick up and carry
    """

    def __init__(self, color, size=0.6, carry_scaling_factor=1.3):
        assert color in COLOR_NAMES
        super().__init__(
            mesh_name='ball_{}'.format(color),
            height=size,
            static=False,
            check_collisions=True,
            carry_scaling_factor=carry_scaling_factor

        )
        self.carry_scaling_factor = carry_scaling_factor
        self.color = color

    def set_color(self, color):
        mesh_name = 'ball_{}'.format(color)
        self.mesh = ObjMesh.get(mesh_name)


class Agent(Entity):
    def __init__(self):
        super().__init__()

        # Distance between the camera and the floor
        self.cam_height = 1.5

        # Camera up/down angles in degrees
        # Positive angles tilt the camera upwards
        self.cam_pitch = 0

        # Vertical field of view in degrees
        self.cam_fov_y = 60

        # Bounding cylinder size for the agent
        self.radius = 0.4
        self.height = 1.6

        # Object currently being carried by the agent
        self.carrying_right = None
        self.carrying_left = None

    @property
    def cam_pos(self):
        """
        Camera position in 3D space
        """

        rot_y = gen_rot_matrix(Y_VEC, self.dir)
        cam_disp = np.array([self.cam_fwd_disp, self.cam_height, 0])
        cam_disp = np.dot(cam_disp, rot_y)

        return self.pos + cam_disp

    @property
    def cam_dir(self):
        """
        Camera direction (lookat) vector

        Note: this is useful even if just for slight domain
        randomization of camera angle
        """

        rot_z = gen_rot_matrix(Z_VEC, self.cam_pitch * math.pi / 180)
        rot_y = gen_rot_matrix(Y_VEC, self.dir)

        dir = np.dot(X_VEC, rot_z)
        dir = np.dot(dir, rot_y)

        return dir

    def randomize(self, params, rng):
        params.sample_many(rng, self, [
            'cam_height',
            'cam_fwd_disp',
            'cam_pitch',
            'cam_fov_y',
        ])
        # self.radius = params.sample(rng, 'bot_radius')

    def render(self):
        """
        Draw the agent
        """

        # Note: this is currently only used in the top view
        # Eventually, we will want a proper 3D model

        p = self.pos + Y_VEC * self.height
        dv = self.dir_vec * self.radius
        rv = self.right_vec * self.radius

        p0 = p + 0.75 * dv
        p1 = p + 0.65 * (rv - dv)
        p2 = p + 0.65 * (-rv - dv)

        glColor3f(1, 0, 0)
        glBegin(GL_TRIANGLES)
        glVertex3f(*p0)
        glVertex3f(*p2)
        glVertex3f(*p1)

        glEnd()

        p0 = p + dv
        p1 = p + 0.75 * (rv - dv)
        p2 = p + 0.75 * (-rv - dv)

        glColor3f(0, 0, 0)
        glBegin(GL_TRIANGLES)
        glVertex3f(*p0)
        glVertex3f(*p2)
        glVertex3f(*p1)
        glEnd()

        """
        glBegin(GL_LINE_STRIP)
        for i in range(20):
            a = (2 * math.pi * i) / 20
            pc = p + dv * math.cos(a) + rv * math.sin(a)
            glVertex3f(*pc)
        glEnd()
        """

    def step(self, delta_time):
        pass

## Motivation
Drones and ground robotic vehicles have been used with great success to perform tasks in areas that are difficult or unsafe for humans to access.
In certain situations RF signals cannot reach robots and can therefore not be controlled by a human operator. In these scenarios, we need autonomous robots to carry out tasks.
It can be argued that autonomous robots may perform better or more precisely than human beings in certain tasks. An example of such a problem is a collapsed mine, where trapped miners need assistance.
In many cases collapsed mines are very dangerous and full of toxic gases.
It is therefore not safe for rescue workers to enter. Robots may be of great assistance in such a situation, but the RF signals will most likely be unable to reach the robot, due to the thick layers of ground and rock that separate the trapped miners from the rescue workers.
The robot must therefore be able to make the decisions of the human operator by itself.

## Environment
This project is set in an environment that is developed in @maximecb's [Mini-World](https://github.com/maximecb/gym-miniworld) simulator.
The environment is representative of a 3D mine with a number of corrodors/rooms and doorways.
The task of the robot or agent is to deliver a first aid kit to a miner. To complete this task, a number of obstacles need to be overcome by the agent.
As a collapsed mine is unknown, it can be difficult to develop an agent that is able to deal with everything it may encounter.
The agent must therefore be capable of learning i.e. teach itself to deal with certain situations.
Obstacles that the agent will encounter in the simulated environment includes:
1. Multiple rooms that are generated in a random fashion and need to be navigated.
2. Doors that are closed and have to be opened in order to access other areas of the environment.
3. Some doors may be locked and a key has to be found to unlock them.
<!-- 4. Debris that blocks the agent’s path and has to be moved out of the way in order to advance to other areas of the environment. -->

## Solution
The agent has access to a single RGB first-persoon camera and it is the only way to observe the environment.
The agent therefore do not have access to any other external information about the environment such as a GPS for localisation.
To address this problem, the project makes use of Deep Reinforcement Learning.
A Distributed Deep Q-Network with a few modifications is implemented.
The advantage of this algorithm is that it is general and is able to learn from the pixels.
The agent is introduced to the above mentioned sub-tasks and is able to learn to complete them all.
Components of the algorithm includes:
1. A Distributed version of DeepMind's Deep Q-Network.
2. Dueling Neural Network Architecture
3. Double DQN update
4. Prioritized Experience Replay
5. Multi-step updates
6. Frame-stacking and Action-stacking to deal with the partially observable environment
7. Incremental Learning for problems where rewards are very sparse

As the final algorithm consists of a number of modifications, an ablation study is done to assess the contribution of each improvement.
The agent only receives credit when the miner receives the first aid kit.
This allows for a general reward function and therefore a new reward function does not have to be specified for each new type of obstacle introduced to the problem.
Using this reward function can lead to the sparse reward problem, where rewards are too sparse for the agent to ever encounter.
A seperate test is conducted to assess how Incremental Learning can help in a scenario like this.
The algorithm's ability to generalise and operate in unseen (different looking) and very random environments is also examined.

## Sample Videos
![Sample video](resources/sample.gif)   ![Sample video](resources/random_textures.gif)

[Higher quality YouTube video](https://www.youtube.com/watch?v=bWR8HVpqBIo)


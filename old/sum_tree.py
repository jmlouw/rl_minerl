import numpy as np

# Sumtree
# a binary tree data structure where the parent's value is the sum of its childrens

class Node:
    def __init__(self, left, right, is_leaf: bool = False, idx=None):
        self.left = left
        self.right = right
        self.is_leaf = is_leaf

        if not self.is_leaf:
            self.value = self.left.value + self.right.value
        self.parent = None
        self.idx = idx

        if left is not None:
            left.parent = self
        if right is not None:
            right.parent = self

    @classmethod
    def create_leaf(cls, value, idx):
        leaf = cls(None, None, is_leaf=True, idx=idx)
        leaf.value = value
        return leaf


class SumTree:
    def __init__(self, capacity):
        self.max_recorded_priority = 1.0
        self.capacity = capacity
        self.root_node, self.leaf_nodes = self.create_tree(capacity)
        self.data = np.zeros(capacity, dtype=object)
        self.n_entries = 0
        self.write = 0

    def create_tree(self, capacity):
        nodes = [Node.create_leaf(v, i) for i, v in enumerate([0] * capacity)]
        leaf_nodes = nodes
        while len(nodes) > 1:
            inodes = iter(nodes)
            nodes = [Node(*pair) for pair in zip(inodes, inodes)]

        return nodes[0], leaf_nodes

    def retrieve(self, value: float):
        leaf_node = self._retrieve(value, self.root_node)
        return leaf_node.idx, leaf_node.value, self.data[leaf_node.idx]

    @classmethod
    def _retrieve(cls, value: float, node: Node):
        if node.is_leaf:
            return node

        if node.left.value >= value:
            return cls._retrieve(value, node.left)
        else:
            return cls._retrieve(value - node.left.value, node.right)

    def add(self, p, data):
        leaf_node = self.leaf_nodes[self.write]
        self.data[self.write] = data

        self.update(leaf_node, p)

        self.write += 1
        if self.write >= self.capacity:
            self.write = 0

        if self.n_entries < self.capacity:
            self.n_entries += 1

    def update(self, node: Node, new_value: float):
        self.max_recorded_priority = max(new_value, self.max_recorded_priority)

        change = new_value - node.value

        node.value = new_value
        self.propagate_changes(change, node.parent)

    def propagate_changes(self, change: float, node: Node):
        node.value += change

        if node.parent is not None:
            self.propagate_changes(change, node.parent)

    def total(self):
        return self.root_node.value


if __name__ == '__main__':
    # test sumtree
    tree = SumTree(4)
    tree.add(1, 'Appels')
    tree.add(4, 'Mango')
    tree.add(2, 'Tuna')
    tree.add(3, 'Bacon')

    print(tree.retrieve(0.5))
    print(tree.retrieve(3.5))
    print(tree.retrieve(6.5))
    print(tree.retrieve(7.5))

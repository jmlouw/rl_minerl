from unittest import TestCase
import gym
import numpy as np
import numpy.testing as nt

# user imports
from old.minerl.minerl_utils import register_env
from wrappers.minerl_env_wrappers import LFA_Observation_Wrapper


class Testlfa_observation_wrapper(TestCase):
    @classmethod
    def setUp(self) -> None:
        env_id = register_env('walk_goal0')
        self.env = gym.make(env_id)
        self.dim = 8
        self.env = LFA_Observation_Wrapper(self.env, dim=self.dim)
        state = self.env.reset()
        self.state = self.env.reset()

    def test_observation(self):
        self.assertEqual(self.env.observation_space.sample().shape, (self.dim, self.dim))
        self.assertEqual(self.env.observation_space.sample().shape, self.state.shape)
        nt.assert_array_equal(self.state, np.array([[0, 0, 0, 1, 1, 0, 0, 0],
                                                    [0, 0, 0, 2, 2, 0, 0, 0],
                                                    [0, 0, 2, 2, 2, 2, 0, 0],
                                                    [0, 0, 2, 2, 2, 2, 0, 0, ],
                                                    [0, 2, 2, 2, 2, 2, 2, 0],
                                                    [0, 2, 2, 2, 2, 2, 2, 0],
                                                    [0, 2, 2, 2, 2, 2, 2, 0],
                                                    [2, 2, 2, 2, 2, 2, 2, 2]]))
        print(self.state)

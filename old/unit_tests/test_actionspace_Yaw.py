from unittest import TestCase
import numpy.testing as npt
import numpy as np
import gym
import minerl
from collections import OrderedDict

from wrappers.minerl_env_wrappers import Actionspace_Yaw

class TestActionspace_Yaw(TestCase):
    @classmethod
    def setUp(self) -> None:
        env = gym.make('MineRLNavigateDense-v0')
        self.env = Actionspace_Yaw(env)


    def test_action(self):
        print(self.env.action_space.n)
        npt.assert_equal(self.env.action(0)['camera'], np.array([0,0]))
        npt.assert_equal(self.env.action(1)['camera'], np.array([0,-30]))
        npt.assert_equal(self.env.action(2)['camera'], np.array([0,30]))

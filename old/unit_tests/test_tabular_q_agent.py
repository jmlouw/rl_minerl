from unittest import TestCase
from old.agents.agent_tabular_q import tabular_state
import numpy as np

# lava_front = str(pov[25,32,1] < 100)
# lava_left =  str(pov[40,4,1] < 100)
# lava_right = str(pov[40,60,1] < 100)


class Test(TestCase):
    def test_tabular_state(self):
        pov = np.ones((64,64,3))*200 # no lava 0
        print(tabular_state(pov))

        pov = np.ones((64,64,3))*200 # front 1
        pov[25,32,1] = 0
        print(tabular_state(pov))

        pov = np.ones((64,64,3))*200 # left 2
        pov[40,4,1] =0
        print(tabular_state(pov))

        pov = np.ones((64,64,3))*200 # 3
        pov[25,32,1] = 0 #front
        pov[40,4,1] =0 # left
        print(tabular_state(pov))

        pov = np.ones((64,64,3))*200 # right 4
        pov[40,60,1] = 0
        print(tabular_state(pov))

        pov = np.ones((64,64,3))*200 # 5
        pov[25,32,1] = 0 # front
        pov[40,60,1] = 0 # right
        print(tabular_state(pov))

        pass

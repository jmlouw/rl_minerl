from unittest import TestCase
from gym.spaces import Discrete, Box, Dict
import numpy as np

# user imports
from agents.agent_lfa import Agent_LFA
from utils.gen_args import Arguments


class TestAgent_LFA(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestAgent_LFA, cls).setUpClass()
        args = Arguments()
        args.eligibility_traces = True
        cls.ap = Discrete(3)
        obs_space = Dict({'pov': Box(
            low=0,
            high=2,
            shape=(2, 2),
            dtype='uint8'
        )})

        cls.test_agent = Agent_LFA(cls.ap, obs_space, args)

        cls.sample_obs = obs_space.sample()
        cls.sample_pov = cls.sample_obs['pov']

        cls.sample_pov = np.array([[1,1],
                                   [2,2]])

        cls.test_agent.weights = np.zeros((len(cls.test_agent.weights)), dtype=np.float64)
        print('Initial weights: ',cls.test_agent.weights)

    def test_string_agent_specs(self):
        print(self.test_agent.agent_specs_dict())

    def test_get_s_feature(self):
        print('get_s_feature')
        print(self.test_agent.get_S_feature(self.sample_pov))

    def test_get_sa_feature(self):
        print('get_sa_feature')
        for i in range(self.ap.n):
            print(f'action = {i}')
            print(self.test_agent.get_SA_feature(self.sample_pov, i))

    def test_get_approx_q(self):
        print('test_get_approx_q')
        for i in range(self.ap.n):
            print(f'action = {i}')
            print(self.test_agent.get_approx_q(self.sample_pov, i))

    def test_get_sa_feature_(self):
        print('test_get_sa_feature_')
        print(self.test_agent.get_SA_feature_(self.sample_pov))
        pass

    def test_get_approx_q_(self):
        print('get_approx_q_')
        print(self.test_agent.get_approx_q_(self.sample_pov))

    def test_update_agent(self):
        print('test_update_agent')
        self.test_agent.alpha = 1
        self.test_agent.gamma = 1
        self.test_agent.e_t_enabled = False

        state = {'pov': np.array([[1, 1],
                          [2, 2]])}

        print(self.test_agent.get_SA_feature(state['pov'].squeeze(), 0))

        next_state = {'pov':np.array([[0, 0],
                               [0, 0]])}

        reward = 500

        self.test_agent.update_agent(state, 0, next_state, reward, total_steps=50)
        print('weights:\n',self.test_agent.weights)
        print('q-values:\n',self.test_agent.get_approx_q_(state['pov'].squeeze()))
        print('q-values_next:\n',self.test_agent.get_approx_q_(next_state['pov'].squeeze()))
        print()

        self.test_agent.update_agent(state, 0, next_state, reward, total_steps=50)
        print('weights:\n',self.test_agent.weights)
        print('q-values:\n',self.test_agent.get_approx_q_(state['pov'].squeeze()))
        print('q-values_next:\n',self.test_agent.get_approx_q_(next_state['pov'].squeeze()))
        print()

        self.test_agent.update_agent(state, 0, next_state, reward, total_steps=50)
        print('weights:\n',self.test_agent.weights)
        print('q-values:\n',self.test_agent.get_approx_q_(state['pov'].squeeze()))
        print('q-values_next:\n',self.test_agent.get_approx_q_(next_state['pov'].squeeze()))
        print()

        self.test_agent.update_agent(state, 0, next_state, reward, total_steps=50)
        print('weights:\n',self.test_agent.weights)
        print('q-values:\n',self.test_agent.get_approx_q_(state['pov'].squeeze()))
        print('q-values_next:\n',self.test_agent.get_approx_q_(next_state['pov'].squeeze()))


    def test_select_action(self):
        pass

from unittest import TestCase
import gym
from wrappers.minigrid_env_wrappers import MiniGridPOVRGBWrapper
from matplotlib import pyplot as plt

class TestFullObsRGBWrapper(TestCase):
    @classmethod
    def setUp(self) -> None:
        self.env = gym.make('MiniGrid-DistShift2-v0')
        self.env = MiniGridPOVRGBWrapper(self.env)

    def test_observation(self):
        state = self.env.reset()
        for a in range(10):
            next_state, reward, done, info = self.env.step(self.env.action_space.sample())
            if done:
                self.env.reset()
            plt.imshow(next_state['pov'])
            plt.show()
        print(state['pov'].shape)

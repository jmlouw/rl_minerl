import gym
import minerl
from multiprocessing import Pipe, Process
import multiprocessing
from utils.utils import to_tensor
from old.minerl.minerl_utils import register_env


def worker(remote, env):
    name = multiprocessing.current_process().name
    print("[{}]".format(name) + "Setting up Minecraft Environment...")
    remote.send("[{}]".format(name) + "Minecraft running.")
    try:
        while True:
            cmd, action = remote.recv()
            if cmd == 'reset':
                # print("[{}]".format(name) + "Resetting...")
                # TODO: Add np.random.randint RNG for seed
                #env.seed(action)  # Reseed before resetting
                obs = env.reset()

                # print("[{}]".format(name) + "Ready.")
                remote.send((obs))

            elif cmd == 'step':
                obs, reward, done, info = env.step(action)
                remote.send((obs, reward, done, info))

            elif cmd == 'seed':
                remote.send(env.seed(action))

            elif cmd == 'render':
                remote.send(env.render())

            elif cmd == 'close':
                env.close()
                remote.close()
                break

    except Exception as e:
        print(f'Error in worker {name}: {e}')


class GymManager:
    def __init__(self, env1, env2, env_tens: bool = False):
        from wrappers.minerl_env_wrappers import Actionspace_Yaw
        self.env_tens = env_tens
        self.curr_env = 'active'  # Set to active since it will be reset to backup
        self.ex_seeds = {  # Some arbitrary seeds
            'active': 210395,
            'backup': 483219
        }
        self.env1 = env1
        self.env2 = env2
        self.action_space = self.env1.action_space
        self.observation_space = self.env1.observation_space
        self.active_remote, a = Pipe()
        self.backup_remote, b = Pipe()
        self.active_env = Process(target=worker, args=(a, self.env1))
        self.backup_env = Process(target=worker, args=(b, self.env2))
        self.active_env.start()
        self.backup_env.start()
        print(self.active_remote.recv())
        print(self.backup_remote.recv())
        self.backup_remote.send(('reset', self.ex_seeds[self.curr_env]))
        self.prev_obs = self.backup_remote.recv()  # Store reset obs from backup env
        self.backup_ready = True

    def render(self):
        self.active_remote.send(('render', 0))
        return self.active_remote.recv()

    def step(self, action):
        #print(self.backup_remote.recv())
        if self.backup_remote.poll():  # Poll to see if env has been reset, if yes, store obs for next loop.
            self.prev_obs = self.backup_remote.recv()
            self.backup_ready = True

        self.active_remote.send(('step', action))
        obs, reward, done, info = self.active_remote.recv()
        if self.env_tens:
            obs = to_tensor(obs)
        return obs, reward, done, info

    def reset(self):  # Reset active env & swap to backup
        while not self.backup_ready:
            if self.backup_remote.poll():
                self.backup_ready = True
                self.prev_obs = self.backup_remote.recv()

        self._switch_curr_env()
        self.active_remote.send(('reset', self.ex_seeds[self.curr_env]))

        temp = self.backup_env
        self.backup_env = self.active_env
        self.active_env = temp

        temp = self.backup_remote
        self.backup_remote = self.active_remote
        self.active_remote = temp

        if self.env_tens:
            self.prev_obs = to_tensor(self.prev_obs)
        self.backup_ready = False
        return self.prev_obs  # Should be observation from backup env reset

    #         return self.backup_remote.recv()

    def close(self):
        self.active_remote.send(('close', 0))
        return True

    def _switch_curr_env(self):  # For keeping separate seeds. Might be worth it to randomize inside worker
        self.curr_env = 'backup' if self.curr_env == 'active' else 'active'

    def sample_action(self):
        return self.action_space.sample()

    @property
    def spec(self):
        return self.env1.spec

if __name__ == '__main__':
    xml_name = 'stay_alive'
    env_id = register_env(xml_name)
    env = GymManager(env_id)
    obs = env.reset()
    #time.sleep(5)
    episodes = 50
    for i in range(episodes):
        print(f'Episode Count:{i}')
        done = False
        while not done:
            n_obs, reward, done, info = env.step(env.sample_action())
            #time.sleep(0.05)
        print('resetting envs')
        obs = env.reset()

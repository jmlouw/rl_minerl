from pathlib import Path

import numpy as np
from gym import register
from minerl.env import spaces


def register_env(xml_name):
    env_id = 'custom_mission-v0'
    observation_space = spaces.Dict({
        'pov': spaces.Box(low=0, high=255, shape=(64, 64, 3), dtype=np.uint8),
        'XPos': spaces.Box(low=-1000, high=1000, shape=(), dtype=np.float32),
        'YPos': spaces.Box(low=-1000, high=1000, shape=(), dtype=np.float32),
        'ZPos': spaces.Box(low=-1000, high=1000, shape=(), dtype=np.float32)
    })

    action_space = spaces.Dict(spaces={
        "forward": spaces.Discrete(2),
        "jump": spaces.Discrete(2),
        "camera": spaces.Box(low=-100, high=100, shape=(2,), dtype=np.float32),
    })

    project_folder = Path(__file__).parent.parent
    my_mission_dir = project_folder / 'levels' / 'minerl' / (xml_name + '.xml')
    register(
        id=env_id,
        entry_point='minerl.env:MineRLEnv',
        kwargs={
            'xml': my_mission_dir,
            'observation_space': observation_space,
            'action_space': action_space,
        },
        max_episode_steps=10000,
    )
    return env_id
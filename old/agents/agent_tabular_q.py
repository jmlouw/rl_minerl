import numpy as np
from agents.agent_base_q import BaseQAgent
from utils.gen_args import Arguments
import random
from random import randrange
import time
from matplotlib import pyplot as PLT

def tabular_state(pov: np.array):
    """
    Returns a tabular state
    0 - no lava             0 0 0
    1 - lava front          0 0 1
    2 - lava left           0 1 0
    3 - lava front left     0 1 1
    4 - lava right          1 0 0
    5 - lava front right    1 0 1
    :return: integer 0 or 1 or 2 or 3 or 4 or 5
    """
    lava_front = str(int(pov[25,32,1] < 100))
    lava_left =  str(int(pov[40,4,1] < 100))
    lava_right = str(int(pov[40,60,1] < 100))

    return int(lava_right + lava_left+lava_front, 2)



class Tabular_Q_Agent(BaseQAgent):
    def __init__(self, action_space, observation_space, arg: Arguments):
        super().__init__(action_space, observation_space, arg)
        # self.policy  = np.zeros((8, env.action_space.n))
        print(self.policy)

    def agent_main(self, env, max_steps = 25000):
        rate = self.strategy.start
        episode = 0
        episode_step = 0
        state = env.reset()
        while 1:
            episode += 1
            self.reset_time.tic() # start timer for reset
            state = env.reset()
            self.reset_time.toc() # end timer for reset
            while 1:
                episode_step +=1

                self.step_time.tic() # start timer for step
                rate = self.strategy.get_exploration_rate(self.total_steps)
                self.total_steps += 1
                action = self.select_action(state, env, rate)
                next_state, reward, done, info = env.step(action)
                print(f'state: {state}, step: {self.total_steps}, reward: {reward}' )



                self.policy[state, action] = self.policy[state, action] * (1 - self.lr) + \
                                             self.lr * (reward + self.gamma * np.max(self.policy[next_state, :]))
                state = next_state
                self.step_time.toc() # end timer for step
                if self.is_tracking:
                    self.tracking.after_step(reward, self.step_time.elapsed)
                env.render()

                if done or self.total_steps == max_steps:
                    break

            if self.is_tracking:
                self.tracking.after_episode(rate, self.reset_time.elapsed)

            if self.total_steps == max_steps:
                break

        print(self.policy)
        state = env.reset()
        while 1:
            action = np.argmax(self.policy[state,:])
            state, reward, done, info = env.step(action)
            print(state)
            env.render()
            if done:
                break

            time.sleep(0.1)

        if self.is_tracking:
            self.tracking.close()

    def select_action(self, state, env, rate):
        if rate > random.random():
            action = randrange(env.action_space.n)
            return action
        else:
            return np.argmax(self.policy[state,:])

    def get_current(self, states, actions):
        pass

    def get_next(self, next_states):
        pass






import torch
import torch.nn as nn
from torch.distributions import Categorical
import torch.nn.functional as F

class Flatten(nn.Module):
    def forward(self, input):
        return input.view(input.size(0), -1)


class DQN_Simple_fobs(nn.Module):
    def __init__(self, obs_space, no_actions):
        super().__init__()
        self.device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
        o_shp = obs_space['pov'].shape
        dir_size = obs_space['direction'].shape[0]
        self.fc1 = nn.Linear(in_features=o_shp[0]*o_shp[1]*o_shp[2] + dir_size, out_features=24)
        self.fc2 = nn.Linear(in_features=24, out_features=32)
        self.out = nn.Linear(in_features=32, out_features=no_actions)

    def forward(self, t):
        pov = t['pov'].flatten(start_dim=1).to(self.device)
        dir = t['direction'].to(self.device)
        # print(pov)
        # print(dir)

        # print('POV', pov)
        # print('Direction', dir)
        t= torch.cat((pov, dir), dim=1)
        # print(t.shape)

        t = F.relu(self.fc1(t))
        t = F.relu(self.fc2(t))
        t = self.out(t)
        return t




class DQN(nn.Module):
    def __init__(self, observation_space, no_actions):
        super().__init__()
        self.my_net = nn.Sequential(
            nn.Conv2d(in_channels=3, out_channels=32, kernel_size=3, stride=2,padding=3),
            nn.ReLU(),
            nn.Conv2d(in_channels=32, out_channels=64, kernel_size=2, stride=2, padding=3),
            nn.ReLU(),
            nn.Conv2d(in_channels=64, out_channels=64, kernel_size=2, stride=2, padding=3),
            nn.ReLU(),
            Flatten(),
            nn.Linear(in_features=6 * 6 * 7, out_features=150),
            nn.ReLU(),
            nn.Linear(in_features=150, out_features=no_actions),
            nn.ReLU()  # e of (-1, 1) -> scale to (-10, 10)
        )

    def forward(self, t):
        return self.my_net(t)
    

class DQN_Simple(nn.Module):
    def __init__(self, obeservation_space, no_actions):
        super().__init__()
            
        self.fc1 = nn.Linear(in_features=64*64*3, out_features=24)   
        self.fc2 = nn.Linear(in_features=24, out_features=32)
        self.out = nn.Linear(in_features=32, out_features=no_actions)

    def forward(self, t):
        t = t.flatten(start_dim=1)
        t = F.relu(self.fc1(t))
        t = F.relu(self.fc2(t))
        t = self.out(t)
        return t
    
## PPO
class ActorCritic(nn.Module):
    def __init__(self,  no_actions):
        super(ActorCritic, self).__init__()
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        # self.affine = nn.Linear(state_dim, n_latent_var)

        # actor
        self.action_layer = nn.Sequential(
            nn.Conv2d(in_channels=3, out_channels=32, kernel_size=8, stride=4),
            nn.ReLU(),
            nn.Conv2d(in_channels=32, out_channels=64, kernel_size=4, stride=2),
            nn.ReLU(),
            nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, stride=1),
            nn.ReLU(),
            Flatten(),
            nn.Linear(in_features=4 * 4 * 64, out_features=150),
            nn.ReLU(),
            nn.Linear(in_features=150, out_features=no_actions),
            nn.Softmax(dim=-1))  # e of (-1, 1) -> scale to (-10, 10)

        # critic
        self.value_layer = nn.Sequential(
            nn.Conv2d(in_channels=3, out_channels=32, kernel_size=8, stride=4),
            nn.ReLU(),
            nn.Conv2d(in_channels=32, out_channels=64, kernel_size=4, stride=2),
            nn.ReLU(),
            nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, stride=1),
            nn.ReLU(),
            Flatten(),
            nn.Linear(in_features=4 * 4 * 64, out_features=150),
            nn.ReLU(),
            nn.Linear(in_features=150, out_features=1),
        )

    def forward(self):
        raise NotImplementedError

    def act(self, state, memory):

        state = (state).float().to(self.device)
        action_probs = self.action_layer(state)
        dist = Categorical(action_probs)
        action = dist.sample()

        memory.states.append(state)
        memory.actions.append(action)
        memory.logprobs.append(dist.log_prob(action))

        return action.item()

    def evaluate(self, state, action):
        action_probs = self.action_layer(state)
        dist = Categorical(action_probs)

        action_logprobs = dist.log_prob(action)
        dist_entropy = dist.entropy()

        state_value = self.value_layer(state)

        return action_logprobs, torch.squeeze(state_value), dist_entropy
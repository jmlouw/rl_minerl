import numpy as np
import random
import copy

# user imports
from old.sum_tree import SumTree

class ReplayMemory(object):
    def __init__(self, capacity):
        self.capacity = capacity
        self.memory = []
        self.position = 0

    def add(self, data_sample):
        """Saves a transition."""
        if len(self.memory) < self.capacity:
            self.memory.append(None)
        self.memory[self.position] = copy.copy(data_sample)
        self.position = (self.position + 1) % self.capacity

    def sample(self, batch_size):
        return random.sample(self.memory, batch_size), None, None

    def can_provide_sample(self, batch_size):
            return len(self.memory)>batch_size

    def __len__(self):
        return len(self.memory)

class PReplayMemory(object):
    def __init__(self, capacity:int):
        self.tree = SumTree(capacity)
        self.capacity = capacity
        self.e = 0.01
        self.a = 0.6
        self.beta = 0.4
        self.beta_increment = 0.001

    def _get_priority(self, td_error):
        return (np.abs(td_error) + self.e) ** self.a

    def add(self, data_sample):
        #p = self._get_priority(error)
        p = self.tree.max_recorded_priority
        self.tree.add(p, data_sample)

    def sample(self, n):
        batch = []
        idxs = []
        segment = self.tree.total()/n
        priorities = []

        self.beta = np.min([1., self.beta + self.beta_increment])

        for i in range(n):
            a = segment * i
            b = segment * (i + 1)

            s = random.uniform(a, b)
            (idx, p, data) = self.tree.retrieve(s)
            priorities.append(p)
            batch.append(data)
            idxs.append(idx)

        sampling_probabilities = np.array(priorities, dtype=np.float32) / self.tree.total()

        is_weight = np.power(self.tree.n_entries * sampling_probabilities, -self.beta)
        is_weight /= is_weight.max()

        return batch, idxs, is_weight

    def update(self, idx, td_error):
        p = self._get_priority(td_error)
        self.tree.update(self.tree.leaf_nodes[idx], p)

    def can_provide_sample(self, batch_size):
        return self.tree.n_entries>batch_size






import gym
import numpy as np
import time
from numpy import savez_compressed
import os
import imageio
import json

# user imports
from old.minerl.minerl_utils import register_env
from minerl.env import spaces

class Recorder():
    def __init__(self, env_name):
        self.env_name = env_name
        self.action_dict = {"attack": [],
                       "back": [],
                       "camera": [],
                       "forward": [],
                       "jump": [],
                       "left": [],
                       "right":[],
                       'sneak':[],
                       "sprint": []}


        self.reward_list = []
        self.recording = []

        self.metadict = {"success": None, "duration_ms": None, "duration_steps": 0, "total_reward": 0, "stream_name": None}

    def create_recorder_dir(self, env_name):
        rec_folder = 'recordings'
        if not os.path.exists(rec_folder):
            os.makedirs(rec_folder)

        path_envname =rec_folder+ '/'+env_name
        if not os.path.exists(path_envname):
            os.makedirs(path_envname)

        subfolders = [ f.name for f in os.scandir(path_envname) if f.is_dir()]
        newsubfolder = str(len(subfolders)+1)

        self.metadict['stream_name'] = newsubfolder

        subfolderpath = path_envname +'/' + newsubfolder
        if not os.path.exists(subfolderpath):
            os.makedirs(subfolderpath)

        return subfolderpath


    def add_step(self, action, reward, frame: np.array):
        for key, value in action.items():
            self.action_dict[key].append(value)

        diff_list = list(set(self.action_dict.keys())- set(action.keys()))

        for key in diff_list:
            self.action_dict[key].append(0)

        self.reward_list.append(reward)
        self.recording.append(frame)

        self.metadict['duration_steps'] += 1
        self.metadict['total_reward'] += reward
        #print(self.metadict)

    def save_recording(self, is_success):
        npz_dict = {}
        for key, value in self.action_dict.items():
            npz_dict['action_'+key] = np.asarray(value, dtype=np.float32)
        npz_dict['reward'] = self.reward_list
        save_path = self.create_recorder_dir(self.env_name)
        savez_compressed(save_path+ '/rendered.npz', **npz_dict)
        imageio.mimwrite(save_path+ '/recording.mp4', self.recording, fps= 20)

        self.metadict['success'] = is_success

        with open(save_path+ '/metadata.json', 'w') as fp:
            json.dump(self.metadict, fp)


class Recording_Agent():
    def __init__(self, env_name):
        self.env_name = env_name
        env_id = register_env(env_name)
        self.env = gym.make(env_id)
        state = self.env.reset()
        state = self.env.reset()
        self.input = False
        self.human_action = self.env.action_space.no_op()
        self.env.render()
        self.env.unwrapped.viewer.window.on_key_press = self.key_press
        self.env.unwrapped.viewer.window.on_key_release = self.key_release
        self.recorder = Recorder(env_name)

    def rollout(self):
        next_state, reward, done, info = self.env.step(self.human_action)
        print(next_state)
        #print(reward)
        self.recorder.add_step(self.human_action, reward, next_state['pov'])
        time.sleep(0.03)
        if done:

            self.recorder.save_recording(reward==1000)
            self.recorder = Recorder(self.env_name)
            state = self.env.reset()

    def key_press(self, key, mod):
        self.human_action = self.env.action_space.no_op()
        self.input = True
        #print(key)

        if key == 119:
            self.human_action['forward'] = 1  # w
        # if key == 97:
        #     self.human_action['left'] = 1  # left
        if key == 32:
            self.human_action['jump'] = 1  # space
            self.human_action['forward'] = 1  # w

        # camera ----> numpad "4 8 6 2"
        value = 10
        if key == 65460:
            self.human_action['camera'] = np.array([0, -value])
        if key == 65464:
            self.human_action['camera'] = np.array([-value, 0])
        if key == 65462:
            self.human_action['camera'] = np.array([0, value])
        if key == 65458:
            self.human_action['camera'] = np.array([value, 0])

    def key_release(self, key, mod):
        self.input = False

    def start_episode(self):
        while 1:
            self.env.render()
            if self.input == True:
                self.rollout()

def main():
    my_recorder = Recording_Agent(env_name='choose')
    my_recorder.start_episode()

if __name__ == '__main__':
    main()

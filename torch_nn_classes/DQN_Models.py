import torch
import torch.nn as nn
from torch.distributions import Categorical
import torch.nn.functional as F


class Flatten(nn.Module):
    def forward(self, input):
        return input.view(input.size(0), -1)

class DQN_Dueling(nn.Module):
    def __init__(self, observation_space, num_actions, dueling):
        super(DQN_Dueling, self).__init__()
        self.dueling = dueling
        in_channels, h, w = observation_space['pov'].shape

        if 'act_hist' in observation_space.spaces:
            act_hist_len = observation_space['act_hist'].shape[0]
        else:
            act_hist_len = 0

        if 'compass' in observation_space.spaces:
            compass_len = observation_space['compass'].shape[0]
        else:
            compass_len = 0

        self.num_actions = num_actions

        self.conv1 = nn.Conv2d(in_channels=in_channels, out_channels=32, kernel_size=8, stride=4)
        self.conv2 = nn.Conv2d(in_channels=32, out_channels=64, kernel_size=4, stride=2)
        self.conv3 = nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, stride=1)

        def conv2d_size_out(size, kernel_size=8, stride=4):
            return (size - (kernel_size - 1) - 1) // stride + 1

        convw = conv2d_size_out(conv2d_size_out(conv2d_size_out(w, 8, 4), 4, 2), 3, 1)
        convh = conv2d_size_out(conv2d_size_out(conv2d_size_out(h, 8, 4), 4, 2), 3, 1)
        linear_input_size = (convw *convh) * 64

        # self.fc0 = nn.Linear(in_features=linear_input_size + act_hist_len + compass_len +1, out_features=128) # +1 for is_carrying

        self.fc1_adv = nn.Linear(in_features=linear_input_size + act_hist_len + compass_len +1, out_features=128)
        self.fc2_adv = nn.Linear(in_features=128, out_features=num_actions)


        if self.dueling:
            self.fc1_val = nn.Linear(in_features=linear_input_size + act_hist_len + compass_len +1, out_features=128)
            self.fc2_val = nn.Linear(in_features=128, out_features=1)
        
        self.relu = nn.ReLU()
        # print(self)

    def forward(self, x):
        x_pov = x['pov']
        x_pov = self.relu(self.conv1(x_pov))
        x_pov = self.relu(self.conv2(x_pov))
        x_pov = self.relu(self.conv3(x_pov))
        x_pov = x_pov.reshape(x_pov.size(0), -1)
        x_tot = x_pov

        if 'act_hist' in x.keys():
            act_hist = x['act_hist']
            x_tot = torch.cat((x_tot, act_hist), 1)

        if 'compass' in x.keys():
            compass = x['compass']
            x_tot = torch.cat((x_tot, compass), 1)

        # print(x['carrying'].shape)
        if 'carrying' in x.keys():
            carrying = x['carrying']
            x_tot = torch.cat((x_tot, carrying), 1)

        # x_tot = self.relu(self.fc0(x_tot))
        adv = self.relu(self.fc1_adv(x_tot))
        adv = self.fc2_adv(adv)

        if self.dueling:
            val = self.relu(self.fc1_val(x_tot))
            val = self.fc2_val(val)
            x = val + adv - adv.mean(1, keepdim=True)
        else:
            x = adv
        return x

    def get_num_learnable_paramaters(self):
        return sum(p.numel() for p in self.parameters() if p.requires_grad)



    # def get_weights_biases_grads(self)->dict:
    #     return {}


# class DQN_Simple(nn.Module):
#     def __init__(self, obs_space, no_actions):
#         super().__init__()
#         self.device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
#         o_shp = obs_space['pov'].shape
#         self.fc1 = nn.Linear(in_features=o_shp[0] * o_shp[1] * o_shp[2], out_features=5)
#         # self.fc2 = nn.Linear(in_features=24, out_features=32)
#         self.out = nn.Linear(in_features=5, out_features=no_actions)
#
#     def forward(self, t):
#         t = t['pov'].to(self.device)
#         t = t.flatten(start_dim=1)
#         t = F.relu(self.fc1(t))
#         # t = F.relu(self.fc2(t))
#         t = self.out(t)
#         return t
#
#     def get_weights_biases_grads(self)->dict:
#         return {'linear1.bias': self.fc1.bias, 'linear1.weight': self.fc1.weight,
#                 'linear1.weight.grad': self.fc1.weight.grad, 'linear2.bias': self.fc1.bias,
#                 'linear2.weight': self.fc1.weight,
#                 'linear2.weight.grad': self.fc1.weight.grad}


from matplotlib.pyplot import imshow
# class DQN_Dueling(nn.Module):
#     def __init__(self, observation_space, num_actions):
#         super(DQN_Dueling, self).__init__()
#         self.device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
#         hw = observation_space['pov'].shape[0]
#         in_channels = observation_space['pov'].shape[2]
#
#         self.num_actions = num_actions
#
#         self.conv1 = nn.Conv2d(in_channels=in_channels, out_channels=32, kernel_size=8, stride=4)
#         self.conv2 = nn.Conv2d(in_channels=32, out_channels=64, kernel_size=4, stride=2)
#         self.conv3 = nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, stride=1)
#
#         def conv2d_size_out(size, kernel_size=8, stride=4):
#             return (size - (kernel_size - 1) - 1) // stride + 1
#
#         convdim = conv2d_size_out(conv2d_size_out(conv2d_size_out(hw, 8, 4), 4, 2), 3, 1)
#         linear_input_size = (convdim **2) * 64
#
#         #self.fc0 = nn.Linear(in_features=linear_input_size, out_features=512)
#
#         self.fc1_adv = nn.Linear(in_features=linear_input_size, out_features=512)
#         self.fc1_val = nn.Linear(in_features=linear_input_size, out_features=512)
#
#         self.fc2_adv = nn.Linear(in_features=512, out_features=num_actions)
#         self.fc2_val = nn.Linear(in_features=512, out_features=1)
#
#         self.relu = nn.ReLU()
#
#     def forward(self, x):
#         x = x['pov'].to(self.device)
#         # print(x)
#         x = self.relu(self.conv1(x))
#         x = self.relu(self.conv2(x))
#         x = self.relu(self.conv3(x))
#         x = x.reshape(x.size(0), -1)
#        # x = self.relu(self.fc0(x))
#         adv = self.relu(self.fc1_adv(x))
#         val = self.relu(self.fc1_val(x))
#
#         adv = self.fc2_adv(adv)
#         val = self.fc2_val(val)
#
#         # print(adv)
#         # print(val)
#
#         x = val + adv - adv.mean(1, keepdim=True)
#         return x
#
#     def get_weights_biases_grads(self)->dict:
#         return {}

# class DQN(nn.Module):
#     def __init__(self, observation_space, no_actions):
#         super().__init__()
#         self.device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
#         self.my_net = nn.Sequential(
#             nn.Conv2d(in_channels=3, out_channels=32, kernel_size=8, stride=4),
#             nn.ReLU(),
#             nn.Conv2d(in_channels=32, out_channels=64, kernel_size=4, stride=2),
#             nn.ReLU(),
#             nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, stride=1),
#             nn.ReLU(),
#             Flatten(),
#             nn.Linear(in_features=4 * 4 * 64, out_features=150),
#             nn.ReLU(),
#             nn.Linear(in_features=150, out_features=no_actions),
#             nn.ReLU()  # e of (-1, 1) -> scale to (-10, 10)
#         )
#
#     def forward(self, t):
#         t = t['pov'].to(self.device)
#         return self.my_net(t)

# class DQN_1(nn.Module):
#     def __init__(self, observation_space, no_actions):
#         super().__init__()
#         self.my_net = nn.Sequential(
#             nn.Conv2d(in_channels=1, out_channels=32, kernel_size=3, stride=1, padding=1),
#             nn.ReLU(),
#             nn.Conv2d(in_channels=32, out_channels=64, kernel_size=3, stride=1, padding=1),
#             nn.ReLU(),
#             nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, stride=1, padding=1),
#             nn.ReLU(),
#             Flatten(),
#             nn.Linear(in_features=16 * 16 * 64, out_features=150),
#             nn.ReLU(),
#             nn.Linear(in_features=150, out_features=no_actions),
#             nn.ReLU()  # e of (-1, 1) -> scale to (-10, 10)
#         )
#
#     def forward(self, t):
#         return self.my_net(t)
#
#
# class Linear_Layer(nn.Module):
#     def __init__(self, input_size, output_size):
#         super().__init__()
#         self.my_linear_layer = nn.Linear(in_features=input_size, out_features=output_size)
#
#     def forward(self, t):
#         return self.my_linear_layer(t)


# class DQN(nn.Module):
#     def __init__(self, observation_space, actions=3):
#         in_channels, h, w = observation_space['pov'].shape
#
#         super(DQN, self).__init__()
#         self.device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
#
#         self.conv1 = nn.Conv2d(in_channels=in_channels, out_channels=32, kernel_size=8, stride=4)
#         self.conv2 = nn.Conv2d(in_channels=32, out_channels=64, kernel_size=4, stride=2)
#         self.conv3 = nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, stride=1)
#
#         def conv2d_size_out(size, kernel_size=8, stride=4):
#             return (size - (kernel_size - 1) - 1) // stride + 1
#
#         convdim = conv2d_size_out(conv2d_size_out(conv2d_size_out(hw, 8, 4), 4, 2), 3, 1)
#         linear_input_size = (convdim **2) * 64
#
#         self.l1 = nn.Linear(linear_input_size, 512)
#         self.head = nn.Linear(512, actions)
#
#         self.relu = nn.ReLU()
#
#     def forward(self, x):
#         x = x['pov']
#         x = self.relu(self.conv1(x))
#         x = self.relu(self.conv2(x))
#         x = self.relu(self.conv3(x))
#         x = x.reshape(x.size(0), -1)
#         x = self.relu(self.l1(x))
#         return self.head(x)
#
#     def get_weights_biases_grads(self)->dict:
#         return {}


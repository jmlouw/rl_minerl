import numpy as np
import pandas as pd
import csv

# user imports
from utils.gen_args import Arguments
from utils.utils import create_dir


class Moving_Average:
    def __init__(self, length=10):
        from collections import deque
        self.deq = deque()
        self.length = length

    def getMovingAvg(self, new_value):
        self.deq.append(new_value)
        if len(self.deq) > self.length:
            self.deq.popleft()
            return np.sum(self.deq) / len(self.deq)
        else:
            return 0

class Logger:
    def __init__(self, results_dir, run_num, args: Arguments):
        """
        logger for non-distributed experiments
        logger logs for each run of the experiment
        """
        self.no_runs = args.num_runs
        self.episode_return = 0
        self.episode_duration = 0
        self.total_success_episodes = 0
        self.result_run_path = create_dir([results_dir, str(run_num)])

        # create files to save results and statistics
        with open(self.result_run_path / "stats.csv", "w") as fd:
            writer = csv.writer(fd)
            writer.writerow(['name', 'value'])

        self.csv_return_learning_steps = self.result_run_path / 'return_learningsteps.csv'
        df = pd.DataFrame(columns=['learning_step', 'return', 'seconds'])
        df.set_index('learning_step', inplace=True)
        df.to_csv(self.csv_return_learning_steps)

        self.csv_return_episodes = self.result_run_path / 'return_episodes.csv'
        df = pd.DataFrame(columns=['episode', 'return', 'seconds'])
        df.set_index('episode', inplace=True)
        df.to_csv(self.csv_return_episodes)

        self.csv_duration_episodes = self.result_run_path / 'duration_episodes.csv'
        df = pd.DataFrame(columns=['episode', 'duration', 'seconds'])
        df.set_index('episode', inplace=True)
        df.to_csv(self.csv_duration_episodes)

        self.csv_mean_sampled_reward = self.result_run_path / 'mean_sampled_reward.csv'
        df = pd.DataFrame(columns=['learning_step', 'mean_sampled_reward', 'seconds'])
        df.set_index('learning_step', inplace=True)
        df.to_csv(self.csv_mean_sampled_reward)

    def log_stat(self, stat_name, stat_value):
        with open(self.result_run_path / 'stats.csv', 'a') as fd:
            writer = csv.writer(fd)
            writer.writerow([stat_name, stat_value])

    def after_episode(self, cur_episode, cur_learning_step, episode_return, episode_duration, seconds):
        # return over learning steps
        pd.DataFrame({'return': episode_return, 'seconds':seconds}, index=[cur_learning_step]).to_csv(self.csv_return_learning_steps, mode='a',
                                                                         header=False, index=True)
        # return over episodes
        pd.DataFrame({'return':episode_return, 'seconds':seconds}, index=[cur_episode]).to_csv(self.csv_return_episodes, mode='a', header=False,
                                                                   index=True)
        # duration over episodes
        pd.DataFrame({'duration': episode_duration,'seconds':seconds}, index=[cur_episode]).to_csv(self.csv_duration_episodes, mode='a', header=False,
                                                                     index=True)

    def log_mean_sampled_reward(self, learning_step, mean_sampled_reward, seconds):
        pd.DataFrame({'mean_sampled_reward': mean_sampled_reward, 'seconds': seconds}, index=[learning_step]).to_csv(self.csv_mean_sampled_reward, mode='a', header=False,
                                                                     index=True)

    def after_run(self):
        pass

    def after_experiment(self, experiment_duration):
        pass

if __name__ == '__main__':
    args = Arguments()
    args.parse_args()
    logger = Logger(args.exp_result_dir,1, args)
    logger.log_stat('preload_time', 1000)
    logger.log_stat('learning_time', 20302)
    logger.log_stat('evaluation_time', 221324)
    logger.after_episode(1, 300, 0.9, 130)
    logger.after_episode(2, 500, 0.1, 230)
    print(args)


        # self.experiment_duration = Timer()
        # self.experiment_duration.start()
        # self.gym_type = args.gym_type
        # self.agent_name = args.agent_name
        # self.level_name = args.level_name
        # self.test_after_run = args.test_after_run
        # self.total_steps = 0
        # self.episode_count = 0
        # self.num_episodes = args.num_episodes
        # self.num_test_episodes = args.num_test_episodes
        # if args.experiment_name == 'default':
        #     self.experiment_name = '{agent:}_{date:%B%d_%H-%M}'.format(date=datetime.datetime.now(),
        #                                                                agent=args.agent_name)
        # else:
        # self.experiment_name = args.experiment_name

        # self.tb = SummaryWriter(log_dir= Path('runs')/(self.level_name+'_'+self.experiment_name + '_run_0'))
        #     self.text_to_details(specs)
        #     with open(self.result_folder_path/'specifications.json', 'w') as fp:
        #         env_specs.pop('level_name')
        #         json.dump({**env_specs, **agent_specs}, fp)
        #
        # self.agent_specs = agent_specs
    # def default_value_train(self):
    #     empty_rewards = np.empty((self.num_episodes,))
    #     empty_rewards[:] = np.nan
    #     return empty_rewards
    # def default_value_test(self):
    #     empty_rewards = np.empty((self.num_test_episodes,))
    #     empty_rewards[:] = np.nan
    #     return empty_rewards
    # def general(self, weights_biases_grads: dict):
    #     for key, value in weights_biases_grads.items():
    #         self.tb.add_histogram(key, value, self.episode_count)

    # def plot_individual_run(self, run, tests):
    #     plt.figure(figsize=self.fig_size)
    #     plt.tight_layout(rect=[0,0,.8,2])
    #     axes = plt.gca()
    #     axes.set_ylim([-1, 1])
    #     axes.set_xlim([0, self.num_episodes-1])
    #     plt.yticks(np.arange(-1, 1.1, step=0.1))
    #     plt.xticks(np.arange(0, self.num_episodes, step=100))
    #     plt.grid()
    #     plt.plot(run, color='r', linewidth=2)
    #     plt.axhline(tests.mean(), color= 'b', linewidth = 2)
    #     plt.legend(['return', 'mean_greedy_return'], loc='best')
    #     plt.title(f'{self.experiment_name}: Total Return per Episode (run: {self.current_run_num+1})', fontsize=self.font_size)
    #     plt.figtext(0.5, 0.005, str(self.agent_specs)[1:-1], wrap=True, horizontalalignment='center', fontsize=self.font_size)
    #     plt.xlabel('Episode', fontsize=self.font_size)
    #     plt.ylabel('Return per episode', fontsize=self.font_size)
    #     plt.savefig(self.result_folder_path / 'individual_runs'/f'run_{self.current_run_num+1}.pdf')
    #
    # def plot_mean_returns(self):
    #     return_df = self.result_reward_dframe
    #     greedy_df = self.test_return_dframe
    #     mean_test_return = greedy_df.mean(axis = 0).mean()
    #     plt.figure(figsize=self.fig_size)
    #     axes = plt.gca()
    #     axes.set_ylim([-1, 1])
    #     axes.set_xlim([0, self.num_episodes-1])
    #     plt.yticks(np.arange(-1, 1.1, step=0.1))
    #     plt.xticks(np.arange(0, self.num_episodes, step=100))
    #     plt.grid()
    #     plt.plot(return_df.mean(axis=1), color='r', linewidth=2)
    #     plt.axhline(mean_test_return, color= 'b', linewidth = 2)
    #     plt.legend(['mean', 'mean_greedy_return'], loc='best')
    #     plt.title(f'{self.experiment_name}: Total Return per Episode ({self.no_runs} runs)', fontsize=self.font_size)
    #     plt.figtext(0.5, 0.01, str(self.agent_specs)[1:-1], wrap=True, horizontalalignment='center', fontsize=self.font_size)
    #     plt.xlabel('Episode', fontsize=self.font_size)
    #     plt.ylabel('Return per episode', fontsize=self.font_size)
    #     plt.savefig(self.result_folder_path / 'rewards.pdf')
    #
    # def plot_diff_experiments(self):
    #     pass
    #
    # def result_timing_dict(self):
    #     df_rewards = self.result_reward_dframe
    #     df_durations = self.result_duration_dframe
    #     longest_episode_per_run = list(df_durations.max())
    #     shortest_episode_per_run = list(df_durations.min())
    #     steps_per_run = list(df_durations.sum())
    #     total_steps = sum(steps_per_run)
    #
    #     max_return_per_run = list(df_rewards.max())
    #     min_return_per_run = list(df_rewards.min())
    #     avg_return_per_run = [round(x,5) for x in list(df_rewards.mean())]
    #
    #     step_percentage = (self.total_step_time / (self.total_step_time + self.total_reset_time)) * 100
    #     reset_percentage = (self.total_reset_time / (self.total_step_time + self.total_reset_time)) * 100
    #
    #     dict_results = {'average_return/run': str(avg_return_per_run), 'maximum_return/run': str(max_return_per_run),
    #                     'minimum_return/run': str(min_return_per_run), 'total_steps/run': str(steps_per_run),
    #                     'shortest_episode/run': str(shortest_episode_per_run),
    #                     'longest_episode/run': str(longest_episode_per_run)}
    #
    #     dict_timings = {'num_times_stepped': str(np.round(total_steps)),
    #                     'total_time_stepped': str(datetime.timedelta(seconds=round(self.total_step_time))),
    #                     'avg_time/step': str(np.round((self.total_step_time / total_steps), 5)),
    #                     '%_time_stepped': str(np.round(step_percentage, 2)),
    #                     'num_times_reset': str(np.round(self.reset_count)),
    #                     'total_time_reset': str(datetime.timedelta(seconds=round(self.total_reset_time))),
    #                     'avg_time/reset': str(
    #                         np.round((self.total_reset_time / (self.num_episodes * self.no_runs)), 2)),
    #                     '%_time_reset': str(np.round(reset_percentage, 2)),
    #                     'experiment_duration': str(datetime.timedelta(seconds=round(self.experiment_duration.elapsed)))}
    #
    #     return dict_results, dict_timings
    #
    # def text_to_details(self, text):
    #     file = open(self.result_folder_path / 'info.txt', 'a')
    #     file.write(text)
    #     file.close()

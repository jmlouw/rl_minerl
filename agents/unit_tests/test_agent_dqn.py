from unittest import TestCase
from gym.spaces import Discrete, Dict, Box
import numpy as np
import torch
from collections import namedtuple

# user imports
from agents.agent_dqn import DQN_Agent, extract_tensors
from utils.gen_args import Arguments
from utils.utils import to_tensor

from agents.agent_dqn import Experience


# Experience = namedtuple('Experience', ('state', 'action', 'next_state', 'reward'))


class TestDQN_Agent(TestCase):
    @classmethod
    def setUpClass(cls):
        args = Arguments()
        args.batch_size =4
        cls.action_space = Discrete(3)
        cls.obs_space = Dict({'pov': Box(
            low=0,
            high=255,
            shape=(64, 64, 4),
            dtype='uint8'
        )})
        cls.test_agent = DQN_Agent(cls.action_space, cls.obs_space, args)
        state_0 = {'pov': np.zeros(cls.obs_space['pov'].shape, dtype=np.float64)}
        state_1 = {'pov': np.ones(cls.obs_space['pov'].shape, dtype=np.float64)}
        state_2 = {'pov': 2*np.ones(cls.obs_space['pov'].shape, dtype=np.float64)}
        state_3 = {'pov': 3*np.ones(cls.obs_space['pov'].shape, dtype=np.float64)}

        cls.experiences = [Experience(state_0, 0, 1, state_0, True),
                           Experience(state_1, 1, 0, state_1, False),
                           Experience(state_2, 2, 0, state_2, False),
                           Experience(state_3, 1, 1, state_3, True)]  # batch of experiences size 2

        cls.test_agent.update_agent(state_1, 0, 0, state_2, True, 1000)

    def test_string_add_agentspecs(self):
        self.fail()

    def test_update_agent(self):
        self.fail()

    def test_select_action(self):
        self.fail()

    def test_get_current(self):
        states, actions, rewards, next_states, dones = self.test_agent.extract_tensors(self.experiences)
        print(self.test_agent.get_current(states, actions))

    def test_get_next(self):
        states, actions, rewards, next_states, dones = self.test_agent.extract_tensors(self.experiences)
        print(self.test_agent.get_next(next_states, dones))

    def test_get_policy_q_values(self):
        states, actions, rewards, next_states, dones = self.test_agent.extract_tensors(self.experiences)
        print("STATES")
        print(states)

        print("NEXT STATES")
        print(next_states)

        print("REWARDS")
        print(rewards)

        print("DONES")
        print(dones)

        print("ACTIONS")
        print(actions)

        print(self.test_agent.get_next(next_states, dones))

    def test_save_model(self):
        self.test_agent.save_model('test_gym', 'test_level', 'test_agent_name', 10, 120, 5, 'tester')

    def test_load_model(self):
        self.test_agent.save_model('test_gym', 'test_level', 'test_agent_name', 10, 120, 5, 'tester')
        print(self.test_agent.load_model_training('test_gym', 'test_level', 'test_agent_name', 'tester'))

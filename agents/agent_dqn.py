import copy
from collections import namedtuple
import torch
import torch.optim as optim
import torch.nn.functional as F
import numpy as np
from pathlib import Path
from ttictoc import Timer

# user imports
from utils.utils import to_tensor, unique_path, create_dir, nn_dict, state_np2tensor
from agents.agent_base_q import BaseQAgent
from agents.replay_buffer import ReplayBuffer, PrioritisedReplayBuffer
from utils.gen_args import Arguments

Experience = namedtuple('Experience', ('state', 'action', 'reward', 'next_state', 'done'))
#Data = namedtuple('Data', ('pov', 'direction'))

# nn_dict = {'DQN': DQN,
#            'DQN_Dueling': DQN_Dueling,
#            'DQN_Simple': DQN_Simple, }

class DQN_Agent(BaseQAgent):
    def __init__(self, action_space, observation_space, args: Arguments):
        super().__init__(action_space, observation_space, args)
        #seed_things(1)
        self.device = args.device
        self.batch_size = args.batch_size
        self.target_update = args.target_update
        self.prioritised_replay = args.prioritised_replay
        self.double = args.double_dqn
        self.epsilon = args.epsilon

        if self.prioritised_replay:
            self.replay_buffer = PrioritisedReplayBuffer(args.replay_buffer_capacity)
        else: self.replay_buffer = ReplayBuffer(args.replay_buffer_capacity)

        self.model_name = args.nn_model
        nn_model = nn_dict[args.nn_model]
        self.q_net = nn_model(observation_space, action_space.n).to(args.device)
        self.q_target_net = nn_model(observation_space, action_space.n).to(args.device)
        self.q_target_net.load_state_dict(self.q_net.state_dict())
        self.q_target_net.eval()
        self.optimizer = optim.Adam(params=self.q_net.parameters(), lr=self.lr)


        # self.agent_specs = {**self.agent_specs, 'nn_model': self.model_name, 'target_update_step': self.target_update,
        #                     'batch_size': self.batch_size, 'replay_size': self.replay_buffer._maxsize, 'prioritised_replay': self.prioritised_replay, 'double': self.double}


    def add_to_buffer(self, state, action, reward, next_state, done):
        data = copy.deepcopy(Experience(state, action, reward, next_state, done))
        self.replay_buffer.add(data)

    def update_agent(self, state, action, reward, next_state, done, total_steps):
        data = copy.deepcopy(Experience(state, action, reward, next_state, done))
        self.replay_buffer.add(data)

        mean_rewards_sampled = 0

        if self.replay_buffer.can_provide_sample(self.batch_size):
            if self.prioritised_replay:
                experiences, idxes, is_weights = self.replay_buffer.sample(self.batch_size)
            else:
                experiences = self.replay_buffer.sample(self.batch_size)

            states, actions, rewards, next_states, dones = self.extract_tensors(experiences)
            mean_rewards_sampled = np.mean(rewards.cpu().detach().numpy())
            current_q_values = self.get_current(states, actions)
            if self.double:
                next_q_values = self.get_next_double(next_states, dones)
            else:
                next_q_values = self.get_next(next_states, dones)

            target_q_values = (next_q_values * torch.tensor(self.gamma).to(self.device)) + rewards.to(self.device)

            if self.prioritised_replay:
                td_errors = (target_q_values.unsqueeze(1) - current_q_values).cpu().detach().numpy()
                self.replay_buffer.update_priorities(idxes, td_errors)
                loss = F.smooth_l1_loss(current_q_values.squeeze(), target_q_values.detach(), reduction='none')*torch.from_numpy(is_weights).to(self.device)
                loss = loss.mean()
            else:
                loss = F.smooth_l1_loss(current_q_values.squeeze(), target_q_values.detach())

            self.optimizer.zero_grad()
            loss.backward()
            for param in self.q_net.parameters():
                param.grad.data.clamp_(-1, 1)
            self.optimizer.step()

        if total_steps % self.target_update == 0:
            self.q_target_net.load_state_dict(self.q_net.state_dict())

        return mean_rewards_sampled


    def select_action(self, state):
        state = self.state_np2tensor(state)
        if self.epsilon > np.random.random():
            action = np.random.randint(self.action_space.n)
            return action
        else:
            with torch.no_grad():
                return self.q_net(state).argmax(dim=1).item()

    def get_current(self, states, actions):
        actions = actions.to(self.device)
        return self.q_net(states).gather(dim=1, index=actions.unsqueeze(-1))

    def get_next(self, next_states, dones):
        final_state_locations = dones
        non_final_state_locations = np.logical_not(final_state_locations)
        non_final_states = {}
        for key in self.observation_space.spaces:
            non_final_states[key] = next_states[key][non_final_state_locations]

        batch_size = self.batch_size
        values = torch.zeros(batch_size).to(self.device)
        values[non_final_state_locations] = self.q_target_net(non_final_states).max(dim=1)[0].detach()
        return values

    def get_next_double(self, next_states, dones):
        with torch.no_grad():
            final_state_locations = dones
            non_final_state_locations = np.logical_not(final_state_locations)
            non_final_states = {}
            for key in self.observation_space.spaces:
                non_final_states[key] = next_states[key][non_final_state_locations]

            batch_size = self.batch_size
            values = torch.zeros(batch_size).to(self.device)
            next_state_argmax_actions = self.q_net(non_final_states).argmax(dim=1)

            values[non_final_state_locations] = self.q_target_net(non_final_states).gather(dim=1,
                                                                                           index=next_state_argmax_actions.unsqueeze(
                                                                                             -1)).squeeze(1)
        return values

    def state_np2tensor(self, state):
        new_state = state.copy()
        new_state['pov'] = to_tensor(state['pov']).unsqueeze(0).float()
        if 'act_hist' in new_state.keys():
            new_state['act_hist'] = torch.tensor(state['act_hist']).unsqueeze(0).float()
        return new_state

    def extract_tensors(self, experiences):
        # Convert batch of Experiences to Experience of batches
        batch = Experience(*zip(*experiences))
        states = {}
        next_states = {}
        for key in self.observation_space.spaces:
            states[key] = [dict[key] for dict in batch.state]
            next_states[key] = [dict[key] for dict in batch.next_state]
            if key == 'pov':
                states[key] = to_tensor(np.stack(states[key])).float()
                next_states[key] = to_tensor(np.stack(next_states[key])).float()
            else:
                states[key] = torch.tensor(np.stack(states[key])).float()
                next_states[key] = torch.tensor(np.stack(next_states[key])).float()

        actions = torch.tensor(np.stack(batch.action))
        rewards = torch.tensor(np.stack(batch.reward)).float()
        dones = list(batch.done)
        return (states, actions, rewards, next_states, dones)

    def get_q_net_state_dict(self):
        """
        returns the current weights of the neural-network
        """
        q_net_state_dict = self.q_net.cpu().state_dict()
        self.q_net = self.q_net.to(self.device)
        return  q_net_state_dict

    def get_weights_biases_grads(self) -> {}:
        return self.q_net.get_weights_biases_grads()

    def save_model(self, path, episode, step, model_file_name):
        file_path = path / (model_file_name + '.pt')
        model_dict = {
            'nn_name': self.model_name,
            'episode': episode,
            'step': step,
            'model_state_dict': self.q_net.state_dict(),
            'optimizer_state_dict': self.optimizer.state_dict(),
        }
        torch.save(model_dict, file_path)

    def load_model_training(self, gym_type, level_name, agent_name, model_file_name):
        PATH = Path(__file__).parent.parent / 'trained_models' / gym_type / level_name / agent_name / (
                model_file_name + '.pt')
        checkpoint = torch.load(PATH)
        self.q_net.load_state_dict(checkpoint['model_state_dict'])
        self.q_target_net.load_state_dict(checkpoint['model_state_dict'])
        self.optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        # self.replay_buffer = checkpoint['replay_memory']
        return checkpoint['run'], checkpoint['episode'], checkpoint['step']


    @classmethod
    def load_model_inference(cls, path_to_model, observation_space, action_space, dueling):
        # PATH = Path(__file__).parent.parent / 'trained_models' / gym_type / level_name / agent_name / (
        #         stored_model_name + '.pt')
        root = Path(__file__).parent.parent
        PATH = Path.joinpath(root,path_to_model)
        print(PATH)

        checkpoint = torch.load(PATH)
        nn_model = nn_dict[checkpoint['nn_name']]

        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        policy_net = nn_model(observation_space, action_space.n, dueling)
        policy_net.load_state_dict(checkpoint['model_state_dict'])
        return policy_net.to(device)

    @classmethod
    def test_select_action(cls, state, model, action_space, rate, device):
        state = state_np2tensor(state, device)
        if rate > np.random.random():
            action = np.random.randint(action_space.n)
            return action
        else:
            with torch.no_grad():
                return model(state).argmax(dim=1).item()

def seed_things(seed):
    np.random.seed(seed)
    torch.manual_seed(seed)
    if torch.cuda.is_available():
        torch.cuda.manual_seed(seed)

# def extract_tensors(experiences):
#     # Convert batch of Experiences to Experience of batches
#     batch = Experience(*zip(*experiences))
#     states = to_tensor(np.stack(batch.state))
#     actions = torch.tensor(np.stack(batch.action))
#     rewards = torch.tensor(np.stack(batch.reward))
#     next_states = to_tensor(np.stack(batch.next_state))
#     dones = torch.tensor(np.stack(batch.done))
#     return (states, actions, rewards, next_states, dones)


import random
import numpy as np
#np.seterr(all='raise')
from time import sleep

# user imports
from agents.agent_base_q import BaseQAgent
from utils.gen_args import Arguments
from wrappers.minerl_env_wrappers import LFA_Observation_Wrapper


class Agent_LFA(BaseQAgent):
    def __init__(self, action_space, observation_space, args: Arguments):
        super().__init__(action_space, observation_space, args)

        self.no_features = observation_space.spaces['pov'].shape[0]**2
        req_alpha = 1/self.no_features
        if self.alpha > req_alpha:
            print(f'Alpha, {self.alpha}, not smaller than 1/no_featuers, {req_alpha}. Alpha is now = {req_alpha}')
            self.alpha = req_alpha


        self.no_values = 3

        self.weights = np.zeros((self.no_features * self.no_actions * self.no_values))
        self.e_t_enabled = args.eligibility_traces
        self.e_trace_vector = np.zeros((self.no_features * self.no_actions * self.no_values))

        self.lam = args.lam

    def add_agent_specs_dict(self):
        add_specs = (f'LFA Agent:\n'
                     f'Number of features: {self.no_features}\n'
                     f'Number of values per feature: {self.no_values}\n'
                     f'Number of weights: {len(self.weights)}\n'
                     f'Eligibility Traces: {self.e_t_enabled}\n')

        if self.e_t_enabled:
            add_specs+= (f'Lambda: {self.lam}\n')

        return add_specs

    def zero_e_trace(self):
        self.e_trace_vector = np.zeros_like(self.e_trace_vector)

    def get_S_feature(self, s):
        s_flatten = s.flatten()
        state_feature = np.zeros((len(s_flatten), self.no_values))
        for index, value in enumerate(s_flatten):
            state_feature[index, value] = 1
        return state_feature

    def get_SA_feature(self, s, a) -> np.array:
        sa_feature = np.zeros((self.no_actions, self.no_features, self.no_values))
        sa_feature[a, :, :] = self.get_S_feature(s)
        return sa_feature

    def get_approx_q(self, s, a):
        """

        :param s:
        :param a:
        :return: a single q-value for the given action and state
        """
        return self.get_SA_feature(s, a).flatten().dot(self.weights)

    def get_SA_feature_(self, s) -> np.array:
        sa_feature = np.zeros((self.no_actions, self.no_features * self.no_actions * self.no_values))
        for a in range(self.no_actions):
            temp = np.zeros((self.no_actions, self.no_features, self.no_values))
            temp[a, :, :] = self.get_S_feature(s)
            sa_feature[a] = temp.flatten()

        return sa_feature

    def get_approx_q_(self, s):
        """

        :param s:
        :return: vector of q values for all actions in a given state
        """
        return self.get_SA_feature_(s).dot(self.weights)

    def update_agent(self, state, action, next_state, reward, total_steps):
        state = state['pov'].squeeze()
        next_state = next_state['pov'].squeeze()
        current_q = self.get_approx_q(state, action)
        if np.array_equal(next_state, np.zeros_like(next_state)):
            next_q_value = 0
        else:
            next_q_value = np.max(self.get_approx_q_(next_state))

        if self.e_t_enabled:
            delta = (reward + self.gamma * next_q_value - current_q)
            self.e_trace_vector = self.gamma*self.lam*self.e_trace_vector + self.get_SA_feature(state, action).flatten()
            self.weights += self.alpha * delta * self.e_trace_vector
        else:
            deltaw = (self.alpha * (reward + self.gamma * next_q_value - current_q) * (self.get_SA_feature(state, action).flatten()))
            self.weights += deltaw


    def select_action(self, state, env, rate):
        state = state['pov'].squeeze()
        if rate > random.random():
            return self.action_space.sample()
        else:
            return np.argmax(self.get_approx_q_(state))

    def episode_done(self):
        if self.e_t_enabled:
            self.zero_e_trace()

    @staticmethod
    def wrap_env(env, args: Arguments):
        env  = LFA_Observation_Wrapper(env, args.downsampled_dimensions)
        return env

import math
from utils.gen_args import Arguments


class BaseQAgent:
    def __init__(self, action_space, observation_space, args: Arguments):
        self.action_space = action_space
        self.observation_space = observation_space
        self.no_actions = action_space.n
        self.gamma = args.gamma
        self.lr = args.learning_rate

        # self.epsilon_start = args.eps_start
        # self.epsilon_end = args.eps_end
        # self.epsilon_decay = args.eps_decay

        # self.agent_specs = {
        #     'epsilon': f'(start: {self.epsilon_start}, end: {self.epsilon_end}, decay: {self.epsilon_decay})',
        #     'learning_rate': str(self.lr),
        #     'discount': str(self.gamma)}

    def update_agent(self, state, action, reward, next_state, done, total_steps):
        raise NotImplementedError

    def select_action(self, state):
        raise NotImplementedError

    @classmethod
    def test_select_action(cls, state, model, action_space, rate, device):
        pass

    # def get_exploration_rate(self, current_step):
    #     #    return 0.1
    #     return self.epsilon_end + (self.epsilon_start - self.epsilon_end) * math.exp(
    #         -1. * current_step * self.epsilon_decay)

    def get_weights_biases_grads(self):
        raise NotImplementedError

    def episode_done(self):
        pass

    def load_model(self, gym_env_name, level_name, agent_name, stored_model_name):
        pass

    def save_model(self, path, episode, step, model_file_name):
        pass

    @staticmethod
    def wrap_env(env, args: Arguments):
        """
        Env wrapped with agent specific wrappers
        :param env:
        :param args:
        :return:
        """
        return env

    def add_to_buffer(self, state, action, reward, next_state, done):
        pass

from ttictoc import Timer
import copy
import wandb
import torch
import pandas as pd
import numpy as np

# user imports
from agents.agent_dqn import DQN_Agent
from utils.gen_args import Arguments
from agents.logger import Logger
from utils.utils import make_miniworld_env, agents, create_dir, nn_dict, state_np2tensor


class Actor_Eval():
    def __init__(self, num_evaluation_episodes, run_num, args: Arguments):

        self.eval_dir = args.eval_actor_dir
        self.num_eval_episodes = num_evaluation_episodes
        self.env = make_miniworld_env(args)
        nn_model = nn_dict[args.nn_model]
        self.q_network = nn_model(self.env.observation_space, self.env.action_space.n).to(args.device)
        self.q_network.eval()

        self.csv_evaluation = self.eval_dir / f'{run_num}.csv'
        df = pd.DataFrame(columns=['learning_step'] + [str(i) for i in range(args.num_eval_episodes)] + ['seconds'])
        df.set_index('learning_step', inplace=True)
        df.to_csv(self.csv_evaluation)

    def append_result(self, learning_step, results):
        pd.DataFrame(results, index=[learning_step]).to_csv(self.csv_evaluation, mode='a',
                                                            header=False, index=True)

    def test_policy(self, q_network_state_dict, learning_step, seconds):
        self.q_network.load_state_dict(q_network_state_dict)
        eval_returns = {}
        for episode in range(self.num_eval_episodes):
            ep_return = 0
            state = self.env.reset()
            done = False
            while not done:
                action = self.select_action(state)
                next_state, reward, done, info = self.env.step(action)
                ep_return += reward

            eval_returns[str(episode)] = ep_return
        eval_returns['seconds'] = seconds
        self.append_result(learning_step, eval_returns)
        eval_returns.pop('seconds')
        return eval_returns

    def select_action(self, state):
        state = state_np2tensor(state)
        with torch.no_grad():
            return self.q_network(state).argmax(dim=1).item()


class Simulation():
    def __init__(self, action_space, observation_space, run_num, args: Arguments):
        '''
        Class used for the simulation/training of non-distributed agents
        '''

        self.run_num = run_num
        self.save_results = args.save_results
        if args.save_results:
            self.logger = Logger(args.exp_result_dir, run_num, args)
            self.model_dir = create_dir(
                [args.exp_result_dir, str(run_num),
                 'model'])  # creates directory inside results directory to save nn model

        self.preload_size = args.buffer_preload_size

        self.agent: DQN_Agent = agents[args.agent_type](action_space, observation_space, args)
        self.env = make_miniworld_env(args)

        self.save_interval = args.save_interval  # interval the model is saved
        self.env_name = args.env_name
        self.deploy_eval_actor = args.deploy_eval_actor
        self.log_wandb = args.log_wandb

        if args.save_results and args.deploy_eval_actor:
            self.eval_interval = args.eval_interval
            self.num_eval_episodes = args.num_eval_episodes
            self.evaluation_actor = Actor_Eval(args.num_eval_episodes, run_num, args)

        self.sample_time = 0

    def preload_buffer(self):
        """
        prefills the replay buffer with the specified amount of transitions
        """
        print('started to pre-load experience replay buffer')
        preload_timer = Timer()
        state = self.env.reset()
        preload_timer.start()

        for prefill_step in range(self.preload_size):
            action = self.agent.select_action(state)
            next_state, reward, done, info = self.env.step(action)
            self.agent.add_to_buffer(state, action, reward, next_state, done)
            state = copy.copy(next_state)
            if done:
                state = self.env.reset()
        preloadtime = preload_timer.stop()
        if self.save_results:
            self.logger.log_stat('preloadtime', preloadtime)
        if self.log_wandb:
            wandb.log({'preloadtime': preloadtime}, step=0, commit=False)
        print(f'{self.preload_size} transition pre-load time: {preloadtime:.2f} sec')

    def train(self, num_learning_steps):
        '''
        main training loop for non-distributed agents
        '''
        print(f'starting run {self.run_num}')
        run_timer = Timer()
        run_timer.start()
        self.preload_buffer()
        learning_timer = Timer()
        learning_timer.start()
        learning_time = 0
        evaluation_timer = Timer()
        evaluation_time = 0

        state = self.env.reset()

        cur_episode = 0
        cur_learning_step = 0

        episode_return = 0
        episode_duration = 0

        mean_rewards = 0
        while (cur_learning_step < num_learning_steps):

            action = self.agent.select_action(state)
            next_state, reward, done, info = self.env.step(action)
            episode_return += reward
            episode_duration += 1
            cur_learning_step += 1

            mr = self.agent.update_agent(state, action, reward, next_state, done, cur_learning_step)
            mean_rewards += mr
            # if rewards_s is not None:
            #     rewards_sampled += rewards_s
            state = copy.copy(next_state)

            if done:
                if self.log_wandb:
                    wandb.log({'episodic return': episode_return, 'episodic duration': episode_duration},
                              step=cur_learning_step, commit=False)
                if self.save_results:
                    learning_time += learning_timer.stop()
                    learning_timer.start()
                    self.logger.after_episode(cur_episode, cur_learning_step, episode_return,
                                              episode_duration, learning_time)
                state = self.env.reset()
                episode_duration = 0
                episode_return = 0
                cur_episode += 1

            if self.save_results and (cur_learning_step % self.save_interval == 0):
                print(f'saving nn-model @ step {cur_learning_step}')
                self.agent.save_model(self.model_dir, cur_episode, cur_learning_step, 'model')

            # if (cur_learning_step + 1) % self.eval_interval == 0:
            #     # mean_sampled_reward = np.mean(rewards_sampled)
            #     # if self.log_wandb:
            #     #     wandb.log({'mean reward sampled': mean_sampled_reward}, step=cur_learning_step+1, commit=False)
            #     if self.save_results:
            #         self.logger.log_mean_sampled_reward(cur_learning_step, mean_sampled_reward)

            if self.save_results and self.deploy_eval_actor and (cur_learning_step + 1) % self.eval_interval == 0:
                learning_time += learning_timer.stop()
                evaluation_timer.start()
                q_state_dict = self.agent.get_q_net_state_dict()
                eval_result = self.evaluation_actor.test_policy(q_state_dict, cur_learning_step + 1,
                                                                seconds=learning_time)
                mean_eval_return = np.mean(list(eval_result.values()))
                delta_eval_time = evaluation_timer.stop()
                evaluation_time += delta_eval_time
                mean_rewards /= (self.eval_interval)
                self.logger.log_mean_sampled_reward(cur_learning_step+1, mean_rewards, learning_time)
                print(
                    f'step: {cur_learning_step + 1},'
                    f' mean evaluation return: {mean_eval_return:.2f},'
                    f' eval_time: {delta_eval_time:.2f},'
                    f' mean_sampled_reward: {mean_rewards:.4f},'
                    f' learning_time_elapsed: {learning_time:.2f}')
                mean_rewards = 0
                if self.log_wandb:
                    wandb.log({'mean evaluator return': mean_eval_return}, step=cur_learning_step + 1, commit=False)
                learning_timer.start()

        print(f'run {self.run_num} complete')
        run_time = run_timer.stop()

        if self.log_wandb:
            wandb.log({'run_time': run_time}, step=cur_learning_step + 1, commit=False)
            wandb.log({'learning_time': learning_time}, step=cur_learning_step + 1, commit=False)
            wandb.log({'evaluation_time': evaluation_time}, step=cur_learning_step + 1, commit=False)
            wandb.log({'transitions generated': num_learning_steps}, step=cur_learning_step + 1, commit=False)

        if self.save_results:
            self.logger.log_stat('run_time', run_time)
            self.logger.log_stat('learning_time', learning_time)
            self.logger.log_stat('evaluation_time', evaluation_time)
            print('saving final model')
            self.agent.save_model(self.model_dir, cur_episode, cur_learning_step, 'model')

        # def run_experiment(self, agent: agent_base_q, env: gym.Env, args: Arguments):
    #     print('__________________________Live Tracking__________________________')
    #     for run in range(self.no_runs):
    #         print(f'\nRun no: {run}:')
    #         my_agent = agent(env.action_space, env.observation_space, args)
    #         #env.seed(1)
    #         self.train(my_agent, env, run)
    #         if args.test_after_run:
    #             print(f'\nTesting agent from run {run}...')
    #             env.seed(1)
    #             # self.test_agent(my_agent, env, args.num_test_episodes, e_rate=0)
    #         self.tracking.after_run(run)
    #     self.tracking.after_experiment({})

    # def test_agent(self, agent_: BaseQAgent, env, num_test_episodes, e_rate=0):
    #     agent = agent_
    #     state = env.reset()
    #
    #     for current_test_episode in range(num_test_episodes):
    #         return_ = 0
    #         while 1:
    #             action = agent.select_action(state, e_rate)
    #             state, reward, done, info = env.step(action)
    #             return_ += reward
    #
    #             if done:
    #                 print(f'Test episode: {current_test_episode + 1}, return: {np.round(return_, 2)}')
    #                 self.tracking.after_test_episode(current_test_episode, return_)
    #                 state = env.reset()
    #                 break

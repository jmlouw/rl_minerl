import inspect
import gym
from gym_miniworld.miniworld import MiniWorldEnv


from .walkgoal import *
from .navigate import *
from .walkgoalobstacleroom import *
from .fetch import *
from .keydoorballgoal import *
from .choices import *
from .numbers import *
from .lockeddoor import *
from .navigate_multi_room import *
from .fetch_multi_room import *
from .combo_multi_room import *

# Registered environment ids
env_ids = []

def register_envs():
    module_name = __name__
    global_vars = globals()

    # Iterate through global names
    for global_name in sorted(list(global_vars.keys())):
        env_class = global_vars[global_name]

        if not inspect.isclass(env_class):
            continue

        if not issubclass(env_class, gym.core.Env):
            continue

        if env_class is MiniWorldEnv:
            continue

        # Register the environment with OpenAI Gym
        gym_id = 'miniworld-%s-v0' % (global_name)
        entry_point = '%s:%s' % (module_name, global_name)

        gym.envs.registration.register(
            id=gym_id,
            entry_point=entry_point,
        )

        env_ids.append(gym_id)

        #print('Registered env:', gym_id)
register_envs()

import numpy as np
import math
from gym_miniworld.miniworld import MiniWorldEnv, Room
from gym_miniworld.entity import Box, MeshEnt
from gym import spaces

import math
from gym_miniworld.miniworld import MiniWorldEnv, Room

from gym_miniworld.params import DEFAULT_PARAMS
from gym import spaces

class Navigate(MiniWorldEnv):
    """
    Environment in which the goal is to go to a red box
    placed randomly in one big room.
    """

    def __init__(self, num_rooms = None,  ap =5, size=10, max_episode_steps=250, **kwargs):
        assert size >= 2
        self.size = size

        super().__init__(
            max_episode_steps=max_episode_steps,
            **kwargs
        )

        self.reward_range= (-0.001, 1.0)
        self.action_space = spaces.Discrete(ap)

    def _gen_world(self):
        room = self.add_rect_room(
            min_x=0,
            max_x=self.size,
            min_z=0,
            max_z=self.size,wall_tex='stone',
            floor_tex='sandstone_top',ceil_tex = 'sandstone_bottom'
        )
        self.box = self.place_entity(MeshEnt(
            mesh_name='emerald_block/emerald_block', height=1, static=False, check_collisions=False, carry_yaw=1), dir=0)

        self.place_agent()

    def step(self, action):
        obs, reward, done, info = super().step(action)
        reward += -0.001
        info['success'] = False
        if self.near(self.box):
            info['success'] = True
            reward += 1
            done = True
        return obs, reward, done, info

# class OneRoomS6(OneRoom):
#     def __init__(self, max_episode_steps=100, **kwargs):
#         super().__init__(size=6, max_episode_steps=max_episode_steps, **kwargs)
#
# class OneRoomS6Fast(OneRoomS6):
#     def __init__(self, forward_step=0.7, turn_step=45):
#         # Parameters for larger movement steps, fast stepping
#         params = DEFAULT_PARAMS.no_random()
#         params.set('forward_step', forward_step)
#         params.set('turn_step', turn_step)
#
#         super().__init__(
#             max_episode_steps=50,
#             params=params,
#             domain_rand=False
#         )

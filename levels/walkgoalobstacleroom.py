import numpy as np
import math
from gym_miniworld.miniworld import MiniWorldEnv, Room
from gym_miniworld.entity import Box, Box_Rect
from gym import spaces

import math
from gym_miniworld.miniworld import MiniWorldEnv, Room

from gym_miniworld.params import DEFAULT_PARAMS
from gym import spaces

class WalkGoalObstacleRoom(MiniWorldEnv):
    """
    Environment in which the goal is to go to a red box
    placed randomly in one big room.
    """

    def __init__(self, ap =5, size=10, max_episode_steps=1000, **kwargs):
        DEFAULT_PARAMS.set('cam_fov_y', 90, 55, 100)
        DEFAULT_PARAMS.set('cam_pitch', -20, -30, 30)
        assert size >= 2
        self.size = size

        super().__init__(
            max_episode_steps=max_episode_steps,
            **kwargs
        )

        self.reward_range= (-0.1, 1.0)
        self.action_space = spaces.Discrete(ap)

    def _gen_world(self):
        room = self.add_rect_room(
            min_x=0,
            max_x=self.size,
            min_z=0,
            max_z=self.size,wall_tex='stone',
            floor_tex='sandstone_top',ceil_tex = 'sandstone_bottom'
        )

        self.goal = self.place_entity(Box(color='green'))
        goal_x = self.goal.pos[0]
        goal_z = self.goal.pos[2]
        rad = 6
        self.obstacles = []

        for i in range(3):
            self.obstacles.append(self.place_entity(Box_Rect(color='red', size_x=2, size_y= 0.1, size_z=2)))
            # self.obstacles.append(self.place_entity(Box_Rect(color='red', size_x=2, size_y= 0.1, size_z=2),min_x=goal_x-rad ,max_x=goal_x+rad, min_z=goal_z-rad, max_z=goal_z+rad))

        self.place_agent()

    def step(self, action):
        obs, reward, done, info = super().step(action)
        reward += -0.0001
        if self.collide(self.goal):
            # reward = self._reward()
            reward += 1
            done = True

        for obstacle in self.obstacles:
            if self.collide(obstacle):
                reward -=0.1
                done = True
                break

        return obs, reward, done, info

# class OneRoomS6(OneRoom):
#     def __init__(self, max_episode_steps=100, **kwargs):
#         super().__init__(size=6, max_episode_steps=max_episode_steps, **kwargs)
#
# class OneRoomS6Fast(OneRoomS6):
#     def __init__(self, forward_step=0.7, turn_step=45):
#         # Parameters for larger movement steps, fast stepping
#         params = DEFAULT_PARAMS.no_random()
#         params.set('forward_step', forward_step)
#         params.set('turn_step', turn_step)
#
#         super().__init__(
#             max_episode_steps=50,
#             params=params,
#             domain_rand=False
#         )

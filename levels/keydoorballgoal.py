import numpy as np
import math
from gym import spaces
from gym_miniworld.miniworld import MiniWorldEnv, Room
from gym_miniworld.entity import Box, ImageFrame, MeshEnt, Key
from gym_miniworld.params import DEFAULT_PARAMS


class KeyDoorBallGoal(MiniWorldEnv):
    """
    Outside environment with two rooms connected by a gap in a wall
    """

    def __init__(self, **kwargs):
        DEFAULT_PARAMS.set('cam_fov_y', 90, 55, 100)
        self.door_open = False
        super().__init__(
            max_episode_steps=900,
            **kwargs
        )

        # Allow only the movement actions
        self.action_space = spaces.Discrete(5)

    def _gen_world(self):
        # Top
        room0 = self.add_rect_room(
            min_x=-7, max_x=7,
            min_z=0.5, max_z=8,
            wall_tex='stone',
            floor_tex='sandstone_top',
            ceil_tex='sandstone_bottom',
        )
        # Bottom
        room1 = self.add_rect_room(
            min_x=-7, max_x=7,
            min_z=-8, max_z=-0.5,
            wall_tex='stone',
            floor_tex='sandstone_top',
            ceil_tex='sandstone_bottom',
        )

        self.connect_rooms(room0, room1, min_x=-1, max_x=1)

        self.box = self.place_entity(Box(color='green'), room=room1)
        self.door = MeshEnt(
            mesh_name='doom_door', height=2, static=False
        )
        self.key = Key(color='yellow')
        self.place_entity(self.key, room=room0)

        self.place_entity(self.door
                          ,
                          pos=np.array([0, 0, -.5]),
                          dir=np.pi / 2
                          )

        self.place_agent(room=room0)

    def step(self, action):
        obs, reward, done, info = super().step(action)
        # print(self.door)
        # print(self.entities)

        if self.near(self.key) and not self.agent.carrying_right:

            self.agent.carrying_right = self.key

        if self.collide(self.key, self.door) and not self.door_open:

            self.entities.remove(self.door)
            self.entities.remove(self.key)
            self.agent.carrying_right = None
            self.door_open = True


        if self.near(self.box):
            reward += self._reward()
            done = True

        return obs, reward, done, info

    def reset(self):
        self.door_open = False
        return super().reset()

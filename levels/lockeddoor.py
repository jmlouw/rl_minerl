import numpy as np
import math
from gym import spaces
from gym_miniworld.miniworld import MiniWorldEnv, Room
from gym_miniworld.entity import Box, ImageFrame, MeshEnt, Key, Ball, Box_Rect, TextFrame
from gym_miniworld.params import DEFAULT_PARAMS
import copy

key_to_color = {'redstone_block': 'red', 'emerald_block': 'green', 'lapis_block': 'blue'}
color_to_key = {'redstone_block': 'red', 'emerald_block': 'green', 'lapis_block': 'blue'}

block_to_key = {'iron_block': 'iron_sword/iron_sword', 'gold_block': 'gold_sword/gold_sword', 'diamond_block': 'diamond_sword/diamond_sword'}
Y_VEC = np.array([0, 1, 0])

class LockedDoor(MiniWorldEnv):
    """
    Outside environment with two rooms connected by a gap in a wall
    """

    def __init__(self, **kwargs):
        self.door_open = False
        self.intermed_reward = False
        super().__init__(
            max_episode_steps=2000,
            **kwargs
        )

        # Allow only the movement actions
        # self.action_space = spaces.Discrete(5)

    def _get_carry_pos(self, agent_pos, ent: MeshEnt):
        """
        Compute the position at which to place an object being carried
        """
        #dist = self.agent.radius + ent.radius + self.max_forward_step
        pos = agent_pos + np.cross(self.agent.dir_vec,(np.array([0,1,0])))*ent.carry_offset[2]+ ent.carry_offset[0]*self.agent.dir_vec
        y_pos = self.agent.cam_height - ent.height+ ent.carry_offset[1]
        pos = pos + Y_VEC * y_pos
        return pos

    def _gen_world(self):
        # Top
        room0 = self.add_rect_room(
            min_x=-4, max_x=7,
            min_z=0, max_z=8,
            wall_tex='gray_concrete',
            floor_tex='chiseled_quartz_block_top',
            ceil_tex='quartz_block_bottom',
        )
        # Bottom
        room1 = self.add_rect_room(
            min_x=1, max_x=7,
            min_z=-7, max_z=-1,
            wall_tex='diamond_ore',
            floor_tex='spruce_planks',
            ceil_tex='spruce_planks',
        )

        self.connect_rooms(room0, room1, min_x=3, max_x=5, max_y=2)

        # self.box = self.place_entity(Box(color='green'), room=room1, dir=0, pos= np.array([4, 0, -4]))
        self.diamond_pick = self.place_entity(MeshEnt(
            mesh_name='diamond_pickaxe/diamond_pickaxe', height=1, static=True, check_collisions=True
        ), dir=0, room=room1,  pos=np.array([4, 1, -4]))

        self.goal = self.place_entity(MeshEnt(
            mesh_name='diamond_block/diamond_block', height=1, static=False, check_collisions=False
        ), room=room1, dir=-np.pi/2, pos=np.array([4, 0, -4]))

        lock_type = 'redstone_block'

        self.key = MeshEnt(mesh_name='ball_yellow', height=0.8, static=False, check_collisions=False,
            carry_offset=np.array([0.65,0.3,0.4]), carry_pitch=1)
        self.place_entity(self.key, room=room0, pos=np.array([-2.5, 0.1, 4]))

        self.lock = [MeshEnt(
            mesh_name=f'{ lock_type}/{ lock_type}', height=1, static=False, check_collisions=False
        ) for _ in range (4)]

        for i, segment in enumerate(self.lock):
            j = 0
            if i>=2:
                j= -2.98
            i %= 2
            self.place_entity(segment, room0, pos= np.array([5.49+j, 0 +i, -0.48]), dir = 0)


        self.door_left = MeshEnt(
            mesh_name=f'wooden_door_red/wooden_door_red', height=2, static=False, radius=1.005
        )
        self.door_right = MeshEnt(
            mesh_name=f'wooden_door_red/wooden_door_red', height=2, static=False, radius=1.005
        )

        self.place_entity(self.door_left
                          ,
                          pos=np.array([3.5, 0, -1]),
                          dir=0
                          )
        self.place_entity(self.door_right
                          ,
                          pos=np.array([4.5, 0, -1]),
                          dir=np.pi
                          )

        self.place_agent(room=room0, dir=np.pi / 2, pos=np.array([5, 0, 5]))

    def check_plate(self, block):
        if 3<block.pos[0] <5 and -1<block.pos[2] <1:
            return  True
        else:
            return False

    def step(self, action):
        obs, reward, done, info = super().step(action)

        reward -= 0.0001

        if self.close_by(self.key) and not self.agent.carrying_right and not self.door_open:
            self.key.set_mesh('ball_blue')
            self.agent.carrying_right = self.key
            ent_pos = self._get_carry_pos(self.agent.pos, self.agent.carrying_right)
            self.agent.carrying_right.pos = ent_pos
            self.agent.carrying_right.dir = self.agent.dir
            obs = self.render_obs()
            if self.intermed_reward:
                reward += 0.1

        if self.check_plate(self.key) :
            if not self.door_open:
                self.key.set_mesh('ball_green')
                self.door_left.set_mesh('wooden_door_green/wooden_door_green')
                self.door_right.set_mesh('wooden_door_green/wooden_door_green')

                for i, segment in enumerate(self.lock):
                    segment.set_mesh('emerald_block/emerald_block')
                # for segment in self.lock:
                #     segment.set_mesh(mesh_name=f'iron_block/iron_block')
                self.door_left.check_collisions = False
                self.door_left.dir = -np.pi/2
                self.door_left.pos = np.array([3.1, 0, -.5])

                self.door_right.check_collisions = False
                self.door_right.dir = -np.pi/2
                self.door_right.pos = np.array([4.9, 0, -0.5])
                self.door_open = True


        if self.close_by(self.goal):
            reward += self._reward()
            done = True

        return obs, reward, done, info

    def reset(self):
        self.door_open = False
        return super().reset()

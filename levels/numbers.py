import numpy as np
import math
from gym import spaces
from gym_miniworld.miniworld import MiniWorldEnv, Room
from gym_miniworld.entity import Box, ImageFrame, MeshEnt, Key, Ball, Box_Rect, TextFrame, MeshEntToggle
from gym_miniworld.params import DEFAULT_PARAMS
import copy

key_to_color = {'redstone_block': 'red', 'emerald_block': 'green', 'lapis_block': 'blue'}
color_to_key = {'redstone_block': 'red', 'emerald_block': 'green', 'lapis_block': 'blue'}

block_to_key = {'iron_block': 'iron_sword/iron_sword', 'gold_block': 'gold_sword/gold_sword',
                'diamond_block': 'diamond_sword/diamond_sword'}
Y_VEC = np.array([0, 1, 0])


class Numbers(MiniWorldEnv):
    """
    Outside environment with two rooms connected by a gap in a wall
    """
    def __init__(self, **kwargs):
        self.door_open = False
        super().__init__(
            max_episode_steps=900,
            **kwargs
        )
        # Allow only the movement actions
        # self.action_space = spaces.Discrete(5)

    def _get_carry_pos(self, agent_pos, ent: MeshEnt):
        """
        Compute the position at which to place an object being carried
        """
        # dist = self.agent.radius + ent.radius + self.max_forward_step
        pos = agent_pos + np.cross(self.agent.dir_vec, (np.array([0, 1, 0]))) * ent.carry_offset[2] + ent.carry_offset[
            0] * self.agent.dir_vec
        y_pos = self.agent.cam_height - ent.height + ent.carry_offset[1]
        pos = pos + Y_VEC * y_pos
        return pos

    def _gen_world(self):
        # Top
        room0 = self.add_rect_room(
            min_x=-4, max_x=7,
            min_z=0, max_z=8,
            wall_tex='stone_bricks',
            floor_tex='chiseled_quartz_block_top',
            ceil_tex='quartz_block_bottom',
        )
        # Bottom
        room1 = self.add_rect_room(
            min_x=1, max_x=7,
            min_z=-7, max_z=-1,
            wall_tex='diamond_ore',
            floor_tex='spruce_planks',
            ceil_tex='spruce_planks',
        )

        self.connect_rooms(room0, room1, min_x=3, max_x=5, max_y=2)

        self.key = np.random.randint(1, 8)

        self.text = self.place_entity(
            TextFrame(np.array([-3.5, 1, 4]), str=f'{self.key}', dir=0, height=7),
            pos=np.array([-4, 1.5, 4]), room=room0, dir=0)

        # pillars
        self.pillar_segs = []
        x = -0.5
        z = -0.45
        for i in range(3):
            for j in range(3):
                pillar_seg = MeshEnt(
                    mesh_name='gray_concrete_block/gray_concrete_block', height=1, static=False, check_collisions=False
                )
                self.place_entity(pillar_seg, pos=np.array([x, j, z]), dir=0)
                self.pillar_segs.append(pillar_seg)
            x += 1.3

        x = -0.5
        z = 0.035
        self.swords = []
        for i in range(3):
            sword = MeshEntToggle(mesh_name='iron_sword/iron_sword', mesh_name_toggled='gold_sword/gold_sword',
                                  height=1, static=False, check_collisions=True, static_pitch=np.pi / 4, radius=0.5, render_translation=np.array([0.35,0,0]))
            self.place_entity(sword, pos=np.array([x, 1, z]), dir=0)
            self.swords.append(sword)
            x += 1.3

        # self.box = self.place_entity(Box(color='green'), room=room1, dir=0, pos= np.array([4, 0, -4]))
        self.diamond_pick = self.place_entity(MeshEnt(
            mesh_name='diamond_pickaxe/diamond_pickaxe', height=1, static=True, check_collisions=True
        ), dir=0, room=room1, pos=np.array([4, 1, -4]))

        self.goal = self.place_entity(MeshEnt(
            mesh_name='diamond_block/diamond_block', height=1, static=False, check_collisions=False
        ), room=room1, dir=-np.pi / 2, pos=np.array([4, 0, -4]))

        self.door_left = MeshEnt(
            mesh_name=f'wooden_door/wooden_door', height=2, static=False, radius=1.005
        )
        self.door_right = MeshEnt(
            mesh_name=f'wooden_door/wooden_door', height=2, static=False, radius=1.005
        )

        self.place_entity(self.door_left
                          ,
                          pos=np.array([3.5, 0, -1]),
                          dir=0
                          )
        self.place_entity(self.door_right
                          ,
                          pos=np.array([4.5, 0, -1]),
                          dir=np.pi
                          )

        self.place_agent(room=room0, dir=np.pi, pos=np.array([5, 0, 5]))

    def check_plate(self, block):
        if 4.5 < block.pos[0] < 6 and -1 < block.pos[2] < 0.5:
            return True
        else:
            return False

    def step(self, action):
        obs, reward, done, info = super().step(action)

        answer = ''
        for sword in self.swords:

            if sword.toggled:
                answer += '1'
            else:
                answer += '0'


        if answer == '{:03b}'.format(self.key):
            for sword in self.swords:
                sword.set_mesh('green_sword/green_sword')
                self.door_left.check_collisions = False
                self.door_left.dir = -np.pi / 2
                self.door_left.pos = np.array([3.1, 0, -.5])

                self.door_right.check_collisions = False
                self.door_right.dir = -np.pi / 2
                self.door_right.pos = np.array([4.9, 0, -0.5])


        if self.collide(self.goal):
            reward += self._reward()
            done = True

        return obs, reward, done, info

    def reset(self):
        self.door_open = False
        return super().reset()

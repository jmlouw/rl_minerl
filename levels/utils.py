import numpy as np
from typing import List

from gym_miniworld.miniworld import Room


def check_intersection(x_min, x_max, z_min, z_max, placed_rooms: List[Room]):
    new_room_outline = np.array([
        # East wall
        [x_max, z_max],
        # North wall
        [x_max, z_min],
        # West wall
        [x_min, z_min],
        # South wall
        [x_min, z_max],
    ])
    new_room = Room(new_room_outline)
    for room in placed_rooms:
        if room.intersects(new_room):
            return True
    return False

def generate_room(rand, placed_rooms: List[Room], size = ((5, 10), (5,10)), connected_room = None):
    room_size_x = rand.np_random.randint(*size[0]) * 2
    room_size_z = rand.np_random.randint(*size[1]) * 2

    if not placed_rooms:
        c_room_x0 = 0
        c_room_z0 = 0
        c_room_x1 = room_size_x
        c_room_z1 = room_size_z
        return c_room_x0, c_room_x1, c_room_z0, c_room_z1, None, None, None, True

    available_walls = ['north', 'east', 'south', 'west']
    not_valid_room = True
    not_valid_count = 0

    while not_valid_room:
        if connected_room is not None:
            current_room = placed_rooms[connected_room]
        else:
            current_room = rand.choice(placed_rooms)

        wall = rand.choice(available_walls)
        if wall == 'north' or wall == 'south':
            current_room_width = np.abs(current_room.min_x - current_room.max_x)
            door_center = rand.np_random.randint(current_room.mid_x - (current_room_width / 2 - 1),
                                         current_room.mid_x + (current_room_width / 2 - 1))
            nextroomshift = rand.np_random.randint(1, room_size_x / 2)
            c_room_x0 = door_center - (nextroomshift)
            c_room_x1 = door_center + (room_size_x - nextroomshift)

        else:
            nextroomshift = rand.np_random.randint(1, room_size_z / 2)
            current_room_width = np.abs(current_room.min_z - current_room.max_z)
            door_center = rand.np_random.randint(current_room.mid_z - (current_room_width / 2 - 1),
                                         current_room.mid_z + (current_room_width / 2 - 1))
            c_room_z0 = door_center - (nextroomshift)
            c_room_z1 = door_center + (room_size_z - nextroomshift)

        if wall == 'north':
            c_room_z1 = current_room.min_z - 1
            c_room_z0 = c_room_z1 - room_size_z
            door_pos = np.array([door_center - 1, door_center + 1, c_room_z1, c_room_z1])
        elif wall == 'east':
            c_room_x0 = current_room.max_x + 1
            c_room_x1 = c_room_x0 + room_size_x
            door_pos = np.array([c_room_x0, c_room_x0, door_center - 1, door_center + 1])
        elif wall == 'south':
            c_room_z0 = current_room.max_z + 1
            c_room_z1 = c_room_z0 + room_size_z
            door_pos = np.array([door_center - 1, door_center + 1, c_room_z0, c_room_z0])
        else:
            c_room_x1 = current_room.min_x - 1
            c_room_x0 = c_room_x1 - room_size_x
            door_pos = np.array([c_room_x1, c_room_x1, door_center - 1, door_center + 1])
        not_valid_room = check_intersection(c_room_x0, c_room_x1, c_room_z0, c_room_z1, placed_rooms)
        not_valid_count += 1

        if not_valid_room and not_valid_count >= 50:
            return None, None, None, None, None, None, None, False

    return c_room_x0, c_room_x1, c_room_z0, c_room_z1, wall, door_pos, current_room, True

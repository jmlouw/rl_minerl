import numpy as np

from gym_miniworld.miniworld import MiniWorldEnv
from gym_miniworld.entity import MeshEnt, Door
from levels.utils import generate_room

Y_VEC = np.array([0, 1, 0])

room_types = {
    'door_key_room': {'wall_tex': 'end_stone_bricks',
                      'floor_tex': 'red_sandstone_top',
                      'ceil_tex': 'birch_planks'},
    'door_room': {'wall_tex': 'stone_bricks',
                  'floor_tex': 'green_wool',
                  'ceil_tex': 'oak_log_top'},
    'emerald_room': {'wall_tex': 'purple_concrete',
                     'floor_tex': 'black_wool',
                     'ceil_tex': 'spruce_planks'},
    'steve_room': {'wall_tex': 'light_gray_concrete',
                   'floor_tex': 'chiseled_quartz_block_top',
                   'ceil_tex': 'iron_block'}}




class ComboMultiRoom(MiniWorldEnv):
    """
    Room with multiple objects. The agent collects +1 reward for picking up
    each object. Objects disappear when picked up.
    """

    def __init__(self, num_rooms=3, size=14, intermed_reward=False, **kwargs):
        assert num_rooms >= 1
        self.num_rooms = num_rooms
        assert size >= 2
        self.size = size
        self.intermed_reward = intermed_reward

        super().__init__(
            max_episode_steps=1000,
            **kwargs
        )
        # Reduce the action space
        # self.action_space = spaces.Discrete(3)

    def _get_carry_pos(self, agent_pos, ent: MeshEnt):
        """
        Compute the position at which to place an object being carried
        """
        # dist = self.agent.radius + ent.radius + self.max_forward_step
        pos = agent_pos + np.cross(self.agent.dir_vec, (np.array([0, 1, 0]))) * ent.carry_offset[2] + ent.carry_offset[
            0] * self.agent.dir_vec
        y_pos = self.agent.cam_height - ent.height + ent.carry_offset[1]
        pos = pos + Y_VEC * y_pos
        return pos

    def room_door(self, wall, door_pos):

        center_x = (door_pos[0]+door_pos[1])/2
        center_z = (door_pos[2]+door_pos[3])/2
        center_pos = np.array([center_x, 0, center_z])

        if wall == 'north':
            door_left_pos = center_pos + np.array([-0.5,0,0])
            door_right_pos = center_pos + np.array([+0.5,0,0])
            dir = np.pi
        elif wall == 'east':
            dir = np.pi/2
            door_left_pos = center_pos + np.array([0,0,-0.5])
            door_right_pos = center_pos + np.array([0,0,+0.5])
        elif wall == 'south':
            dir = -np.pi
            door_left_pos = center_pos + np.array([-0.5,0,0])
            door_right_pos = center_pos + np.array([+0.5,0,0])
        else:
            dir = -np.pi/2
            door_left_pos = center_pos + np.array([0,0,+0.5])
            door_right_pos = center_pos + np.array([0,0,-0.5])

        self.door_left = Door(
            mesh_name=f'wooden_door/wooden_door', height=2, static=False, check_collisions=True, dir=dir+np.pi, left_door=True
        )
        self.door_right = Door(
            mesh_name=f'wooden_door/wooden_door', height=2, static=False, check_collisions=True, dir=dir, left_door=False
        )

        self.place_entity(self.door_left,
                          pos=door_left_pos
                          )
        self.place_entity(self.door_right,
                          pos=door_right_pos,
                          dir=dir
                          )


    def room_door_key(self, wall, door_pos):
            pass

    def _gen_world(self):
        my_rooms = []
        valid_level = False
        room_types_list = list(room_types.keys())[-self.num_rooms:]

        while not valid_level:
            for i, room_type in zip(range(self.num_rooms), room_types_list):

                c_room_x0, c_room_x1, c_room_z0, c_room_z1, wall, door_pos, current_room, valid_level = generate_room(
                    my_rooms)
                if not valid_level:
                    my_rooms.clear()
                    self.rooms.clear()
                    break
                new_room = self.add_rect_room(
                    min_x=c_room_x0, max_x=c_room_x1,
                    min_z=c_room_z0, max_z=c_room_z1,
                    wall_tex=room_types[room_type]['wall_tex'],
                    floor_tex=room_types[room_type]['floor_tex'],
                    ceil_tex=room_types[room_type]['ceil_tex'],
                )
                my_rooms.append(new_room)


                if len(my_rooms) >= 2:
                    room_types[room_types_list[i-1]]['door_pos'] = door_pos
                    room_types[room_types_list[i-1]]['wall'] = wall

                    if wall == 'north' or wall == 'south':
                        self.connect_rooms(my_rooms[-1], my_rooms[-2], min_x=door_pos[0], max_x=door_pos[1], max_y=2)
                    else:
                        self.connect_rooms(my_rooms[-1], my_rooms[-2], min_z=door_pos[2], max_z=door_pos[3], max_y=2)

        for room, room_type in zip(my_rooms, room_types_list):
            if room_type == 'door_room':
                self.room_door(room_types[room_type]['wall'], room_types[room_type]['door_pos'])
            # if room_type == 'emerald_room':
            #     self.room_door(room_types[room_type]['wall'], room_types[room_type]['door_pos'])

        # object_room = random.randint(0, len(my_rooms)-1)
        self.goal = self.place_entity(MeshEnt(
            mesh_name='steve/steve', height=2.2, static=False, check_collisions=False
        ), room=my_rooms[-1])
        self.object = self.place_entity(MeshEnt(
            mesh_name='emerald_block/emerald_block', height=1, static=False, check_collisions=True,
            carry_offset=np.array([0.7, 0.1, 0.5]), carry_yaw=1), room=my_rooms[-2] if self.num_rooms>=2 else my_rooms[-1], dir=0)
        self.place_agent(room=my_rooms[0])

    def set_num_rooms(self, num_rooms):
        self.num_rooms = num_rooms

    def step(self, action):
        obs, reward, done, info = super().step(action)
        # obs, reward, done, info = super().step(action)
        reward -= 0.0001
        if self.near(self.object) and not self.agent.carrying_right:
            self.object.set_mesh('diamond_block/diamond_block')
            if self.intermed_reward:
                reward += 0.5
            self.agent.carrying_right = self.object
            ent_pos = self._get_carry_pos(self.agent.pos, self.agent.carrying_right)
            self.agent.carrying_right.pos = ent_pos
            self.agent.carrying_right.dir = self.agent.dir
        info['success'] = False
        if (self.near(self.goal) and self.agent.carrying_right):
            info['success'] = True
            reward += 1
            done = True
        return obs, reward, done, info


# class FetchS20(FetchS10):
#     def __init__(self, **kwargs):
#         super().__init__(size= 20, **kwargs)
#
#
# class FetchS30(FetchS10):
#     def __init__(self, **kwargs):
#         super().__init__(size=30, **kwargs)

import numpy as np
from gym.spaces import Box
from gym_miniworld.miniworld import MiniWorldEnv
from gym_miniworld.entity import MeshEnt, DoubleDoor
from levels.utils import generate_room
from gym_miniworld.params import DEFAULT_PARAMS

Y_VEC = np.array([0, 1, 0])

angle_to_dir = {0: 'south', 4: 'south', 1: 'east', 2: 'north', 3: 'west'}
angle_to_one_hot = {0: np.array([1, 0, 0, 0]), 4: np.array([1, 0, 0, 0]), 1: np.array([0, 1, 0, 0]),
                    2: np.array([0, 0, 1, 0]), 3: np.array([0, 0, 0, 1])}


class FetchMultiRoom(MiniWorldEnv):
    """
    Room with multiple objects. The agent collects +1 reward for picking up
    each object. Objects disappear when picked up.
    """

    def __init__(self, size=((5, 10), (5, 10)), max_episode_steps=1500,
                 intrinsic_step_reward=-0.0001, connected_room=None,
                 steve_placement=None, block_placement=None, open_doors=False, locked_doors_enabled=True, **kwargs):
        DEFAULT_PARAMS.set('cam_pitch', -10, -10, 10)
        self.intrinsic_step_reward = intrinsic_step_reward
        self.size = size
        self.connected_room = connected_room
        self.locked_doors_enabled = locked_doors_enabled
        self.open_doors = open_doors
        self.num_rooms = 1

        self.steve_placement = steve_placement
        self.block_placement = block_placement

        self.placed_doors = []
        self.locked_doors = []
        self.keys = []

        super().__init__(
            max_episode_steps=max_episode_steps,
            **kwargs
        )
        self.observation_space.spaces['compass'] = Box(low=0, high=1, shape=(2,), dtype=np.uint8)

    def _get_carry_pos(self, agent_pos, ent: MeshEnt):
        """
        Compute the position at which to place an object being carried
        """
        # dist = self.agent.radius + ent.radius + self.max_forward_step
        pos = agent_pos + np.cross(self.agent.dir_vec, (np.array([0, 1, 0]))) * ent.carry_offset[2] + ent.carry_offset[
            0] * self.agent.dir_vec
        y_pos = self.agent.cam_height - ent.height + ent.carry_offset[1]
        pos = pos + Y_VEC * y_pos
        return pos

    def get_door_center_dir(self, wall, door_pos):
        center_x = (door_pos[0] + door_pos[1]) / 2
        center_z = (door_pos[2] + door_pos[3]) / 2
        center_pos = np.array([center_x, 0, center_z])
        if wall == 'north':
            center_pos += np.array([0, 0, +0.5])
            dir = 1 * np.pi
        elif wall == 'east':
            center_pos += np.array([-0.5, 0, 0])
            dir = np.pi / 2
        elif wall == 'south':
            center_pos += np.array([0, 0, -0.5])
            dir = 2 * np.pi
        else:
            dir = -np.pi / 2
            center_pos += np.array([+0.5, 0, 0])
        return dir, center_pos

    def room_door(self, wall, door_pos, locked):
        dir, center_pos = self.get_door_center_dir(wall, door_pos)
        new_double_door = DoubleDoor(locked, 2, static=False, check_collisions=True, open=self.open_doors)
        self.place_entity(new_double_door, pos=center_pos, dir=dir)
        return new_double_door

    def place_obstacle(self, wall, door_pos):
        _, center_pos = self.get_door_center_dir(wall, door_pos)
        self.obstacle = self.place_entity(MeshEnt(
            mesh_name='coal_block_light/coal_block_light', height=1, static=False, carriable=True, check_collisions=True,
            carry_offset=np.array([0.7, 0.25, 0.7]), carry_yaw=-1.5, carry_scaling_factor=2),pos=center_pos, dir=0)

    def place_doors(self, walls_doors):
        for index, (wall, door, current_room) in enumerate(walls_doors):
            if (index+1) % 2 == 0:
                new_door = self.room_door(wall, door, False)
                self.placed_doors.append((new_door, current_room))
            else:
                self.place_obstacle(wall, door)


    def lock_doors(self):
        """
        a function that lock certain doors. The base class function locks doors with a 50% chance
        """
        for door, current_room in self.placed_doors:
            locked = bool(self.rand.np_random.randint(0,1))
            if locked:
                door.lock()
                self.locked_doors.append(door)
                new_key = MeshEnt(mesh_name='key_blue', height=0.5, static=False, carriable=True,
                                  check_collisions=False, carry_offset=np.array([0.7, -0.5, -0.65]), carry_yaw=1.3,
                                  render_trans=np.array([0, 0.5, 0]))
                self.keys.append(new_key)
                self.place_entity(new_key, room=current_room)

    def get_texture(self):
        return 'stone_bricks', 'mossy_cobblestone', 'mossy_stone_bricks'
    
    

    def generate_map(self):
        main_rooms = []
        openings = []
        valid_level = False
        tex_wall, tex_floor, tex_ceil = self.get_texture()
        while not valid_level:
            for i in range(self.num_rooms):
                c_room_x0, c_room_x1, c_room_z0, c_room_z1, wall, door_pos, current_room, valid_level = generate_room(self.rand,
                    placed_rooms=main_rooms, size=self.size, connected_room=self.connected_room)

                if not valid_level:
                    main_rooms.clear()
                    self.rooms.clear()
                    openings.clear()
                    break

                if door_pos is not None:
                    openings.append((wall, door_pos, current_room))
                new_room = self.add_rect_room(
                    min_x=c_room_x0, max_x=c_room_x1,
                    min_z=c_room_z0, max_z=c_room_z1,
                    wall_tex= tex_wall,
                    floor_tex=tex_floor,
                    ceil_tex=tex_ceil,
                    # wall_tex= 'stone_bricks',
                    # floor_tex='green_wool1',
                    # ceil_tex='quartz_block_bottom',
                )
                main_rooms.append(new_room)
                if len(main_rooms) >= 2:
                    if wall == 'north' or wall == 'south':
                        self.connect_rooms(current_room, new_room, min_x=door_pos[0], max_x=door_pos[1], max_y=2, wall_tex='spruce_planks', ceil_tex='spruce_planks')
                    else:
                        self.connect_rooms(current_room, new_room, min_z=door_pos[2], max_z=door_pos[3], max_y=2, wall_tex='spruce_planks', ceil_tex='spruce_planks')
        return openings, main_rooms

    def _gen_world(self):
        self.locked_doors.clear()
        self.placed_doors.clear()
        self.keys.clear()

        openings, main_rooms = self.generate_map()
        self.place_doors(openings)
        if self.locked_doors_enabled:
            self.lock_doors()

        if self.steve_placement is not None:
            steves_room = main_rooms[self.steve_placement]
        else:
            steves_room = self.rand.choice(main_rooms)
        self.steve = self.place_entity(MeshEnt(
            mesh_name='steve_injured/steve_injured', height=1, static=False, check_collisions=False
        ), room=steves_room)

        if self.block_placement is not None:
            blocks_room = main_rooms[self.block_placement]
        else:
            blocks_room = self.rand.choice(main_rooms)
        self.block = self.place_entity(MeshEnt(
            mesh_name='first_aid_red', height=0.75, static=False, carriable=True, check_collisions=False,
            carry_offset=np.array([0.45, 0.25, 0.5]), carry_yaw=-1.5), room=blocks_room, dir=np.pi/2)

        self.place_agent(room=main_rooms[0])

    def compass(self):
        vector_to_emerald = (self.block.pos - self.agent.pos)
        vector_to_steve = (self.steve.pos - self.agent.pos)

        angle_to_emerald = angle_between(vector_to_emerald, self.agent.dir_vec)
        angle_to_steve = angle_between(vector_to_steve, self.agent.dir_vec)

        if self.agent.carrying_right:
            angle_to_emerald = -1
        return angle_to_emerald, angle_to_steve

    def step(self, action):

        obs, reward, done, info = super().step(action)
        reward += self.intrinsic_step_reward

        # if self.agent.carrying_right == self.obstacle:
        #     self.obstacle.set_mesh('coal_block/coal_block')
        # elif self.obstacle.mesh_name == 'coal_block/coal_block':
        #     self.obstacle.set_mesh('coal_block_light/coal_block_light')
        #
        # if self.agent.carrying_right == self.block:
        #     self.block.set_mesh('first_aid_darkred')
        # elif self.block.mesh_name == 'first_aid_darkred':
        #     self.block.set_mesh('first_aid_red')

        # for key in self.keys:
        #     if self.near(key) and not self.agent.carrying_left:
        #         self.agent.carrying_left = key
        #         ent_pos = self._get_carry_pos(self.agent.pos, self.agent.carrying_left)
        #         self.agent.carrying_left.pos = ent_pos
        #         self.agent.carrying_left.dir = self.agent.dir
        #         key.set_mesh('key_purple')

        # if self.near(self.block) and not self.agent.carrying_right:
        #     self.block.set_mesh('first_aid_blue')
        #     self.agent.carrying_right = self.block
        #     ent_pos = self._get_carry_pos(self.agent.pos, self.agent.carrying_right)
        #     self.agent.carrying_right.pos = ent_pos
        #     self.agent.carrying_right.dir = self.agent.dir

        info['success'] = False
        if self.agent.carrying_right:
            if self.agent.carrying_right.mesh_name == 'key_blue':
                for door in self.locked_doors:
                    if self.near(self.agent.carrying_right, door):
                        old_key = self.agent.carrying_right
                        self.keys.remove(old_key)
                        self.agent.carrying_right = None
                        door.unlock()
                        self.locked_doors.remove(door)
                        self.entities.remove(old_key)
                        break

        if (self.near(self.steve) and self.agent.carrying_right):
            if self.agent.carrying_right.mesh_name == 'first_aid_red':
                info['success'] = True
                reward += 1
                done = True

        info['compass'] = self.compass()
        info['carrying'] = 1 if self.agent.carrying_right else 0

        return obs, reward, done, info


def angle_between(v1, v2):
    """ Returns the angle in radians between vectors 'v1' and 'v2'::
            # >>> angle_between((1, 0, 0), (0, 1, 0))
            1.5707963267948966
            # >>> angle_between((1, 0, 0), (1, 0, 0))
            0.0
            # >>> angle_between((1, 0, 0), (-1, 0, 0))
            3.141592653589793
    """
    dot = v1[0] * v2[0] + v1[2] * v2[2]
    det = v1[0] * v2[2] - v2[0] * v1[2]
    return (np.arctan2(det, dot) + np.pi)


class FetchThreeRoomsDoorObstacle(FetchMultiRoom):
    def __init__(self, max_episode_steps=1000, **kwargs):
        self.num_rooms = 3
        super().__init__(size=((7, 9), (7,9)), max_episode_steps=max_episode_steps, connected_room=None,
                         steve_placement=None, block_placement=None,
                         locked_doors_enabled=False, **kwargs)

    def place_doors(self, walls_doors):
        lock_obs_index = self.rand.np_random.randint(0,2)
        for index, (wall, door, current_room) in enumerate(walls_doors):
            if (index + lock_obs_index ) % 2 == 0:
                new_door = self.room_door(wall, door, False)
                self.placed_doors.append((new_door, current_room))
            else:
                self.place_obstacle(wall, door)


class FetchObstacle(FetchMultiRoom):
    def __init__(self, steve_placement=None, block_placement=None,max_episode_steps=2000,connected_room=None, **kwargs):

        super().__init__(size=((8, 9), (8,9)), max_episode_steps=max_episode_steps, connected_room=connected_room,
                         steve_placement=steve_placement, block_placement=block_placement,
                         locked_doors_enabled=False, **kwargs)
        self.num_rooms = 2

    def place_doors(self, walls_doors):
        for index, (wall, door, current_room) in enumerate(walls_doors):
            self.place_obstacle(wall, door)

class FetchTwoRoomsObstacleSparse(FetchObstacle):
    def __init__(self, **kwargs):
        super().__init__(steve_placement=0, block_placement=-1,connected_room=-1,**kwargs)
        self.num_rooms = 2

class FetchTwoRoomsObstacleSparseRandomRooms(FetchObstacle):
    def __init__(self, **kwargs):
        super().__init__(steve_placement=0, block_placement=-1,connected_room=-1,**kwargs)

    def reset(self):
        self.num_rooms = self.rand.np_random.randint(1, 4)
        obs = super().reset()
        return obs

class FetchThreeRoomsObstacleSparse(FetchObstacle):
    def __init__(self, **kwargs):
        super().__init__(steve_placement=0, block_placement=-1,connected_room=-1,**kwargs)
        self.num_rooms = 3

class FetchThreeRoomsObstacleRandom(FetchObstacle):
    def __init__(self, **kwargs):
        super().__init__(steve_placement=-1, block_placement=0,connected_room=-1,**kwargs)
        self.num_rooms = 3

class FetchTwoRoomsObstacleV1(FetchObstacle):
    def __init__(self, **kwargs):
        super().__init__(steve_placement=-1, block_placement=-1,**kwargs)
        self.num_rooms = 2


class FetchFourRoomsObstacle(FetchObstacle):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.num_rooms = 4




# class FetchMultiRoomSparse(FetchMultiRoom):
#     def __init__(self, max_episode_steps=1000, level_number=3, **kwargs):
#         super().__init__(size=((8, 8), (8, 8)), max_episode_steps=max_episode_steps, level_number=level_number,
#                          connected_room=-1, steve_placement=0, block_placement=-1,
#                          **kwargs)
#
#     def set_level(self):
#         if self.level_number >= 2:
#             self.open_doors = False
#             self.num_rooms = self.level_number
#         else:
#             self.open_doors = True
#             self.num_rooms = self.level_number + 1
#         if self.level_number >= 3:
#             self.lock_last_room = True
#
#     def lock_doors(self):
#         door, current_room = self.placed_doors[-1]
#         door.lock()
#         self.locked_doors.append(door)
#         new_key = MeshEnt(mesh_name='key_blue', height=0.5, static=False, carriable=True,
#                           check_collisions=False, carry_offset=np.array([0.7, -0.5, -0.65]), carry_yaw=1.3,
#                           render_trans=np.array([0, 0.5, 0]))
#         self.keys.append(new_key)
#         self.place_entity(new_key, room=current_room)
#
#
# class FetchMultiRoomOneLocked(FetchMultiRoom):
#     def __init__(self, max_episode_steps=1000, level_number=4, **kwargs):
#         super().__init__(size=((7, 9), (7,9)), max_episode_steps=max_episode_steps, level_number=level_number,
#                          **kwargs)
#
#     def set_level(self):
#         self.num_rooms = self.level_number
#         if self.level_number >= 3:
#             self.lock_one_random_room = True
#             self.num_rooms -= 1
#
#     def lock_doors(self):
#         door, current_room = self.rand.choice(self.placed_doors)
#         door.lock()
#         self.locked_doors.append(door)
#         new_key = MeshEnt(mesh_name='key_blue', height=0.5, static=False, carriable=True,
#                           check_collisions=False, carry_offset=np.array([0.7, -0.5, -0.65]), carry_yaw=1.3,
#                           render_trans=np.array([0, 0.5, 0]))
#         self.keys.append(new_key)
#         self.place_entity(new_key, room=current_room)
#
# class FetchMultiRoomOneLockedRandomTextures(FetchMultiRoomOneLocked):
#     def __init__(self, max_episode_steps=1000, level_number=4, **kwargs):
#         super().__init__(max_episode_steps=max_episode_steps, level_number=level_number,
#                          **kwargs)
#
#     def get_texture(self):
#         return self.rand.choice(
#             [('mossy_stone_bricks', 'mossy_stone_bricks_light'), ('stone_bricks', 'stone_bricks_light'),
#              ('stone', 'stone_light'), ('end_stone_bricks', 'sandstone_bottom'),('cobblestone', 'cobblestone_light'),
#              ('mossy_cobblestone', 'mossy_cobblestone'), ('red_sandstone', 'red_sandstone_top')])
#
#     # def get_texture(self):
#     #     return 'iron_block', 'iron_block'
#
#
# class FetchTwoRoomsDoor(FetchMultiRoom):
#     def __init__(self, max_episode_steps=1000, **kwargs):
#         self.num_rooms = 2
#         super().__init__(size=((7, 9), (7,9)), max_episode_steps=max_episode_steps, connected_room=-1,
#                          steve_placement=None, block_placement=None,
#                          locked_doors_enabled=False, **kwargs)
#
#     def place_doors(self, walls_doors):
#         for index, (wall, door, current_room) in enumerate(walls_doors):
#             new_door = self.room_door(wall, door, False)
#             self.placed_doors.append((new_door, current_room))
#
#
#     def set_level(self):
#         pass
#
#     def lock_doors(self):
#         pass



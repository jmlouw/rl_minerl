import numpy as np
from gym_miniworld.miniworld import MiniWorldEnv
from gym_miniworld.entity import MeshEnt
import random

from levels.utils import generate_room

Y_VEC = np.array([0, 1, 0])

class NavigateMultiRoom(MiniWorldEnv):
    """
    Outside environment with two rooms connected by a gap in a wall
    """

    def __init__(self, **kwargs):
        # self.door_open = False
        # self.intermed_reward = False
        super().__init__(
            max_episode_steps=500,
            **kwargs
        )

        # Allow only the movement actions
        # self.action_space = spaces.Discrete(5)

    # def _get_carry_pos(self, agent_pos, ent: MeshEnt):
    #     """
    #     Compute the position at which to place an object being carried
    #     """
    #     #dist = self.agent.radius + ent.radius + self.max_forward_step
    #     pos = agent_pos + np.cross(self.agent.dir_vec,(np.array([0,1,0])))*ent.carry_offset[2]+ ent.carry_offset[0]*self.agent.dir_vec
    #     y_pos = self.agent.cam_height - ent.height+ ent.carry_offset[1]
    #     pos = pos + Y_VEC * y_pos
    #     return pos

    def room_center(self, x0, x1, z0, z1):
        return np.array([(x0 + x1) / 2, (z0 + z1) / 2])



    def _gen_world(self):
        # print('\ngenworld')
        # Top
        wall_textures = ['mossy_stone_bricks', 'oak_planks', 'end_stone_bricks', 'bricks']
        floor_textures = ['mossy_stone_bricks', 'oak_planks', 'end_stone_bricks', 'bricks']

        num_rooms = 5

        room_size = random.randint(5, 7) * 2
        c_room_x0 = 0
        c_room_x1 = c_room_x0 + room_size
        c_room_z0 = 0
        c_room_z1 = c_room_z0 + room_size

        current_room = self.add_rect_room(
            min_x=c_room_x0, max_x=c_room_x1,
            min_z=c_room_z0, max_z=c_room_z1,
            wall_tex='gray_concrete',
            floor_tex='stonecutter_bottom',
            ceil_tex='quartz_block_bottom',
        )

        base_room = current_room
        # north = 0, east = 1, south = 2, west = 3

        for i in range(num_rooms):
            c_room_x0, c_room_x1, c_room_z0, c_room_z1, wall, door_pos = generate_room(current_room, self.rooms)
            new_room = self.add_rect_room(
                min_x=c_room_x0, max_x=c_room_x1,
                min_z=c_room_z0, max_z=c_room_z1,
                wall_tex='gray_concrete',
                floor_tex='stonecutter_bottom',
                ceil_tex='quartz_block_bottom',
            )

            if wall == 'north' or wall == 'south':
                self.connect_rooms(new_room, current_room, min_x=door_pos[0], max_x=door_pos[1], max_y=2)
            else:
                self.connect_rooms(new_room, current_room, min_z=door_pos[2], max_z=door_pos[3], max_y=2)
            current_room = new_room

            if i == num_rooms - 1:
                self.goal = self.place_entity(MeshEnt(
                    mesh_name='diamond_block/diamond_block', height=1, static=False, check_collisions=False
                ), room=current_room, dir=-np.pi / 2)

                self.diamond_pick = self.place_entity(MeshEnt(
                    mesh_name='diamond_pickaxe/diamond_pickaxe', height=1, static=True, check_collisions=True
                ), dir=0, room=current_room, pos=self.goal.pos + np.array([0, 1, 0]))

        #
        # for room in range(num_rooms):
        #     door_center = random.randint(current_room_center_x-(room_size/2-1),current_room_center_x+(room_size/2-1)) # position of door 1
        #     # print('current_room_center: ', current_room_center_x)
        #     # print('door center: ', door_center)
        #
        #     nextroomshift = random.randint(1,room_size/2)
        #     # print('roomshift', nextroomshift)
        #     # Bottom
        #     z_min += room_size+2
        #     min_x = door_center-(nextroomshift)
        #     max_x = door_center+(room_size-nextroomshift)
        #     # print('next room min max: ', min_x, max_x)
        #
        #     if room == num_rooms -1:
        #         wall_tex = 'iron_block'
        #     else:wall_tex = wall_textures[room%len(wall_textures)]
        #     next_room = self.add_rect_room(
        #         min_x=min_x, max_x=max_x,
        #         min_z=z_min, max_z=z_min+room_size,
        #         wall_tex=wall_tex,
        #         floor_tex=wall_tex,
        #         ceil_tex='quartz_block_bottom',
        #     )
        #     self.connect_rooms(current_room, next_room, min_x=door_center-1, max_x=door_center+1, max_y=2)
        #     current_room = next_room
        #     current_room_center_x = (min_x +max_x)/2
        #     # print('next_room_center: ', current_room_center_x)

        self.place_agent(room=base_room)
        print('new reset')

    # def check_plate(self, block):
    #     if 3<block.pos[0] <5 and -1<block.pos[2] <1:
    #         return  True
    #     else:
    #         return False

    def step(self, action):
        obs, reward, done, info = super().step(action)
        reward -= 0.0001

        info['success'] = False
        if self.close_by(self.goal):
            info['success'] = True
            reward += self._reward()
            done = True
        return obs, reward, done, info

    def reset(self):
        # self.door_open = False
        return super().reset()

import random
import sys
from matplotlib import pyplot as plt
import numpy as np

from gym_miniworld.miniworld import MiniWorldEnv, Room
from gym_miniworld.entity import MeshEnt
from gym_miniworld.params import DEFAULT_PARAMS
Y_VEC = np.array([0, 1, 0])

class FetchS10(MiniWorldEnv):
    """
    Room with multiple objects. The agent collects +1 reward for picking up
    each object. Objects disappear when picked up.
    """

    def __init__(self, size=10, intrinsic_step_reward = -0.0001, max_episode_steps = 200, **kwargs):
        DEFAULT_PARAMS.set('turn_step', 9, 9, 9)
        DEFAULT_PARAMS.set('forward_step', 0.5, 0.5, 0.5)
        assert size >= 2
        self.size = size
        self.intrinsic_step_reward = intrinsic_step_reward

        self.textures = ['stone_bricks']

        super().__init__(
            max_episode_steps=max_episode_steps,
            **kwargs
        )

    def _get_carry_pos(self, agent_pos, ent: MeshEnt):
        """
        Compute the position at which to place an object being carried
        """
        # dist = self.agent.radius + ent.radius + self.max_forward_step
        pos = agent_pos + np.cross(self.agent.dir_vec, (np.array([0, 1, 0]))) * ent.carry_offset[2] + ent.carry_offset[
            0] * self.agent.dir_vec
        y_pos = self.agent.cam_height - ent.height + ent.carry_offset[1]
        pos = pos + Y_VEC * y_pos
        return pos

    def choose_texture(self):
        return 'stone_bricks'

    def _gen_world(self):
        texture = self.choose_texture()

        room = self.add_rect_room(
            min_x=0,
            max_x=self.size,
            min_z=0,
            max_z=self.size,
            wall_tex=texture, #stone_bricks
            floor_tex=texture, #green_wool1
            ceil_tex=texture, #quartz_block_bottom
        )

        self.goal = self.place_entity(MeshEnt(
            mesh_name='steve/steve', height=2.2, static=False, check_collisions=False
        ))

        self.object = MeshEnt(
            mesh_name='emerald_block/emerald_block', height=1, static=False, check_collisions=False,
            carry_offset=np.array([0.7, 0.1, 0.5]), carry_yaw=1)

        self.place_entity(self.goal)
        self.place_entity(self.object, dir=0)
        self.place_agent()

    def step(self, action):
        obs, reward, done, info = super().step(action)
        reward += self.intrinsic_step_reward
        if self.near(self.object) and not self.agent.carrying_right:
            self.object.set_mesh('diamond_block/diamond_block')
            self.agent.carrying_right = self.object
            ent_pos = self._get_carry_pos(self.agent.pos, self.agent.carrying_right)
            self.agent.carrying_right.pos = ent_pos
            self.agent.carrying_right.dir = self.agent.dir
        info['success'] = False
        if self.near(self.goal, self.object) or (self.near(self.goal) and self.agent.carrying_right):
            info['success'] = True
            reward += 1
            done = True
        return obs, reward, done, info

class FetchS15(FetchS10):
    def __init__(self, **kwargs):
        super().__init__(size= 15, **kwargs)

class FetchS15_Random_Textures(FetchS10):
    def __init__(self, **kwargs):
        super().__init__(size= 15, **kwargs)

    # def choose_texture(self):
    #     return random.choice(['cobblestone', 'bricks', 'end_stone_bricks','stone', 'stone_bricks', 'mossy_cobblestone', 'mossy_stone_bricks'])

    def choose_texture(self):
        return 'iron_block'

class FetchS30(FetchS10):
    def __init__(self, **kwargs):
        super().__init__(size=30, **kwargs)

import numpy as np
import math
from gym import spaces
from gym_miniworld.miniworld import MiniWorldEnv, Room
from gym_miniworld.entity import Box, ImageFrame, MeshEnt, Key, Ball, Box_Rect, TextFrame
from gym_miniworld.params import DEFAULT_PARAMS
import copy

key_to_color = {'redstone_block': 'red', 'emerald_block': 'green', 'lapis_block': 'blue'}
color_to_key = {'redstone_block': 'red', 'emerald_block': 'green', 'lapis_block': 'blue'}

block_to_key = {'iron_block': 'iron_sword/iron_sword', 'gold_block': 'gold_sword/gold_sword', 'diamond_block': 'diamond_sword/diamond_sword'}
Y_VEC = np.array([0, 1, 0])
class Choices(MiniWorldEnv):
    """
    Outside environment with two rooms connected by a gap in a wall
    """

    def __init__(self, **kwargs):
        self.door_open = False
        super().__init__(
            **kwargs
        )

        # Allow only the movement actions
        # self.action_space = spaces.Discrete(5)

    def _get_carry_pos(self, agent_pos, ent: MeshEnt):
        """
        Compute the position at which to place an object being carried
        """
        #dist = self.agent.radius + ent.radius + self.max_forward_step
        pos = agent_pos + np.cross(self.agent.dir_vec,(np.array([0,1,0])))*ent.carry_offset[2]+ ent.carry_offset[0]*self.agent.dir_vec
        y_pos = self.agent.cam_height - ent.height+ ent.carry_offset[1]
        pos = pos + Y_VEC * y_pos
        return pos

    def _gen_world(self):
        # Top
        room0 = self.add_rect_room(
            min_x=-4, max_x=7,
            min_z=0, max_z=8,
            wall_tex='gray_concrete',
            floor_tex='chiseled_quartz_block_top',
            ceil_tex='quartz_block_bottom',
        )
        # Bottom
        room1 = self.add_rect_room(
            min_x=1, max_x=7,
            min_z=-7, max_z=-1,
            wall_tex='diamond_ore',
            floor_tex='spruce_planks',
            ceil_tex='spruce_planks',
        )

        self.connect_rooms(room0, room1, min_x=3, max_x=5, max_y=2)

        # self.box = self.place_entity(Box(color='green'), room=room1, dir=0, pos= np.array([4, 0, -4]))
        self.diamond_pick = self.place_entity(MeshEnt(
            mesh_name='diamond_pickaxe/diamond_pickaxe', height=1, static=True, check_collisions=True
        ), dir=0, room=room1,  pos=np.array([4, 1, -4]))

        self.goal = self.place_entity(MeshEnt(
            mesh_name='diamond_block/diamond_block', height=1, static=False, check_collisions=False
        ), room=room1, dir=-np.pi/2, pos=np.array([4, 0, -4]))



        key_types = ['iron_sword', 'diamond_sword', 'gold_sword']
        stands = ['iron_block', 'diamond_block', 'gold_block']
        lock_types = ['iron_block', 'diamond_block', 'gold_block']

        self.keys = []
        stands_used= []
        num_keys = 3
        for i in range(num_keys):
            choice = np.random.randint(0, len(key_types))
            chosen_key = key_types[choice]
            stand = stands[choice]
            key_types.remove(chosen_key)
            stands.remove(stand)
            self.keys.append(MeshEnt(
                mesh_name=f'{chosen_key}/{chosen_key}', height=1, static=False, check_collisions=True,
                carry_offset=np.array([0.65,0.7,0.4]), carry_pitch=1, static_pitch=np.pi/4, carriable= True))
            stands_used.append(MeshEnt(
                mesh_name=f'{stand}/{stand}', height=1, static=True, check_collisions=False))

        placementlist = [[-2.5, 0, 1.5],[-2.5, 0, 4],[-2.5, 0, 6.5]]
        for key, block, place, in zip(self.keys, stands_used, placementlist):
            self.place_entity(key, room=room0, pos=np.array(place)+np.array([0,1.3,-0.35]), dir = np.pi/2)
            self.place_entity(block, room=room0, pos=np.array(place), dir = np.pi/2)


        self.lock_type = np.random.choice(lock_types)
        # self.pressure_plate = Box_Rect(color=key_color, size_x=1.8, size_y=0.2, size_z=1.8)

        self.lock = [MeshEnt(
            mesh_name=f'{ self.lock_type}/{ self.lock_type}', height=1, static=False, check_collisions=False
        ) for _ in range (2)]

        for i, segment in enumerate(self.lock):
            self.place_entity(segment, room0, pos= np.array([5.45, 0 +i, -0.35]), dir = 0)

        # if i < 2:
        #         self.place_entity(plate, room0, pos= np.array([5.5+i, -.95, 1.5]), dir = 0)
        #     else:
        #         i %= 2
        #         self.place_entity(plate, room0, pos= np.array([5.5+i, -.95, .5]), dir = 0)


        # self.pressure_plate = MeshEnt(
        #     mesh_name=f'{key}/{key}', height=2, static=False, check_collisions=False
        # )
        # self.place_entity(self.pressure_plate, room0, pos=[6, -1.8, 1], dir=0)
        # self.key = Key(color='yellow')
        # self.place_entity(self.key, room=room0)
        self.door_left = MeshEnt(
            mesh_name=f'wooden_door/wooden_door', height=2, static=False, radius=1.005
        )
        self.door_right = MeshEnt(
            mesh_name=f'wooden_door/wooden_door', height=2, static=False, radius=1.005
        )

        self.place_entity(self.door_left
                          ,
                          pos=np.array([3.5, 0, -1]),
                          dir=0
                          )
        self.place_entity(self.door_right
                          ,
                          pos=np.array([4.5, 0, -1]),
                          dir=np.pi
                          )

        self.place_agent(room=room0, dir=np.pi / 2, pos=np.array([5, 0, 5]))

    def check_plate(self, block):
        if 4.5<block.pos[0] <6 and -1<block.pos[2] <0.5:
            return  True
        else:
            return False

    def step(self, action):
        obs, reward, done, info = super().step(action)
        # print(self.door)
        # print(self.entities)

        for key in self.keys:
            if self.check_plate(key) :
                if key.mesh_name ==  block_to_key[self.lock_type] and not self.door_open:
                    key.check_collisions = False
                    self.agent.carrying_right = None
                    key.pos = self.lock[0].pos + np.array([0.35,0.7,0.5])
                    key.dir = 0
                    key.set_mesh('green_sword/green_sword')
                    for i, segment in enumerate(self.lock):
                        segment.set_mesh('emerald_block/emerald_block')
                    # for segment in self.lock:
                    #     segment.set_mesh(mesh_name=f'iron_block/iron_block')
                    self.door_left.check_collisions = False
                    self.door_left.dir = -np.pi/2
                    self.door_left.pos = np.array([3.1, 0, -.5])

                    self.door_right.check_collisions = False
                    self.door_right.dir = -np.pi/2
                    self.door_right.pos = np.array([4.9, 0, -0.5])
                    self.door_open = True

        # if self.near(self.key) and not self.agent.carrying:
        #
        #     self.agent.carrying = self.key
        #
        # if self.collide(self.key, self.door) and not self.door_open:
        #
        #     self.entities.remove(self.door)
        #     self.entities.remove(self.key)
        #     self.agent.carrying = None
        #     self.door_open = True

        if self.collide(self.goal):
            reward += self._reward()
            done = True

        return obs, reward, done, info

    def reset(self):
        self.door_open = False
        return super().reset()

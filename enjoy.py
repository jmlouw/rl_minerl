import gym
import gym.wrappers
from ttictoc import Timer
from time import sleep
import torch
import imageio
import cv2
# user imports
from agents.agent_base_q import BaseQAgent
from utils.utils import make_miniworld_env, agents, create_dir
from wrappers.minigrid_env_wrappers import MiniGridPOVRGBWrapper
from wrappers.minerl_env_wrappers import Actionspace_Yaw, MineRLObsWrapper

from old.minerl.minerl_utils import register_env
from utils.gen_args import Arguments


#used to demo trained models
class TestAgent(object):
    def __init__(self):
        pass


def test_agent(agent_: BaseQAgent, env, model, render):
    agent = agent_
    state = env.reset()
    device = torch.device('cuda:0')
    tot_return = 0
    no_episodes = 5
    print(env.action_space)
    time = Timer()
    time.start()
    img_array = []
    dir = create_dir(['demo_videos'])
    for i in range (no_episodes):
        return_ = 0
        while 1:
            action = agent.test_select_action(state, model, env.action_space, 0.01, device)
            state, reward, done, info = env.step(action)
            return_ += reward
            sleep(0.01)
            if render:
                img, top_view = env.render(mode='human', view = 'agent')
                top_view = cv2.resize(top_view, dsize=(250, 250), interpolation=cv2.INTER_CUBIC)
                img[:250,-251:-1, :] = top_view
                img_array.append(img)
            if done:
               # print(f'Episode: {i+1}, return: {np.round(return_, 2)}')
                tot_return += return_
                state = env.reset()
                break
    elapsed = time.stop()
    print(elapsed)
    print(elapsed/no_episodes)
    print('Average Return: ', tot_return/no_episodes)
    imageio.mimwrite(dir /'recording.mp4', img_array, fps = 20)


if __name__ == '__main__':
    args = Arguments()
    args.parse_args()
    args.process_args(enjoy=True)

    env = make_miniworld_env(args)
    print(f'path to nn-model: {args.model_dir}')
    print(f'action space: {env.action_space}')
    print(f'observation space: {env.observation_space}')
    env.seed(2)
    agent = agents[args.agent_type]
    full_model_path = args.model_dir
    model = agent.load_model_inference(full_model_path, env.observation_space, env.action_space, args.dueling)
    test_agent(agent, env, model, args.render)

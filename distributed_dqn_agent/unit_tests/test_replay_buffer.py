import ray

from unittest import TestCase
from distributed_dqn_agent.replay_buffer import ReplayBuffer


class TestReplayBuffer(TestCase):
    def setUp(self) -> None:
        ray.init()

    def test_add(self):
        test_buffer = ReplayBuffer.remote(10)
        self.assertEqual(len(ray.get(test_buffer.get_storage.remote())), 0)
        ray.wait([test_buffer.add.remote(0)])
        self.assertEqual(ray.get(test_buffer.get_storage.remote()), [0])
        for i in range(1, 12):
            ray.wait([test_buffer.add.remote(i)])
        self.assertEqual(ray.get(test_buffer.get_storage.remote()), [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11])
        ray.wait([test_buffer.add.remote(13)])
        self.assertEqual(ray.get(test_buffer.get_storage.remote()), [13, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11])

    def test_add_batch(self):
        test_buffer = ReplayBuffer(10)
        self.assertEqual(test_buffer._maxsize, 12)
        test_buffer.add_batch([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
        self.assertEqual(test_buffer._storage, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
        test_buffer.add_batch([10, 11, 12, 13])
        self.assertEqual(test_buffer._storage, [12, 13, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11])

    def test_remove_to_fit(self):
        test_buffer = ReplayBuffer(10)
        self.assertEqual(test_buffer._maxsize, 12)
        test_buffer.add_batch([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
        test_buffer.remove_to_fit()
        self.assertEqual(test_buffer._storage, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
        test_buffer.add(10)
        test_buffer.remove_to_fit()
        self.assertEqual(test_buffer._storage, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
        test_buffer.add_batch([11, 12])
        self.assertEqual(test_buffer._storage, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
        self.assertEqual(test_buffer._storage, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
        test_buffer.remove_to_fit()
        self.assertEqual(test_buffer._storage, [3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
        test_buffer.add_batch([13, 14, 15, 16])
        self.assertEqual(test_buffer._storage, [15, 16, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14])
        test_buffer.remove_to_fit()
        test_buffer.add_batch([17,18])
        self.assertEqual(test_buffer._storage, [5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 17, 18])

    def test_sample(self):
        self.fail()

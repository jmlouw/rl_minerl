from unittest import TestCase
import ray
import copy
import time
from ttictoc import  Timer

# user imports
from distributed_dqn_agent.replay_buffer import ReplayBuffer
from distributed_dqn_agent.dist_utils import ParameterServer
from utils.utils import make_miniworld_env
from utils.gen_args import Arguments
from distributed_dqn_agent.actor import Experience


class TestParameterServer(TestCase):
    def setUp(self) -> None:
        ray.init()
        args = Arguments()
        args.parse_args()
        env = make_miniworld_env(args)
        self.replay_buffer = ReplayBuffer.remote(2 ** 10)
        self.param_server = ParameterServer.remote(actor_ids=['0', '1'], replay_buffer=self.replay_buffer,
                                                   observation_space=env.observation_space, batch_size=32)
        state = env.reset()
        for i in range(1000):
            action = env.action_space.sample()
            next_state, reward, done, info = env.step(action)
            ray.get(self.replay_buffer.add.remote(copy.deepcopy(Experience(state, action, reward, next_state, done))))
            state = copy.copy(next_state)

        self.param_server.start_loading_buffer.remote()
        time.sleep(1)

    def test_sample_miniexpbuffer(self):
        total_time = Timer()
        total_time.start()
        t = Timer()
        t.start()
        time_toget_req = 0
        for i in range(500):
            tempr, time = (ray.get(self.param_server.sample_miniexpbuffer.remote(t)))
            extracted = ray.get(self.param_server.extract_tensors.remote(tempr))
            time_toget_req += time_toget_req
        print(time_toget_req)
        print(total_time.stop())

        pass

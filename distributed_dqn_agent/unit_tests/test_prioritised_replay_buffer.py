import ray
from unittest import TestCase
import copy

from distributed_dqn_agent.prioritised_replay_buffer import PrioritisedReplayBuffer, get_priorities
from utils.utils import make_miniworld_env
from utils.gen_args import Arguments
from distributed_dqn_agent.actor import Experience
import numpy as np

class TestPrioritisedReplayBuffer(TestCase):
    def setUp(self) -> None:
        ray.init()
        args = Arguments()
        args.parse_args()
        self.test_per_buffer = PrioritisedReplayBuffer.remote(20)
        self.env = make_miniworld_env(args)

        self.actor_buffer = []
        state = self.env.reset()
        for i in range(10):
            action = self.env.action_space.sample()
            next_state, reward, done, info = self.env.step(action)
            self.actor_buffer.append((Experience(state, action, reward, next_state, done)))
            state = (next_state)

    def test_add_batch(self):
        priorities = get_priorities(np.ones((len(self.actor_buffer),)) * 0.1, 0.01, 0.6)
        print(priorities)
        self.test_per_buffer.add_batch.remote(self.actor_buffer, priorities)
        print(test_per_buffer)
        pass

    def test__sample_proportional(self):
        self.fail()

    def test_sample(self):
        self.fail()

    def test_update_priorities(self):
        self.fail()

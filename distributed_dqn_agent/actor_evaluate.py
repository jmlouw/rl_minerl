import ray
import numpy as np

# user imports
from utils.gen_args import Arguments
from utils.utils import nn_dict, make_miniworld_env, state_np2tensor
from distributed_dqn_agent.dist_utils import Actor_Logger


@ray.remote
class Actor_Eval():
    def __init__(self, param_server, result_run_path, args: Arguments):
        self.param_server = param_server
        self.actor_id = 'actor_eval'
        self.device = args.actor_device
        self.actor_buffer_size = args.actor_buffer_size
        self.env = make_miniworld_env(args, eval_mode=True)

        self.env.seed(1)

        # create local networks - calculate td errors and e-greedy
        nn_model = nn_dict[args.nn_model]
        self.q_network = nn_model(self.env.observation_space, self.env.action_space.n, args.dueling)
        q_dict, _, learner_step, _, level_number, _ = ray.get(self.param_server.get_params.remote())
        self.q_network.load_state_dict(q_dict)
        self.q_network.eval()

        # logging
        self.episode = 0
        self.update_step = 0
        self.last_learner_step = learner_step
        self.seconds = 0
        self.start_time = None

        self.result_run_path = result_run_path
        self.save_results = args.save_results
        if self.save_results:
            self.logger = Actor_Logger(self.result_run_path / f'{self.actor_id}.csv')

        self.run_complete = False
        print(f'{self.actor_id} created')

    def run(self):
        print(f'{self.actor_id} started evaluating')

        state = self.env.reset()
        episode_return = 0
        episode_return_buffer = []
        episode_success_count = 0
        steps = 0
        episode_steps_buffer = []
        last_episodes = 10
        seconds = 0

        while True:
            action = self.select_action(state)
            steps += 1
            next_state, reward, done, info = self.env.step(action)
            episode_return += reward
            state = next_state

            if done:
                episode_return_buffer.append(episode_return)
                if info['success']:
                    episode_success_count += 1

                # prepare new rollout
                self.episode += 1
                episode_steps_buffer.append(steps)
                steps = 0
                episode_return = 0
                state = self.env.reset()

                if self.run_complete:
                    # print(f'run completed, actor {self.actor_id} is doing {last_episodes} more episodes')
                    last_episodes -= 1

                learner_step, new_seconds = self.load_params_learner()
                if last_episodes <= 0:
                    if self.save_results and len(episode_return_buffer) != 0:
                        self.log(new_seconds, episode_return_buffer, episode_steps_buffer, episode_success_count)
                    break
                if learner_step != self.last_learner_step:
                    if self.save_results and len(episode_return_buffer) != 0:
                        self.log(seconds, episode_return_buffer, episode_steps_buffer, episode_success_count)
                        episode_steps_buffer.clear()
                    # self.param_server.push_returns.remote(return_mean, self.actor_id, self.last_learner_step)
                    self.last_learner_step = learner_step
                    seconds = new_seconds
                    episode_return_buffer.clear()
                    episode_success_count = 0

    def select_action(self, state):
        """
        Receives a state
        return: Returns a action chosen with epsilon greedy strategy
                Returns the q-value of chosen actiom
                Returns the index of the max q-value action
        """
        state = state_np2tensor(state, self.device)
        return self.q_network(state).argmax(dim=1).item()

    def load_params_learner(self):
        # print(f'actor-{self.actor_id} requested network parameters')
        q_dict, _, learner_step, seconds, _, run_complete = ray.get(self.param_server.get_params.remote())
        self.q_network.load_state_dict(q_dict)
        self.run_complete = run_complete
        return learner_step, seconds

    def set_start_time(self, time):
        self.start_time = time

    def log(self, seconds, episode_return_buffer, episode_steps, episode_success_count):
        return_mean = np.mean(episode_return_buffer)
        return_std = np.std(episode_return_buffer)
        ep_steps_mean = np.mean(episode_steps)
        ep_steps_std = np.std(episode_steps)
        dict_to_log = {'learning_step': self.last_learner_step, 'return_mean': return_mean, 'return_std': return_std,
                       'seconds': seconds, 'avg_steps_ep': ep_steps_mean,
                       'std_steps_ep': ep_steps_std, 'num_samples': len(episode_return_buffer),
                       'num_success': episode_success_count}
        self.logger.log_results(dict_to_log)

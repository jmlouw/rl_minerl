import ray
import random

@ray.remote
class ReplayBuffer():
    def __init__(self, size):
        """Create Replay buffer.
        Parameters
        ----------
        size: int
            Max number of transitions to store in the buffer. When the buffer
            overflows the old memories are dropped.
        """
        self._storage = []
        self._size = size
        self._next_idx = 0
        self.num_transitions_added = 0

    def add_batch(self, batch_to_add):
        batch = batch_to_add
        len_batch = len(batch)
        self.num_transitions_added += len_batch
        end = self._next_idx + len_batch

        if end < self._size:
            self._storage[self._next_idx:end] = batch
        else:
            len_first_group = self._size - self._next_idx
            len_sec_group = len_batch - len_first_group
            self._storage[self._next_idx:] = batch[:len_first_group]
            self._storage[:len_sec_group] = batch[len_first_group:]

        self._next_idx = (self._next_idx + len_batch) % self._size

    def sample(self, batch_size):
        """Sample a batch of experiences.
        Parameters
        ----------
        batch_size: int
            How many transitions to sample.
        Returns
        -------
        obs_batch: np.array
            batch of observations
        act_batch: np.array
            batch of actions executed given obs_batch
        rew_batch: np.array
            rewards received as results of executing act_batch
        next_obs_batch: np.array
            next set of observations seen after executing act_batch
        done_mask: np.array
            done_mask[i] = 1 if executing act_batch[i] resulted in
            the end of an episode and 0 otherwise.
        """
        # idxes = [random.randint(0, len(self._storage) - 1) for _ in range(batch_size)]
        # return self._storage[idxes]
        return random.sample(self._storage, batch_size)

    def get_num_transitions(self):
        return self.num_transitions_added

    def __len__(self):
        return len(self._storage)


    # def get_rewards(self):
    #     return self.rewards

    # def __str__(self):
    #     return str(self._storage)

        # def add(self, data):
    #     if self._next_idx >= len(self._storage):
    #         self._storage.append(data)
    #     else:
    #         self._storage[self._next_idx] = data
    #     self._next_idx = (self._next_idx + 1) % self._maxsize

    # def remove_to_fit(self):
    #     if len(self._storage) > self._size:
    #         num_removed = len(self._storage) - self._size
    #         self._storage = self._storage[num_removed:]
    #         self._next_idx -= num_removed
    #         self._next_idx = self._size

    # def can_provide_sample(self, batch_size):
    #     return len(self._storage) > batch_size

    # def get_storage(self):
    #     return self._storage

    # def add_reward(self, rew):
    #     self.rewards.append(rew)

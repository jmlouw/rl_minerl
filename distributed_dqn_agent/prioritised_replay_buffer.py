import ray
import numpy as np
import random
from utils.segment_tree import SumSegmentTree, MinSegmentTree

@ray.remote
class PrioritisedReplayBuffer():
    def __init__(self, size, alpha=0.6, e=0.01):
        """Create Prioritized Replay buffer.
        Parameters
        ----------
        size: int
            Max number of transitions to store in the buffer. When the buffer
            overflows the old memories are dropped.
        alpha: float
            how much prioritization is used
            (0 - no prioritization, 1 - full prioritization)
        """

        self._storage = []
        self._size = size
        # self._maxsize = size * int(0.2 * size)
        self._next_idx = 0
        self.num_transitions_added = 0

        assert alpha >= 0
        self._alpha = alpha

        assert e > 0
        self._e = e

        # it_capacity = 1
        # while it_capacity < size:
        #     it_capacity *= 2

        self._it_sum = SumSegmentTree(self._size)
        self._it_min = MinSegmentTree(self._size)

        # self._max_priority = 1.0

    def add_batch(self, batch, td_errors):
        assert len(batch) == len(td_errors)
        priorities = self.get_priorities(td_errors)

        len_batch = len(batch)
        self.num_transitions_added += len_batch
        end = self._next_idx + len_batch

        if end <= self._size:
            self._storage[self._next_idx:end] = batch
            self._it_sum.set_batch(np.arange(self._next_idx, end), priorities)
            self._it_min.set_batch(np.arange(self._next_idx, end), priorities)
        else:
            len_first_group = self._size - self._next_idx
            len_sec_group = len_batch - len_first_group

            self._storage[self._next_idx:] = batch[:len_first_group]
            self._it_sum.set_batch(np.arange(self._next_idx, self._size), priorities[:len_first_group])
            self._it_min.set_batch(np.arange(self._next_idx, self._size), priorities[:len_first_group])

            self._storage[:len_sec_group] = batch[len_first_group:]
            self._it_sum.set_batch(np.arange(0, len_sec_group), priorities[len_first_group:])
            self._it_min.set_batch(np.arange(0, len_sec_group), priorities[len_first_group:])

        self._next_idx = (self._next_idx + len_batch) % self._size

    def sample(self, batch_size, beta):
        """Sample a batch of experiences.
        compared to ReplayBuffer.sample
        it also returns importance weights and idxes
        of sampled experiences.
        Parameters
        ----------
        batch_size: int
            How many transitions to sample.
        beta: float
            To what degree to use importance weights
            (0 - no corrections, 1 - full correction)
        Returns
        -------
        obs_batch: np.array
            batch of observations
        act_batch: np.array
            batch of actions executed given obs_batch
        rew_batch: np.array
            rewards received as results of executing act_batch
        next_obs_batch: np.array
            next set of observations seen after executing act_batch
        done_mask: np.array
            done_mask[i] = 1 if executing act_batch[i] resulted in
            the end of an episode and 0 otherwise.
        weights: np.array
            Array of shape (batch_size,) and dtype np.float32
            denoting importance weight of each sampled transition
        idxes: np.array
            Array of shape (batch_size,) and dtype np.int32
            idexes in buffer of sampled experiences
        """
        assert beta > 0

        idxes = self._sample_proportional(batch_size)

        weights = []
        p_min = self._it_min.min() / self._it_sum.sum()  # smallest probability?
        max_weight = (p_min * len(self._storage)) ** (-beta)  # max weight?

        for idx in idxes:  # calculate weights
            p_sample = self._it_sum[idx] / self._it_sum.sum()
            weight = (p_sample * len(self._storage)) ** (-beta)
            weights.append(weight / max_weight)

        weights = np.array(weights)
        # encoded_sample = self._encode_sample(idxes)
        # return tuple(list(encoded_sample) + [weights, idxes])
        samples = [self._storage[i] for i in idxes]
        return (samples, idxes, weights)

    def _sample_proportional(self, batch_size):
        res = []
        p_total = self._it_sum.sum(0, len(self._storage) - 1)
        every_range_len = p_total / batch_size
        for i in range(batch_size):
            mass = random.random() * every_range_len + i * every_range_len
            idx = self._it_sum.find_prefixsum_idx(mass)
            res.append(idx)
        return res


    def update_priorities(self, idxes, td_errors):
        """Update priorities of sampled transitions.
        sets priority of transition at index idxes[i] in buffer
        to priorities[i].
        Parameters
        ----------
        idxes: [int] (np.abs(td_errors) + prioritized_replay_eps)
            List of idxes of sampled transitions
        priorities: [float]
            List of updated priorities corresponding to
            transitions at the sampled idxes denoted by
            variable `idxes`.
        """
        assert len(idxes) == len(td_errors)
        priorities = self.get_priorities(td_errors)
        self._it_sum.set_batch(np.array(idxes), priorities)
        self._it_min.set_batch(np.array(idxes), priorities)

    def get_priorities(self, td_errors):
        return (np.abs(td_errors) + self._e) ** self._alpha

    def __len__(self):
        return len(self._storage)

    def get_num_transitions(self):
        return self.num_transitions_added


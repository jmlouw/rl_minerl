import gym
import torch
import ray
import numpy as np
from typing import List
from collections import deque

# user imports
from utils.utils import make_miniworld_env, nn_dict, state_np2tensor, to_tensor, make_miniworld_env1
from utils.gen_args import Arguments
from distributed_dqn_agent.dist_utils import Actor_Logger
from distributed_dqn_agent.learner import Experience

@ray.remote(num_cpus=1, num_gpus=0)
class Actor(object):
    def __init__(self, main_buffer, param_server, actor_id, eps, seed, result_run_path, args: Arguments):
        self.incremental_learning = args.transfer_learning

        self.param_server = param_server
        self.main_buffer = main_buffer
        self.prioritised_replay = args.prioritised_replay
        self.actor_id = actor_id

        self.device = args.actor_device

        self.actor_buffer_size = args.actor_buffer_size

        self.env = make_miniworld_env(args)

        self.env.seed(int(seed))
        # create local networks - calculate td errors and e-greedy
        nn_model = nn_dict[args.nn_model]
        self.q_network = nn_model(self.env.observation_space, self.env.action_space.n, args.dueling)
        self.target_q_network = nn_model(self.env.observation_space, self.env.action_space.n, args.dueling)

        q_dict, target_q_dict, learner_step, _, new_env_name, _ = ray.get(self.param_server.get_params.remote())
        self.q_network.load_state_dict(q_dict)
        self.target_q_network.load_state_dict(target_q_dict)
        self.q_network.eval()
        self.target_q_network.eval()

        self.eps = eps
        self.gamma = args.gamma

        # logging
        self.episode = 0
        self.update_step = 0
        self.last_learner_step = learner_step
        self.seconds = 0
        self.start_time = None
        self.n_step = args.n_step
        self.save_results = args.save_results
        self.result_run_path = result_run_path
        self.n_step_deque = deque(maxlen=args.n_step)

        if self.save_results:
            self.logger = Actor_Logger(self.result_run_path / f'{actor_id}.csv')

        self.max_steps = args.max_steps
        self.intrinsic_step_reward = args.intrinsic_step_reward
        self.cur_env_name = new_env_name
        self.run_complete = False

        self.args = args
        self.seed = seed

        print(f'{self.actor_id} created')

    def set_start_time(self, time):
        self.start_time = time

    def run_actor(self, num_steps=None):
        if num_steps is None:
            num_steps = np.inf
        else:
            num_steps = num_steps
        print(f'{self.actor_id} started acting for num steps {num_steps}')

        state = self.env.reset()
        episode_return = 0
        episode_return_buffer = []
        episode_success_count = 0
        steps = 0
        cur_step = 0
        last_episodes = 10
        seconds = 0
        episode_steps_buffer = []
        local_buffer = []

        if self.prioritised_replay:
            current_q_deque = deque(maxlen=self.n_step)
            first_element = True
            current_q_values = []
            next_q_indices = []

        while True:
            if np.isfinite(num_steps):
                cur_step += 1
                if cur_step >= num_steps:
                    break

            action, current_q_value, q_max_action = self.select_action(state)
            steps += 1
            next_state, reward, done, info = self.env.step(action)
            exp = Experience(state, action, reward, next_state, done, None)

            if self.prioritised_replay:
                old_size = len(self.n_step_deque)
                len_n_deque = append_transition(local_buffer, self.n_step_deque, exp, done, current_q_values,
                                                current_q_deque,
                                                current_q_value)
                append_q_max_action(next_q_indices, q_max_action, done, len_n_deque, old_size == len_n_deque,
                                    first_element)
                if len(local_buffer) >= 1:
                    first_element = False
            else:
                append_transition_non_prioritised(local_buffer, self.n_step_deque, exp, done, self.gamma)

            episode_return += reward
            state = next_state

            if len(local_buffer) >= self.actor_buffer_size:
                if self.prioritised_replay:
                    current_q_values_torch = torch.stack(current_q_values)
                    if local_buffer[-1][-1].done == False:
                        # if last transition appended is done "-1's" are already added as place holders for max next q values and therefore not necessary to find the max q index of next state
                        # next_q_indices.append(self.q_network(state_np2tensor(next_state, self.device)).argmax(dim=1))
                        next_q_indices.append(
                            self.q_network(state_np2tensor(next_state, self.device)).max(dim=1).values)
                    next_q_indices_torch = torch.cat(next_q_indices)
                    td_errors = self.get_td_errors(local_buffer, current_q_values_torch, next_q_indices_torch)
                    self.main_buffer.add_batch.remote(list(local_buffer), td_errors)
                    current_q_values.clear()
                    next_q_indices.clear()
                    first_element = True
                else:
                    insert_n_step_returns(list(local_buffer), self.gamma)
                    self.main_buffer.add_batch.remote(list(local_buffer))
                local_buffer.clear()

            if done:
                episode_return_buffer.append(episode_return)
                if info['success']:
                    episode_success_count += 1
                # prepare new rollout
                self.episode += 1
                episode_steps_buffer.append(steps)
                steps = 0
                episode_return = 0
                state = self.env.reset()

                if self.run_complete:
                    # print(f'run completed, actor {self.actor_id} is doing {last_episodes} more episodes')
                    last_episodes -= 1

                learner_step, new_env_name, new_seconds = self.load_params_learner()
                if self.incremental_learning and self.env.spec.id != new_env_name:
                    print(f'new env name: {new_env_name}')
                    self.cur_env_name = new_env_name
                    self.update_env()

                if last_episodes <= 0:
                    if self.save_results and len(episode_return_buffer) != 0:
                        self.log(new_seconds, episode_return_buffer, episode_steps_buffer, episode_success_count)
                    break

                if learner_step != self.last_learner_step:
                    if self.save_results and len(episode_return_buffer) != 0:
                        self.log(seconds, episode_return_buffer, episode_steps_buffer, episode_success_count)
                        episode_steps_buffer.clear()
                    # self.param_server.push_returns.remote(return_mean, self.actor_id, self.last_learner_step)
                    self.last_learner_step = learner_step
                    seconds = new_seconds
                    episode_return_buffer.clear()
                    episode_success_count = 0

        self.n_step_deque.clear()
        print(f'actor-{self.actor_id} stopped acting')

    def select_action(self, state):
        """
        Receives a state
        return: Returns a action chosen with epsilon greedy strategy
                Returns the q-value of chosen actiom
                Returns the index of the max q-value action
        """
        state = state_np2tensor(state, self.device)
        with torch.no_grad():
            q_values = self.q_network(state)
            max_q_values = q_values.max(dim=1)
        if self.eps > np.random.random():
            action = np.random.randint(self.env.action_space.n)
            current_q_value = q_values[:, action]
        else:
            action = max_q_values.indices.item()
            current_q_value = max_q_values.values
        # return action, current_q_value, max_q_values.indices
        return action, current_q_value, max_q_values.values

    def get_td_errors(self, batch, current_q_values, next_q_indices):
        rewards, nth_states, dones = self.extract_tensors(batch)
        # next_q_values = self.get_next_double(nth_states, dones, next_q_indices)
        next_q_values = next_q_indices
        target_q_values = (next_q_values * self.gamma) + rewards
        td_errors = (target_q_values.unsqueeze(1) - current_q_values).detach().numpy()

        return td_errors.flatten()

    def get_next_double(self, next_states, dones, next_q_indices):
        with torch.no_grad():
            final_state_locations = dones
            non_final_state_locations = np.logical_not(final_state_locations)
            non_final_states = {}
            for key in self.env.observation_space.spaces:
                non_final_states[key] = next_states[key][non_final_state_locations]
            batch_size = len(dones)
            values = torch.zeros(batch_size).to(self.device)
            next_q_indices = next_q_indices[non_final_state_locations]
            # print(next_q_indices)
            values[non_final_state_locations] = self.target_q_network(non_final_states).gather(dim=1,
                                                                                               index=next_q_indices).squeeze(
                1)
        return values

    def extract_tensors(self, experiences):
        # Convert batch of Experiences to Experience of batches
        sample_generator = batch_sample_generator(experiences, self.n_step, self.gamma)
        rewards, next_states_list, dones = zip(*sample_generator)

        next_states = {
            k: [dic[k] for dic in next_states_list]
            for k in self.env.observation_space.spaces
        }
        for key in self.env.observation_space.spaces:  # keys 'pov' 'act_hist'
            if key == 'pov':
                next_states[key] = to_tensor(next_states[key], self.device)
            elif key == 'act_hist':
                next_states[key] = torch.tensor(np.array([np.array(act_hist) for act_hist in next_states[key]])).to(
                    self.device).float()
            else:
                next_states[key] = torch.tensor(np.stack(next_states[key])).to(self.device).float()

        rewards = torch.tensor(np.stack(rewards)).to(self.device).float()  # check of dit reg is
        dones = list(dones)  # check of dit reg is
        return rewards, next_states, dones

    def load_params_learner(self):
        # print(f'actor-{self.actor_id} requested network parameters')
        q_dict, target_q_dict, learner_step, seconds, level_number, run_complete = ray.get(
            self.param_server.get_params.remote())
        self.run_complete = run_complete
        self.q_network.load_state_dict(q_dict)
        self.target_q_network.load_state_dict(target_q_dict)
        return learner_step, level_number, seconds
        # print(f'actor-{self.actor_id} loaded network parameters')

    def log(self, seconds, episode_return_buffer, episode_steps, episode_success_count):
        return_mean = np.mean(episode_return_buffer)
        return_std = np.std(episode_return_buffer)
        ep_steps_mean = np.mean(episode_steps)
        ep_steps_std = np.std(episode_steps)
        dict_to_log = {'learning_step': self.last_learner_step, 'return_mean': return_mean, 'return_std': return_std,
                       'seconds': seconds, 'avg_steps_ep': ep_steps_mean,
                       'std_steps_ep': ep_steps_std, 'num_samples': len(episode_return_buffer),
                       'num_success': episode_success_count}
        self.logger.log_results(dict_to_log)

    def update_env(self):
        self.env =  make_miniworld_env1(self.cur_env_name, self.args)
        self.env.seed(int(self.seed))
        self.env.reset()
        print(f'actor {self.actor_id} env is updated to {self.cur_env_name}')


def append_transition(local_buffer: list, transitions: deque, data, done, current_q_values, current_q_deque,
                      current_q_value):
    transitions.append(data)
    current_q_deque.append(current_q_value)
    len_before = len(transitions)
    if len(transitions) == transitions.maxlen or done:  # have to append when done even if deque is not full
        append_to_local_buffer(local_buffer, transitions, done)
        append_to_current_q_values(current_q_values, current_q_deque, done)
    return len_before


def append_transition_non_prioritised(local_buffer: list, transitions: deque, data, done, gamma):
    transitions.append(data)
    if len(transitions) == transitions.maxlen or done:  # have to append when done even if deque is not full
        append_to_local_buffer(local_buffer, transitions, done)


def append_to_local_buffer(local_buffer: list, transitions: deque, is_done: bool):
    if not transitions:
        return
    local_buffer.append(list(transitions))
    if is_done:
        del transitions[0]
        append_to_local_buffer(local_buffer, transitions, is_done)


def append_to_current_q_values(local_buffer: list, transitions: deque, is_done: bool):
    if not transitions:
        return
    local_buffer.append(transitions[0])
    if is_done:
        del transitions[0]
        append_to_current_q_values(local_buffer, transitions, is_done)


def append_q_max_action(next_q_indices, q_max_action, done, n_step, full_deque, first_element):
    # do not append if it is the first q_max_action of a 'new/empty' local buffer, only interested in q_max_actions afterwards
    # do not append if at least one n_step_deque of a new episode is appended, only interested in q_max_actions afterwards
    # if done and n_step_deque has not been appended to local buffer once, append -1 for the current size of the n_step deque to q_max_actions, do not append q_max_actions
    if full_deque and not first_element:
        next_q_indices.append(q_max_action)
    if done:
        for _ in range(n_step):
            # next_q_indices.append(torch.tensor([-1]))
            next_q_indices.append(torch.tensor([0.0]))


def batch_sample_generator(batch: List[List[Experience]], n_step: int, gamma: float, cache={}):
    if 'empty_state' not in cache:
        cache['empty_state'] = dict([
            (key, np.zeros_like(val)) for key, val in batch[0][0].next_state.items()
        ])
    for sample in batch:
        _, _, rewards, next_states, dones, _ = zip(*sample)
        next_state = next_states[-1] if len(sample) >= n_step else cache['empty_state']
        if sample[0].discounted_reward is None:
            discounted_reward = discount_reward(rewards, gamma)
            sample[0].discounted_reward = discounted_reward
        else:
            discounted_reward = sample[0].discounted_reward
        yield discounted_reward, next_state, dones[-1]


def insert_n_step_returns(batch: List[List[Experience]], gamma: float):
    for sample in batch:
        _, _, rewards, next_states, dones, _ = zip(*sample)
        if sample[0].discounted_reward is None:
            discounted_reward = discount_reward(rewards, gamma)
            sample[0].discounted_reward = discounted_reward


def discount_reward(rewards, gamma=0.99):
    return sum([rew * gamma ** idx for idx, rew in enumerate(rewards)])

import ray
from collections import defaultdict, namedtuple
import pandas as pd
import time

Experience = namedtuple('Experience', ('state', 'action', 'reward', 'next_state', 'done'))


def to_cpu(q_net_dict, q_target_net_dict):
    cpu_q_net_dict = {}
    for key, val in q_net_dict.items():
        cpu_q_net_dict[key] = val.cpu()
    cpu_q_target_net_dict = {}
    for key, val in q_target_net_dict.items():
        cpu_q_target_net_dict[key] = val.cpu()
    return cpu_q_net_dict, cpu_q_target_net_dict

class Actor_Logger():
    def __init__(self, path_to_file):
        self.file_created = False
        self.path_to_file = path_to_file

    def create_log_file(self, path_to_file, columns: list, index: str):
        df = pd.DataFrame(
            columns=columns)
        df.set_index(index, inplace=True)
        df.to_csv(path_to_file)

    def log_results(self,dict_to_log):
        if not self.file_created:
            self.create_log_file(self.path_to_file, dict_to_log.keys(), 'learning_step')
            self.file_created = True

        learner_step = dict_to_log.pop('learning_step')
        pd.DataFrame(dict_to_log , index=[learner_step]).to_csv(
            self.path_to_file, mode='a',
            header=False, index=True)

@ray.remote
class ParameterServer(object):
    def __init__(self, actor_ids: list, q_net_dict=None, q_target_net_dict=None):
        # q_net_dict, q_target_net_dict = to_cpu(q_net_dict, q_target_net_dict)
        self.q_net_dict = q_net_dict
        self.q_target_net_dict = q_target_net_dict
        self.learner_step = 0
        self.returns_dict = {actor_id: defaultdict(list) for actor_id in actor_ids}
        self.cur_env_name = None
        self.run_complete = False
        self.start_time = time.time()

    def set_start_time(self):
        self.start_time = time.time()

    def update_params(self, q_net_dict, q_target_net_dict, learner_step, new_env_name, run_complete = False):
        # q_net_dict, q_target_net_dict = to_cpu()
        self.cur_env_name = new_env_name
        self.learner_step = learner_step
        self.q_net_dict = q_net_dict
        self.q_target_net_dict = q_target_net_dict
        self.run_complete = run_complete
        self.time = time.time() - self.start_time
        # print(f'learner_step: {learner_step}, time: {self.time}, run complete: {self.run_complete}')

    def get_params(self):
        return self.q_net_dict, self.q_target_net_dict, self.learner_step, self.time, self.cur_env_name, self.run_complete

    def push_returns(self, returns, actor_id, associated_learner_step):
        self.returns_dict[actor_id][associated_learner_step] += returns

    def get_returns_dict(self):
        dict_to_return = self.returns_dict
        for actor in self.returns_dict.keys():
            self.returns_dict[actor].clear()
        return self.dict_to_return

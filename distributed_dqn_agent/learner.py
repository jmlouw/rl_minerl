import ray
import torch
import torch.optim as optim
import torch.nn.functional as F
import numpy as np
from collections import deque
import time
import threading
import pandas as pd
from ttictoc import Timer
from recordclass import recordclass
from typing import List

# user imports
from utils.gen_args import Arguments
from utils.utils import to_tensor, nn_dict, save_model, create_dir, LinearSchedule
from distributed_dqn_agent.prioritised_replay_buffer import PrioritisedReplayBuffer

Experience = recordclass('Experience', ('state', 'action', 'reward', 'next_state', 'done', 'discounted_reward'))

@ray.remote(num_gpus=1, num_cpus=2)
class Learner(object):
    # from levels import miniworld
    def __init__(self, action_space, observation_space, replay_buffer, param_server, args: Arguments, run_num, result_run_path = None):
        self.transfer_learning = args.transfer_learning
        self.run_num = run_num
        self.device = args.device
        self.double = args.double_dqn
        self.replay_buffer = replay_buffer
        self.batch_size = args.batch_size

        # buffer info
        self.prioritised_replay = args.prioritised_replay
        if args.prioritised_replay:
            print('learner using priotised experience replay')
            self.beta_schedule = LinearSchedule(args.num_learning_steps,
                                                initial_p=args.per_beta0,
                                                final_p=1.0)
            self.beta_value = args.per_beta0
        else:
            print('learner using normal experience replay')

        # hyperparameters
        self.target_update = args.target_update
        self.gamma = args.gamma
        lr = args.learning_rate
        self.n_step = args.n_step

        self.action_space = action_space
        self.observation_space = observation_space

        # initialize networks
        self.model_name = args.nn_model
        nn_model = nn_dict[args.nn_model]

        self.q_net = nn_model(self.observation_space, self.action_space.n, args.dueling)
        print(f'Neural Network Model: {self.q_net}')
        print(f'Number learnable paramaters: {self.q_net.get_num_learnable_paramaters()}')
        self.q_net.to(self.device)
        self.q_target_net = nn_model(self.observation_space, self.action_space.n, args.dueling).to(self.device)
        self.q_target_net.load_state_dict(self.q_net.state_dict())
        self.q_target_net.eval()
        self.optimizer = optim.Adam(params=self.q_net.parameters(), lr=lr)

        self.param_server = param_server

        self.col_n_b = 4
        self.total_to_collect = self.batch_size*self.col_n_b
        self.mini_buffer_len = 100
        self.mini_buffers = [deque(maxlen=self.mini_buffer_len),deque(maxlen=self.mini_buffer_len)]
        self.mini_buffer_index = 0
        self.transitions_collected = 0
        self.transitions_processed = 0

        # logging
        self.save_interval = args.save_interval
        self.save_results = args.save_results

        self.sample_timer = Timer()
        self.extract_timer = Timer()


        if self.save_results:
            self.result_run_path = result_run_path
            self.model_dir = create_dir([self.result_run_path.parent, 'models'])

            self.csv_mean_sampled_reward = self.result_run_path / 'mean_sampled_reward.csv'
            df = pd.DataFrame(columns=['learning_step', 'mean_sampled_reward', 'seconds'])
            df.set_index('learning_step', inplace=True)
            df.to_csv(self.csv_mean_sampled_reward)

        self.start_time = None

        self.list_index = 0
        self.env_list = args.env_list
        self.current_env = self.env_list[0]
        print(f'current env {self.current_env}')

        self.level_increase_every = args.level_increase_every
        self.target_replay_ratio = args.target_replay_ratio
        self.tot_learning_steps = args.num_learning_steps
        print('learner created')

    def log_results(self, mean_reward_sampled, learning_step):
        seconds = time.time() - self.start_time
        pd.DataFrame({'mean_sampled_reward': mean_reward_sampled, 'seconds': seconds}, index=[learning_step]).to_csv(self.csv_mean_sampled_reward, mode='a', header=False,
                                                                                                                     index=True)

    def set_start_time(self, time):
        self.start_time = time

    def start_loading_buffer(self):
        self.t0 = threading.Thread(target=self._load_mini_buffer, args=(self.mini_buffers[0],), daemon=True)
        self.t0.start()
        self.t1 = threading.Thread(target=self._load_mini_buffer, args=(self.mini_buffers[1],), daemon=True)
        self.t1.start()

    def _load_mini_buffer(self, mini_buffer):
        while True:
            if len(mini_buffer) < self.mini_buffer_len:
                if self.prioritised_replay:
                    experiences, idxes, is_weights = ray.get(self.replay_buffer.sample.remote(self.total_to_collect, self.beta_value))
                    for i in range(0, self.batch_size*(self.col_n_b-1)+1, self.batch_size):
                        exp_extracted = self.extract_tensors(experiences[i:i + self.batch_size])
                        mini_buffer.append((exp_extracted, idxes[i:i + self.batch_size], is_weights[i:i + self.batch_size]))
                else:
                    experiences = ray.get(self.replay_buffer.sample.remote(self.total_to_collect))
                    for i in range(0, self.batch_size*(self.col_n_b-1)+1, self.batch_size):
                        exp_extracted = self.extract_tensors(experiences[i:i + self.batch_size])
                        mini_buffer.append(exp_extracted)
                self.transitions_collected += (self.total_to_collect)
            else:
                time.sleep(1)

    def update_learner(self, steps, tot_learning_step):
        total_reward_sampled = 0

        if self.target_replay_ratio:
            cur_replay_ratio =  ray.get(self.replay_buffer.get_num_transitions.remote())/(tot_learning_step+1)
            while cur_replay_ratio < self.target_replay_ratio:
                # print(f'cur_replay_ratio: {cur_replay_ratio}')
                cur_replay_ratio =  ray.get(self.replay_buffer.get_num_transitions.remote())/(tot_learning_step+1)
                time.sleep(0.005)
            print(f'cur_replay_ratio: {cur_replay_ratio}')

        for step in range(steps):
            while len(self.mini_buffers[self.mini_buffer_index]) == 0:
                self.mini_buffer_index = (self.mini_buffer_index+1)%len(self.mini_buffers)
                time.sleep(0.1)

            if self.prioritised_replay:
                self.beta_value = self.beta_schedule.value(tot_learning_step)
                exp_extracted, idxes, is_weights = self.mini_buffers[self.mini_buffer_index].popleft()
                states, actions, rewards, nth_states, dones = exp_extracted
            else:
                states, actions, rewards, nth_states, dones = self.mini_buffers[self.mini_buffer_index].popleft()

            self.transitions_processed += self.batch_size
            total_reward_sampled += np.sum(rewards.cpu().detach().numpy())
            current_q_values = self.get_current(states, actions)

            if self.double:
                next_q_values = self.get_next_double(nth_states, dones)
            else:
                next_q_values = self.get_next(nth_states, dones)

            target_q_values = (next_q_values * self.gamma) + rewards

            if self.prioritised_replay:
                td_errors = (target_q_values.unsqueeze(1) - current_q_values).cpu().detach().numpy().flatten()
                self.replay_buffer.update_priorities.remote(idxes, td_errors)
                loss = F.smooth_l1_loss(current_q_values.squeeze(), target_q_values.detach(),
                                        reduction='none') * torch.from_numpy(is_weights).to(self.device)
                loss = loss.mean()
            else:
                loss = F.smooth_l1_loss(current_q_values.squeeze(), target_q_values.detach())

            self.optimizer.zero_grad()
            loss.backward()
            for param in self.q_net.parameters():
                param.grad.data.clamp_(-1, 1)
            self.optimizer.step()

            tot_learning_step += 1
            if (tot_learning_step) % self.target_update == 0:
                self.q_target_net.load_state_dict(self.q_net.state_dict())

            if self.transfer_learning and (tot_learning_step % self.level_increase_every==0) and (self.list_index < len(self.env_list)-1):
                self.list_index +=1
                self.current_env = self.env_list[self.list_index]
                print(f'learner changed env to: {self.current_env}')

            # if (tot_learning_step) % 100 == 0: # trim buffer and save weights to param server
            #     self.replay_buffer.remove_to_fit.remote()

            if (tot_learning_step ) % 500 == 0: # update the parameter server for actors every 500 steps
                self.update_param_server(tot_learning_step)

            if self.save_results and (tot_learning_step) % self.save_interval == 0:
                save_model(self.model_dir, tot_learning_step, f'model_{self.run_num}', self.model_name, self.q_net.state_dict(), self.optimizer.state_dict())

        mean_reward_sampled = (total_reward_sampled/(steps*self.batch_size))
        if self.save_results:
            self.log_results(mean_reward_sampled, tot_learning_step)

        return tot_learning_step, mean_reward_sampled

    def update_param_server(self, learner_step):
        self.param_server.update_params.remote(self.q_net.cpu().state_dict(),
                                               self.q_target_net.cpu().state_dict(), learner_step, self.current_env, learner_step == self.tot_learning_steps)
        self.q_net = self.q_net.to(self.device)
        self.q_target_net = self.q_target_net.to(self.device)


    def get_current(self, states, actions):
        # actions = actions.to(self.device)
        return self.q_net(states).gather(dim=1, index=actions.unsqueeze(-1))

    def get_next(self, next_states, dones):
        final_state_locations = dones
        non_final_state_locations = np.logical_not(final_state_locations)
        non_final_states = {}
        for key in self.observation_space.spaces:
            non_final_states[key] = next_states[key][non_final_state_locations]

        batch_size = self.batch_size
        values = torch.zeros(batch_size).to(self.device)
        values[non_final_state_locations] = self.q_target_net(non_final_states).max(dim=1)[0].detach()
        return values

    def get_next_double(self, next_states, dones):
        with torch.no_grad():
            final_state_locations = dones
            non_final_state_locations = np.logical_not(final_state_locations)
            non_final_states = {}
            for key in self.observation_space.spaces:
                non_final_states[key] = next_states[key][non_final_state_locations]

            batch_size = self.batch_size
            values = torch.zeros(batch_size).to(self.device)
            next_state_argmax_actions = self.q_net(non_final_states).argmax(dim=1)

            values[non_final_state_locations] = self.q_target_net(non_final_states).gather(dim=1,
                                                                                           index=next_state_argmax_actions.unsqueeze(
                                                                                               -1)).squeeze(1)
        return values

    def extract_tensors(self, experiences):
        # Convert batch of Experiences to Experience of batches
        sample_generator = batch_sample_generator(experiences, self.n_step)
        states_list, actions, rewards, nth_states_list, dones = zip(*sample_generator)
        states = {
            k: [dic[k] for dic in states_list]
            for k in self.observation_space.spaces
        }
        nth_states = {
            k: [dic[k] for dic in nth_states_list]
            for k in self.observation_space.spaces
        }
        for key in self.observation_space.spaces:  # keys 'pov' 'act_hist'
            if key == 'pov':
                states[key] = to_tensor(states[key], self.device)
                nth_states[key] = to_tensor(nth_states[key], self.device)
            elif key == 'act_hist':
                states[key] = torch.tensor(np.array([np.array(act_hist) for act_hist in states[key]])).to(self.device).float()
                nth_states[key] = torch.tensor(np.array([np.array(act_hist) for act_hist in nth_states[key]])).to(self.device).float()
            elif key == 'carrying':
                states[key] = torch.tensor(np.array(states[key])).unsqueeze(-1).to(self.device).float()
                nth_states[key] = torch.tensor(np.array(nth_states[key])).unsqueeze(-1).to(self.device).float()
            else:
                states[key] = torch.tensor(np.stack(states[key])).to(self.device).float()
                nth_states[key] = torch.tensor(np.stack(nth_states[key])).to(self.device).float()
        actions = torch.tensor(np.stack(actions)).to(self.device)
        rewards = torch.tensor(np.stack(rewards)).to(self.device).float()  # check of dit reg is
        dones = list(dones)  # check of dit reg is

        return states, actions, rewards, nth_states, dones

    def get_buffer_info(self):
        return self.transitions_collected, self.transitions_processed

def batch_sample_generator(batch: List[List[Experience]], n_step: int, cache={}):
    if 'empty_state' not in cache:
        cache['empty_state'] = dict([
            (key, np.zeros_like(val)) for key, val in batch[0][0].next_state.items()
        ])

    for sample in batch:
        states, actions, rewards, next_states, dones, _ = zip(*sample)
        nth_state = next_states[-1] if len(sample) >= n_step else cache['empty_state']
        yield states[0], actions[0], sample[0].discounted_reward, nth_state, dones[-1]

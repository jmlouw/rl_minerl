import time
import ray
import numpy as np
from ttictoc import Timer
import wandb
import csv

# user imports
from utils.utils import create_dir
from distributed_dqn_agent.actor_evaluate import Actor_Eval
from utils.gen_args import Arguments
from distributed_dqn_agent.learner import Learner
from distributed_dqn_agent.replay_buffer import ReplayBuffer
from distributed_dqn_agent.prioritised_replay_buffer import PrioritisedReplayBuffer
from distributed_dqn_agent.dist_utils import ParameterServer
from distributed_dqn_agent.actor import Actor

class Dist_Simulation:
    def __init__(self, action_space, observation_space, run_num, args: Arguments):
        """
        Simulator for distributed DQN
        """
        ray.init(local_mode=False, memory=15000000000, object_store_memory=33000000000)
        self.run_timer = Timer()
        self.run_timer.start()

        self.save_results = args.save_results
        exp_run_dir_id = None
        if self.save_results:
            exp_dir = args.exp_result_dir
            self.exp_run_dir = create_dir([exp_dir, str(run_num)])
            exp_run_dir_id = ray.put(self.exp_run_dir)

        args_id = ray.put(args)

        self.num_actors = args.num_actors
        eps = np.linspace(args.exp_eps_start, args.exp_eps_stop, self.num_actors)
        actor_ids = [f'actor{i}_eps_{ep:.3f}' for i, ep in zip(range(args.num_actors), eps)]
        print(actor_ids)

        if args.prioritised_replay:
            self.replay_buffer = PrioritisedReplayBuffer.remote(args.replay_buffer_capacity, args.per_alpha,
                                                                args.per_eps)
        else:
            self.replay_buffer = ReplayBuffer.remote(args.replay_buffer_capacity)

        self.param_server = ParameterServer.remote(actor_ids=actor_ids)

        self.learner = Learner.remote(action_space, observation_space, self.replay_buffer, self.param_server, args_id,
                                      run_num,
                                      exp_run_dir_id)

        update_params_id = self.learner.update_param_server.remote(0)
        ray.wait([update_params_id], 1)

        if args.env_seed:
            seeds = list(np.arange(args.num_actors, dtype = np.uint8) + run_num * args.num_actors)
        else:
            seeds = [None] * self.num_actors
        print(f'seeds : {seeds}')

        self.actors = [
            Actor.remote(self.replay_buffer, self.param_server, actor_id, ep, seed, exp_run_dir_id, args_id) for
            actor_id, ep, seed in zip(actor_ids, eps, seeds)]

        self.buffer_preload_size = args.buffer_preload_size
        self.deploy_eval_actor = args.eval_actor

        if self.save_results and self.deploy_eval_actor:
            self.actor_evaluate =  Actor_Eval.remote(self.param_server, exp_run_dir_id, args_id)

        self.log_wandb = args.log_wandb
        # create files to save results and statistics
        if self.save_results:
            with open(self.exp_run_dir / "stats.csv", "w") as fd:
                writer = csv.writer(fd)
                writer.writerow(['name', 'value'])

    def train(self, num_learning_steps):
        self.log_stat('preload_size', self.buffer_preload_size)
        self.log_stat('gradient_steps', num_learning_steps)

        learning_step = 0
        learning_timer = Timer()
        learning_time = 0

        # initially fill the main buffer
        num_pre_steps = int(self.buffer_preload_size / self.num_actors)
        print(f'preloading buffer, running each actor for {num_pre_steps} steps')
        learning_timer.start()
        preload_acting = [actor.run_actor.remote(num_pre_steps) for actor in self.actors]
        ray.wait(preload_acting, len(preload_acting))
        preloadtime = learning_timer.stop()
        print(f'buffer preload size: {ray.get(self.replay_buffer.__len__.remote())}, time: {preloadtime}')
        # time.sleep(100000)
        start_time = time.time()
        set_actor_start_time = [actor.set_start_time.remote(start_time) for actor in self.actors]
        set_learner_start_time = [self.learner.set_start_time.remote(start_time)]
        ray.wait(set_actor_start_time, len(set_actor_start_time))
        ray.wait(set_learner_start_time, 1)
        if self.save_results and self.deploy_eval_actor:
            set_eval_start_time = [self.actor_evaluate.set_start_time.remote(start_time)]
            ray.wait(set_eval_start_time, 1)

        self.learner.start_loading_buffer.remote() #start loading learners mini buffers
        print('thread to load mini_exp_buffer started')

        actors= [actor.run_actor.remote() for actor in self.actors]
        if self.save_results and self.deploy_eval_actor:
            print('starting actor evaluate')
            actor_eval = [self.actor_evaluate.run.remote()]

        self.param_server.set_start_time.remote()
        while learning_step < num_learning_steps:
            learning_timer.start()
            learning_step, mean_reward_sampled = ray.get(self.learner.update_learner.remote(1000, learning_step))
            del_learning_time = learning_timer.stop()
            learning_time += del_learning_time
            mean_eval_return = 0

            print(
                f'learning_step: {learning_step}, '
                f'time elapsed: {learning_time:.2f}, '
                f'delta_learning_time: {del_learning_time:.2f}, '
                f'mean_reward_sampled: {mean_reward_sampled:.4f}')

            # wandb logging
            if self.log_wandb:
                if mean_eval_return is not None:
                    wandb.log({'mean evaluator return': mean_eval_return}, step=learning_step, commit=False)
                wandb.log({'mean reward sampled': mean_reward_sampled}, step=learning_step, commit=False)

        ray.wait(actors, len(actors))
        run_time = self.run_timer.stop()
        self.log_stat('run_time', run_time)
        print('all actors are terminated')

        if self.save_results and self.deploy_eval_actor:
            ray.wait(actor_eval, 1)
            print('evaluation actor is terminated')

        self.log_stat('preloadtime', preloadtime)
        self.log_stat('learning_time', learning_time)
        self.log_stat('transitions_generated', ray.get(self.replay_buffer.get_num_transitions.remote()))
        t_collected, t_processed = ray.get(self.learner.get_buffer_info.remote())
        self.log_stat('transitions_collected', t_collected)
        self.log_stat('transitions_processed', t_processed)

        ray.shutdown()

    # def restart_actors(self, actor_buffers):
    #     ready_ids, _remaining_ids = ray.wait(list(actor_buffers.keys()), timeout=0)
    #     while len(ready_ids) > 0:
    #         for ready_id in ready_ids:
    #             actor = actor_buffers.pop(ready_id)
    #             print(f'restarting actor: {actor} with acting id: {ready_id}')
    #             acting_id = actor.run_actor.remote()
    #             print(f'new acting id: {acting_id}')
    #             actor_buffers[acting_id] = actor
    #         ready_ids, _remaining_ids = ray.wait(list(actor_buffers.keys()), timeout=0)

    def log_stat(self, stat_name, stat_value):
        with open(self.exp_run_dir / 'stats.csv', 'a') as fd:
            writer = csv.writer(fd)
            writer.writerow([stat_name, stat_value])

# def calculate_epsilons(num_actors, exp_eps, exp_alpha):
#     actor_nums = np.arange(num_actors)
#     es = exp_eps**(1+(actor_nums/(num_actors-1))*exp_alpha)
#     return es

# def process_return_dicts(self, actors_return_dict, mean_reward_sampled, evaluator_returns, learner_step):
#     print(f'actors_return_dict: {actors_return_dict}')
#     print(f'evaluator_returns: {evaluator_returns}')
#
#     for actor_key, value_dict in actors_return_dict.items():
#         if len(value_dict) > 0:
#             latest_entry = max(value_dict.keys())
#             if len(value_dict[latest_entry]) > 1:
#                 actor_mean = np.mean(value_dict[latest_entry])
#                 mean_list.append(actor_mean)
#
#                 if self.log_wandb:
#                     wandb.log({f'average return: {actor_key}': actor_mean}, step=latest_entry, commit=False)
#
#     if self.log_wandb and len(mean_list) > 0:
#         print(latest_entry)
#         wandb.log({'average return all actors': np.mean(mean_list)}, step=latest_entry, commit=False)
#         if latest_entry in learner_rewards_sampled.keys():
#             wandb.log({'average reward learner sampled': np.mean(learner_rewards_sampled[latest_entry])},
#                       step=latest_entry, commit=False)
#         if self.deploy_eval_actor and latest_entry in evaluator_returns.keys():
#             wandb.log({'average evaluator return': np.mean(evaluator_returns[latest_entry])}, step=latest_entry,
#                       commit=False)

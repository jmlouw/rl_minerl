#!/usr/bin/env python3

"""
This script allows you to manually control the simulator
using the keyboard arrows.
"""

import sys
import pyglet
from pyglet.window import key
import numpy as np

np.set_printoptions(threshold=sys.maxsize)
import gym
from utils.gen_args import Arguments

args = Arguments()
args.parse_args()
import levels
from wrappers.miniworld_env_wrappers import MiniWorldObsWrapperSkippingStacking

env = gym.make(args.env_name, max_episode_steps=args.max_steps,
               depth_channel_enabled=args.depth_channel_enabled)
print(env.observation_space)
env = MiniWorldObsWrapperSkippingStacking(env, num_frames_skip=args.num_frames_skip,
                                          action_stack_size=args.num_actions_stack,
                                          frame_stack_size=args.num_frames_stack,
                                          stack_every=args.stack_every)

env.seed(None)

# np.random.seed(1)
obs = env.reset()

# myobj = plt.imshow(env.render_depth().squeeze(), interpolation='nearest')

# Create the display window

# myobj = imshow(env.render(mode='rgb', view='top'))
env.render()


def step(action):
    print('step {}/{}: {}'.format(env.step_count + 1, env.max_episode_steps, env.actions(action).name))

    obs, reward, done, info = env.step(action)
    # myobj.set_data(np.array(obs['pov'])[-1, :, :])
    # print(np.max(np.array(obs['pov'])[-1, :, :]))

    # plt.draw()
    # plt.pause(0.001)
    # if reward > 0:
    #     print('reward={:.2f}'.format(reward))

    if done:
        print('reward={:.3f}'.format(reward))
        print('done!')
        # np.random.seed(1)
        env.reset()
    env.render()


@env.unwrapped.window.event
def on_key_press(symbol, modifiers):
    """
    This handler processes keyboard commands that
    control the simulation
    """

    if symbol == key.BACKSPACE or symbol == key.SLASH:
        print('RESET')
        env.reset()
        env.render()
        return

    if symbol == key.ESCAPE:
        env.close()
        sys.exit(0)

    if symbol == key.UP:
        step(env.actions.move_forward)
    elif symbol == key.DOWN:
        step(env.actions.move_back)

    elif symbol == key.LEFT:
        step(env.actions.turn_left)
    elif symbol == key.RIGHT:
        step(env.actions.turn_right)

    elif symbol == key.E or symbol == key.P:
        # if env.agent.carrying:
        #     step(env.actions.drop)
        # else:
        step(env.actions.pickup)
    elif symbol == key.PAGEDOWN or symbol == key.D:
        step(env.actions.drop)

    elif symbol == key.ENTER:
        step(env.actions.done)

    elif symbol == key.F:
        step(env.actions.toggle)


@env.unwrapped.window.event
def on_key_release(symbol, modifiers):
    pass


@env.unwrapped.window.event
def on_draw():
    env.render()


@env.unwrapped.window.event
def on_close():
    pyglet.app.exit()


# Enter main event loop
pyglet.app.run()

env.close()

import matplotlib.pyplot as plt
import numpy as np

# env_name = 'miniworld-FetchTwoRoomsObstacleV1-v0'
# experiment_list = ['7actors','7_actors_noprioritised']
# labels = ['PER', 'Random Sampling']

env_name = 'miniworld-FetchTwoRoomsObstacleV1-v0'
experiment_list = ['1actors','3actors','5actors','7actors','9actors']
labels = ['rr=0.140\nactors=1','rr=0.054\nactors=3','rr=0.032\nactors=5','rr=0.023\nactors=7','rr=0.017\nactors=9']

# env_name = 'miniworld-FetchTwoRoomsObstacleV1-v0'
# experiment_list = ['7actors_n=1','7actors_n=3','7actors_n=5','rpr=0.023_n=7','rpr=0.023_n=9','rpr=0.023_n=11']
# labels = ['n=1','n=3','n=5','n=7','n=9','n=11']

# env_name = 'miniworld-FetchThreeRoomsObstacleSparse-v0'
# experiment_list = ['random_domain_initialisation','random_domain_initialisation_rooms']
# labels = ['random_entity_placement','random_number_rooms', 'sparse_reward_problem']

def create_std_fig(fig_name, x_label, y_label, x_ticks, y_ticks, size_x =5.7, size_y = 4):
    f0 = plt.figure(fig_name, figsize=(size_x,size_y))
    plt.title(fig_name)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.grid()
    #     axes = plt.gca()
    plt.xticks(x_ticks)
    plt.yticks(y_ticks)
    return f0

def runs_dir(dir):
    runs = []
    for i in dir.glob('*/'):
        if (i.is_dir()) and i.name != 'models':
            runs.append(i.name)
    return runs

def actors_dir(dir):
    actors = []
    for i in dir.glob('*.csv'):
        if i.name != 'mean_sampled_reward.csv' and i.name != 'stats.csv' and i.name != 'actor_eval.csv':
            actors.append(i.name)
    return actors

def deviance(means, stds, num_samples):
    total_num_samples = np.sum(num_samples)
    total_mean = np.sum(num_samples*means)/total_num_samples

    # std #
    variances = stds**2
    term1 = np.sum(num_samples*variances)
    term2 = np.sum(num_samples*((means-total_mean)**2))
    std = np.sqrt((term1+term2)/total_num_samples)

    return total_mean, std

def flatten_deque(deq):
    return np.array([item for sublist in deq for item in sublist])
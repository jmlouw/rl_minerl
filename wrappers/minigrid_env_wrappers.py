import gym
from gym import spaces
from gym.spaces import Dict, Box, Discrete
import cv2
import gym_minigrid
from gym_minigrid.wrappers import RGBImgPartialObsWrapper


class MG_Reward(gym.core.RewardWrapper):
    def __init__(self, env):
        super().__init__(env)

    def reward(self, reward):
        return int(reward * 1000)


class MiniGridPOVRGBWrapper(gym.core.ObservationWrapper):
    def __init__(self, env, dim=8):
        super().__init__(env)
        self.dim = dim
        self.tile_size = int(dim / self.env.agent_view_size) + 1

        self.observation_space = Dict({'pov': spaces.Box(
            low=0,
            high=255,
            shape=(dim, dim, 3),
            dtype='uint8'
        )})
        self.action_space = Discrete(3)

    def observation(self, obs):
        env = self.unwrapped
        rgb_img_partial = env.get_obs_render(
            obs['image'],
            tile_size=self.tile_size
        )
        rgb_img_partial = cv2.resize(rgb_img_partial, dsize=(self.dim, self.dim))

        return {
            'mission': obs['mission'],
            'pov': rgb_img_partial
        }


import numpy as np


class FullObsRGBWrapper(gym.core.ObservationWrapper):
    dir_dict = {0: np.array([1., 0., 0., 0.]),
                1: np.array([0., 1., 0., 0.]),
                2: np.array([0., 0., 1., 0.]),
                3: np.array([0., 0., 0., 1.])
                }

    def __init__(self, env, tile_size=1):
        super().__init__(env)
        self.tile_size = tile_size
        # obs_shape = env.observation_space['image'].shape
        self.observation_space = Dict({'pov': Box(
            low=0,
            high=255,
            shape=(self.env.width * tile_size, self.env.height * tile_size, 3),
            dtype='uint8'
        ),
            'direction': Box(
                low=0,
                high=1,
                shape=(4,),
                dtype='uint8'
            )})
        self.action_space = Discrete(3)

    def observation(self, obs):
        env = self.unwrapped
        rgb_img = env.render(
            mode='rgb_array',
            highlight=False,
            tile_size=self.tile_size)

        return {
            # 'mission': obs['mission'],
            'pov': rgb_img,
            'direction': self.dir_dict[obs['direction']]
        }

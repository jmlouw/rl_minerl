import gym
from gym.spaces import Dict, Box, Discrete
import numpy as np
from collections import deque
import copy


class LazyFrames(object):
    def __init__(self, frames):
        """This object ensures that common frames between the observations are only stored once.
        It exists purely to optimize memory usage which can be huge for DQN's 1M frames replay
        buffers.
        This object should only be converted to numpy array before being passed to the model.
        You'd not believe how complex the previous solution was."""
        self._frames = frames
        # self._out = None

    def _force(self):
        # if self._out is None:
        #     self._out = np.concatenate(self._frames, axis=-1)
        #     self._frames = None
        # return self._out
        return np.concatenate(self._frames, axis=-1)

    def __array__(self, dtype=None):
        out = self._force()
        if dtype is not None:
            out = out.astype(dtype)
        return out

    def __len__(self):
        return len(self._force())

    def __getitem__(self, i):
        return self._force()[i]

    def count(self):
        frames = self._force()
        return frames.shape[frames.ndim - 1]

    def frame(self, i):
        return self._force()[..., i]


class MyLazyFrames(LazyFrames):
    def __init__(self, frames, out_shape: list):
        super(MyLazyFrames, self).__init__(frames)
        """Small adaptation for nearly 4x speedup. Need to transpose frames before this."""
        self._out_shape = out_shape  # (stack*ch, w, h)

    def _force(self):
        return np.array(self._frames).reshape(self._out_shape)  # (stack, ch, w, h) -> (stack*ch, w, h)

    def __len__(self):
        return len(self._frames)

    def __getitem__(self, i):
        return self.frame(i)

    def count(self):
        return len(self)

    def frame(self, i):
        return self._force()[i]


def angle_to_onehot(compass, num_directions):
    onehot_list = []
    # print(compass)
    for angle in compass:
        # print(angle)
        one_hot_array = np.zeros((num_directions,))
        if angle >= 0:
            one_hot_array[int(np.round(num_directions*angle/(2*np.pi))%num_directions)] = 1
        onehot_list.append(one_hot_array)

    return np.concatenate(onehot_list)

class MiniWorldObsWrapperSkippingStacking(gym.ObservationWrapper):
    """
    Transpose the observation image tensors for PyTorch
    """
    def __init__(self, env, num_frames_skip, action_stack_size, frame_stack_size, stack_every, compass_num_directions = 8):
        super().__init__(env)
        self.num_actions = env.action_space.n
        self.num_frames_skip = num_frames_skip
        self.action_stack_size = action_stack_size
        self.frame_stack_size = frame_stack_size
        self.stack_every = stack_every
        self.compass_num_directions = compass_num_directions
        self.step_count = 0
        self.action_stack = action_stack_size>0
        obs_space = {}
        old_pov_shape = self.observation_space.spaces['pov'].shape
        self._new_pov_shape = [old_pov_shape[2]*frame_stack_size, old_pov_shape[0], old_pov_shape[1]]
        obs_space['pov'] = Box(
            low=0,
            high=255,
            shape=tuple(self._new_pov_shape),
            dtype=np.uint8
        )
        if self.action_stack:
            obs_space['act_hist'] = Box(low = 0, high = 1, shape= (self.action_stack_size*self.num_actions, ), dtype = np.uint8)
            self.action_memory = deque([0]*self.action_stack_size, self.action_stack_size)
        if self.compass_num_directions and 'compass' in self.observation_space.spaces:
            obs_space['compass'] = Box(low = 0, high = 1, shape= (self.observation_space.spaces['compass'].shape[0]*self.compass_num_directions, ), dtype = np.uint8)
        obs_space['carrying'] = Box(low = 0, high =1, shape = (1,),dtype =np.uint8)
        self.observation_space = Dict(obs_space)
        self.frame_memory = deque(maxlen=self.frame_stack_size)
        self._empty_state = self.observation_space.sample()
        for key, value in self.observation_space.spaces.items():
            self._empty_state[key] = np.zeros_like(value.sample())


    def step(self, action):
        self.step_count += 1
        tot_rew = 0
        obs_final = copy.copy(self._empty_state)

        for i in range (self.num_frames_skip):
            obs_frame, reward, done, info = self.env.step(action)
            tot_rew += reward
            if done:
                break

        if 'compass' in self.observation_space.spaces:
            obs_final['compass'] = angle_to_onehot(info['compass'], self.compass_num_directions)

        if self.step_count % self.stack_every == 0:
            self.frame_memory.append(obs_frame.transpose(2, 0, 1))
        obs_final['pov'] = MyLazyFrames(list(self.frame_memory), self._new_pov_shape)

        if self.action_stack:
            # obs_final['act_hist'] = np.array(self.action_memory, dtype = np.uint8).flatten()
            obs_final['act_hist'] = indices2one_hot(self.action_memory, self.num_actions)
            # new_action = np.zeros((self.num_actions, ), dtype=np.uint8)
            # new_action[action] = 1
            self.action_memory.append(action)
        obs_final['carrying'] = info['carrying']
        return obs_final, tot_rew, done, info

    def reset(self, **kwargs):
        self.step_count = 0
        obs_final = copy.copy(self._empty_state)
        frame_reset = (self.env.reset(**kwargs))

        self.frame_memory = deque([frame_reset.transpose(2, 0, 1)]*self.frame_stack_size, maxlen=self.frame_stack_size)
        obs_final['pov'] = MyLazyFrames(list(self.frame_memory), self._new_pov_shape)

        if self.action_stack:
            noop_action = np.zeros((self.num_actions,), dtype=np.uint8)
            noop_action[self.env.actions.no_op] = 1
            self.action_memory = deque([0]*self.action_stack_size, self.action_stack_size)
            # obs_final['act_hist'] = np.array(self.action_memory, dtype=np.uint8).flatten()
            obs_final['act_hist'] = indices2one_hot(self.action_memory, self.num_actions)
        obs_final['carrying'] = 0
        return obs_final

class MiniWolrdActionWrapper(gym.ActionWrapper):
    def __init__(self, env, actions_list):
        super().__init__(env)
        self.action_dict = dict([(i, j) for i, j in zip(range(len(actions_list)), actions_list)])
        self.action_space = Discrete(len(actions_list))

    def action(self, action):
        return self.action_dict[action]


def indices2one_hot(indices, nb_classes):
    """
    Convert an iterable of indices to one-hot encoded list.

    You might also be interested in sklearn.preprocessing.OneHotEncoder

    Parameters
    ----------
    indices : iterable
        iterable of indices
    nb_classes : int
        Number of classes
    dtype : type

    Returns
    -------
    one_hot : list

    Examples
    --------
    >>> indices2one_hot([0, 1, 1], 3)
    [[1, 0, 0], [0, 1, 0], [0, 1, 0]]
    >>> indices2one_hot([0, 1, 1], 2)
    [[1, 0], [0, 1], [0, 1]]
    """
    if nb_classes < 1:
        raise ValueError(
            "nb_classes={}, but positive number expected".format(nb_classes)
        )

    one_hot = []
    for index in indices:
        one_hot.append([0] * nb_classes)
        one_hot[-1][index] = 1
    return [item for sublist in one_hot for item in sublist]

# def to_gray(obs):
#     obs = (
#             0.30 * obs[:,:,0] +
#             0.59 * obs[:,:,1] +
#             0.11 * obs[:,:,2]
#     )
#     return np.expand_dims(obs, axis=2)
#
# class MiniWorldObsWrapper(gym.ObservationWrapper):
#     """
#     Transpose the observation image tensors for PyTorch
#     """
#
#     def __init__(self, env, num_frames = 4):
#         super().__init__(env)
#         self.num_frames = num_frames
#         # low = self.observation_space.low[0, 0, 0]
#         # high = self.observation_space.high[0, 0, 0]
#         shape = (self.observation_space.shape[0], self.observation_space.shape[1], self.num_frames * 3)
#         dtype = self.observation_space.dtype
#         self.observation_space = Dict({'pov': Box(low=0, high=255, shape=shape, dtype= dtype)})
#
#     def step(self, action):
#         tot_rew = 0
#         obs_final = self.observation_space.spaces
#         obs_stacked = np.zeros(self.observation_space['pov'].shape, np.uint8)
#         for i in range (self.num_frames):
#             obs_frame, reward, done, info = self.env.step(action)
#             obs_stacked[:,:,3*i:3*(i+1)] = obs_frame
#             tot_rew += reward
#             if done:
#                 break
#         obs_final['pov'] = obs_stacked
#         #print('OBSWRAPPER\n', obs_final)
#         return obs_final, tot_rew, done, info
#
#     def reset(self, **kwargs):
#         obs_final = self.observation_space.spaces
#         obs_frame = self.env.reset(**kwargs)
#
#         obs_stacked = np.zeros(self.observation_space.spaces['pov'].shape, dtype=np.uint8)
#         for i in range (self.num_frames):
#             obs_stacked[:,:,3*i:3*(i+1)] = obs_frame
#
#         obs_final['pov'] = obs_stacked
#         return obs_final


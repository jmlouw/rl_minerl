import gym
import numpy as np
from minerl.env.spaces import Discrete, Dict, Box


class MineRLObsWrapper(gym.ObservationWrapper):
    """
    Transpose the observation image tensors for PyTorch
    """

    def __init__(self, env):
        super().__init__(env)
        self.no_frames = 4
        shape = (64, 64, self.no_frames*3)
        self.observation_space = Dict({'pov': Box(low=0, high=255, shape=shape, dtype= np.uint8)})

    def step(self, action):
        tot_rew = 0
        obs_final = self.observation_space.spaces
        obs_stacked = np.zeros(self.observation_space['pov'].shape, np.uint8)
        for i in range (self.no_frames):
            obs_frame, reward, done, info = self.env.step(action)
            obs_stacked[:,:,3*i:3*(i+1)] = obs_frame['pov']
            tot_rew += reward
            if done:
                break
        obs_final['pov'] = obs_stacked
        return obs_final, tot_rew, done, info

    def reset(self, **kwargs):
        obs_final = self.observation_space.spaces
        obs_frame = self.env.reset(**kwargs)['pov']
        obs_stacked = np.zeros(self.observation_space.spaces['pov'].shape, dtype=np.uint8)
        for i in range (self.no_frames):
            obs_stacked[:,:,3*i:3*(i+1)] = obs_frame
        obs_final['pov'] = obs_stacked
        return obs_final


class Actionspace_Yaw(gym.ActionWrapper):
    def __init__(self, env):
        super().__init__(env)
        self.actions_list = [  ('camera', np.array([0, -4])),
                               ('camera', np.array([0, 4])), ('forward', 1),]

        self.action_space = Discrete(len(self.actions_list))

    def action(self, action):
        no_op = self.env.action_space.no_op()

        id = self.actions_list[action][0]
        value = self.actions_list[action][1]
        no_op[id] = value
        return no_op

    # def reset(self, **kwargs):
    #     state = self.env.reset(**kwargs)['pov']
    #     #state = to_tensor(state.copy()).unsqueeze(0)
    #     return state

    # def step(self, action):
    #     next_state, reward, done, info = self.env.step(self.action(action))
    #     # if done:
    #     #     next_state['pov'] = np.zeros_like(next_state['pov'])
    #
    #     if action == 0:
    #         reward +=1
    #
    #     return next_state, reward, done, info

from old.agents.agent_tabular_q import tabular_state

class Actionspace_Tabular(gym.ActionWrapper):
    def __init__(self, env):
        super().__init__(env)
        self.actions_list = [('camera', np.array([0, 0])),
                             ('camera', np.array([0, -90])),
                             ('camera', np.array([0, 90]))]

        self.action_space = Discrete(len(self.actions_list))

    def action(self, action):
        no_op = self.env.action_space.no_op()

        id = self.actions_list[action][0]
        value = self.actions_list[action][1]
        no_op[id] = value

        no_op['forward'] = 1
        return no_op

    def reset(self, **kwargs):
        return tabular_state(self.env.reset(**kwargs)['pov'])


    def step(self, action):
        next_state, reward, done, info = self.env.step(self.action(action))
        next_state = tabular_state(next_state['pov'])
        if action == 0:
            reward +=2
        return next_state, reward, done, info


class LFA_Observation_Wrapper(gym.ObservationWrapper):
    def __init__(self, env, dim =8):
        super().__init__(env)
        self.dim = dim
        self.spacing = int(env.observation_space['pov'].shape[0]/self.dim)

        self.observation_space = Dict({'pov':gym.spaces.Box(
            low=0,
            high=2,
            shape=(dim, dim, 1),
            dtype='uint8'
        )})

        self.no_diff_values= 3

    def observation(self, observation):
        my_image = observation['pov']
        my_image = np.round(my_image/255)

        statedict = {'[00]': 0,
                     '[01]': 1,
                     '[11]': 2,
                     '[10]': 0}

        newimage = np.zeros((self.dim, self.dim, 3))
        state_array = np.zeros((self.dim, self.dim, 1), dtype=np.uint8)

        for i_index, i in enumerate(range(0, len(my_image), self.spacing)):
            for j_index, j in enumerate(range(0, len(my_image), self.spacing)):
                newimage[i_index, j_index, 0] = np.max(my_image[i:i+self.spacing, j:j+self.spacing, 0])
                newimage[i_index, j_index, 1] = np.max(my_image[i:i+self.spacing, j:j+self.spacing, 1])
                newimage[i_index, j_index, 2] = 0

                state_array[i_index, j_index, 0] = statedict[(np.array2string(newimage[i_index,j_index,0:2].astype(int).flatten(), separator=''))]
        observation['pov'] = state_array
        return  observation

# min grid wrapper, move to own file


from unittest import TestCase
import gym
from wrappers.miniworld_env_wrappers import MiniWorldObsWrapper, MiniWorldObsWrapperSkippingStacking
from matplotlib import pyplot as plt


class TestMiniWorldObsWrapper(TestCase):
    @classmethod
    def setUp(self) -> None:
        self.env = gym.make('miniworld-WalkGoal-v0')
        self.env = MiniWorldObsWrapper(self.env)

    def test_observation(self):
        state = self.env.reset()
        for a in range(10):
            next_state, reward, done, info = self.env.step(self.env.action_space.sample())
            if done:
                self.env.reset()
            print(next_state['pov'].shape)
            plt.imshow(next_state['pov'])
            plt.show()
        # print(state['pov'].shape)


class TestMiniWorldGrayObsWrapper(TestCase):
    @classmethod
    def setUp(self) -> None:
        self.env = gym.make('miniworld-WalkGoalRoom-v0', obs_height=4, obs_width=4)
        self.env = MiniWorldObsWrapperSkippingStacking(self.env, frame_stack_size=4, num_frames_skip=4, action_stack_size=4)
        state = self.env.reset()

    def test_reset(self):
        state = self.env.reset()
        print(state['pov'][:, :, 0])
        print(state['pov'][:, :, 1])
        print(state['pov'][:, :, 2])
        print(state['pov'][:, :, 3])
        print(state['act_hist'])


    def test_step(self):

        for i in range(10):
            observation, reward, done, info = self.env.step(i%5)
            # self.env.render()
            # sleep(1)
            # for i in range(4):
            #     print(observation['pov'][:,:, i])
            print(observation['pov'].shape)
            print(observation['act_hist'])

            print('next_step')

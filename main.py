import json
import wandb
from pathlib import Path
import ray
import time
import numpy as np

# user imports
from utils.gen_args import Arguments
from utils.utils import make_miniworld_env
from agents.sim import Simulation
from distributed_dqn_agent.dist_sim import Dist_Simulation

def wandb_init(args: Arguments, run_num):
    tags = []
    if args.prioritised_replay:
        tags.append('prioritised_replay')
    if args.double_dqn:
        tags.append('double_dqn')
    if args.distributional_dqn:
        tags.append('distributional_dqn')
    if args.distributed:
        tags.append('distributed')
    tags.append(args.nn_model)

    wandb.init(config=args.as_dict(), project=args.env_name, tags=tags, group=args.experiment_name,
               name='run_'+str(run_num), reinit=True)

def main(args):
    run_start = 0
    if args.save_results:
        # save a configuration file with arguments to experiment's result directory
        args_dict = args.as_dict()
        args_dict['device'] = str(args_dict['device']) #change device type to string
        args_dict['actor_device'] = str(args_dict['actor_device']) #change actor_device type to string
        with open(args.exp_result_dir / 'config.json', 'w') as outfile:
            json.dump(args_dict, outfile)

        runfile = Path(args.exp_result_dir / 'current_run.txt')
        if runfile.exists():
            with open(runfile, 'r') as outfile:
                run_start = int(outfile.read())

    env = make_miniworld_env(args)
    print(env.observation_space)
    print()
    try:
        for run_num in range(run_start, args.num_runs):  # train agent from scratch for the number of runs specified
            if args.log_wandb:
                wandb_init(args, run_num)
            if args.save_results:
                with open(runfile, 'w') as outfile:
                    outfile.write(str(run_num))
            
            simulator = (Dist_Simulation if args.distributed else Simulation)(env.action_space, env.observation_space,
                                                                            run_num, args)
            simulator.train(args.num_learning_steps)
            if args.log_wandb:
                wandb.join()
    except BaseException as e:
        print(e)
    except Exception as e:
        print(e)
    except KeyboardInterrupt as e:
        pass
    finally:
        time.sleep(0.5)
        ray.shutdown()

if __name__ == '__main__':
    args = Arguments()
    args.parse_args()
    if args.use_config_file and args.save_results and (args.exp_result_dir/'config.json').exists():
        print('config file exists, loading configuration')
        with open(args.exp_result_dir / 'config.json', 'r') as outfile:
            args_config = json.load(outfile)
            for att in args.as_dict().keys():
                args.__setattr__(att, args_config[att])
    print(args)
    main(args)

# def main_single(args):
#     my_agent = agents[args.agent_name]
#     my_wrapper_list = wrappers[args.agent_name]
#
#     if args.gym_type == 'minigrid':
#         env = minigrid_startup(args)
#     elif args.gym_type == 'minerl':
#         env = minerl_startup(args, my_agent, my_wrapper_list)
#     elif args.gym_type == 'miniworld':
#         env = miniworld_startup(args)
#
#     else:
#         raise Exception('Specify a valid gym environment. (minigrid or miniworld or minerl)')
#
#     experiment_specs = {'dual_envs': str(args.dual_env), 'render': str(args.render),
#                         'save_results': str(args.save_results), 'agent': str(args.agent_name),
#                         'num_runs': str(args.num_runs),
#                         'num_episodes_per_run': str(args.num_episodes)}
#     env_specs = {'level_name': str(args.level_name), 'reward_range': str(env.reward_range),
#                  'action_space': str(env.action_space),
#                  'observation_space': str(env.observation_space), 'num_frames_skip': args.num_frames_skip,
#                  'num_frames_stacked': args.frame_stack_size}
#     agent_specs = my_agent(env.action_space, env.observation_space, args).agent_specs
#
#     specs = specs_to_string(experiment_specs, header='Experiment Specifications') + '\n' + \
#             specs_to_string(env_specs, header='Environment Specifications') + '\n' + \
#             specs_to_string(agent_specs, header='Agent Specifications')
#     print(specs)
#
#     tracker = Tracking(env_specs, agent_specs, specs, args)
#     simulator = Simulation(tracker, args=args)  # creates the simulator
#     simulator.run_experiment(my_agent, env, args)
#     env.close()


# def main_distributed(args):
#     import ray
#     ray.init(local_mode=False)
#     env_sample = make_env(args)
#
#     experiment_specs = {'dual_envs': str(args.dual_env), 'render': str(args.render),
#                         'save_results': str(args.save_results), 'agent': str(args.agent_name),
#                         'num_runs': str(args.num_runs),
#                         'num_episodes_per_run': str(args.num_episodes)}
#     env_specs = {'level_name': str(args.level_name), 'reward_range': str(env_sample.reward_range),
#                  'action_space': str(env_sample.action_space),
#                  'observation_space': str(env_sample.observation_space), 'num_frames_skip': args.num_frames_skip,
#                  'num_frames_stacked': args.frame_stack_size}
#
#     specs = specs_to_string(experiment_specs, header='Experiment Specifications') + '\n' + \
#             specs_to_string(env_specs, header='Environment Specifications') + '\n'
#
#     tracking = Tracking(env_specs, {}, experiment_specs, args)
#     learner = Learner(env_sample.action_space, env_sample.observation_space, tracking, my_args = args)
#     #ray.timeline()
#     #ray.wait([learner], 1)
#
#     #learner_specs = learner.specs
#
#             #specs_to_string(learner_specs, header='Agent Specifications')
#     print(specs)
#     learing = learner.learn()
#     ray.timeline()

#
# wrappers = {"DQN": [Actionspace_Yaw],
#             "TQ": [Actionspace_Tabular],
#             "LFA": [Actionspace_Yaw]}


# def wrapmyenv(wrap_list, env):
#     for wrapper in wrap_list:
#         env = wrapper(env)
#     return env


# def minerl_startup(args: Arguments, agent, wrappers_list):
#     env_id = register_env(args.level_name)
#     if args.dual_env:  # dual environments
#         env1 = gym.make(env_id)
#         env1 = agent.wrap_env(env1, args)
#         env1 = wrapmyenv(wrappers_list, env1)
#         env2 = gym.make(env_id)
#         env2 = agent.wrap_env(env2, args)
#         env2 = wrapmyenv(wrappers_list, env2)
#         env = GymManager(env1, env2)
#     else:  # single environment
#         env = gym.make(env_id)
#         env = Actionspace_Yaw(env)
#         env = MineRLObsWrapper(env)
#     env.reset()
#     return env


# def minigrid_startup(args):
#     env = gym.make(args.level_name)
#     env = MiniGridPOVRGBWrapper(env, args.downsampled_dimensions)
#     return env


# def miniworld_startup(args):
#     env = gym.make(args.level_name)
#     env = MiniWolrdActionWrapper(env, args.action_dict)
#     env = MiniWorldObsWrapperSkippingStacking(env, num_frames_skip=args.num_frames_skip,
#                                               frame_stack_size=args.frame_stack_size,
#                                               action_stack_size=args.action_stack_size)
#     return env

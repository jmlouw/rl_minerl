import argcomplete
import torch
from typing import List
from typing_extensions import Literal
from tap import Tap
from pathlib import Path
from gym_miniworld.miniworld import MiniWorldEnv


class Arguments(Tap):
    myenvs = Literal[
        'miniworld-FetchObstacle-v0', 'miniworld-FetchTwoRoomsObstacleSparse-v0', 'miniworld-FetchThreeRoomsObstacleSparse-v0',
        'miniworld-FetchThreeRoomsObstacleRandom-v0', 'miniworld-FetchTwoRoomsObstacleV1-v0','miniworld-FetchTwoRoomsObstacleSparseRandomRooms-v0']

    env_name: myenvs = 'miniworld-FetchTwoRoomsObstacleSparseRandomRooms-v0'
    env_eval_name: myenvs = 'miniworld-FetchThreeRoomsObstacleSparse-v0'
    env_list: List[str] = ['miniworld-FetchObstacle-v0','miniworld-FetchTwoRoomsObstacleSparse-v0', 'miniworld-FetchThreeRoomsObstacleSparse-v0']

    # env arguments
    depth_channel_enabled: bool = False
    level_increase_every: int = int(30000)
    compass_directions: int = 8
    max_steps: int = 1500
    intrinsic_step_reward: float = -0.0001

    num_frames_skip: int = 1  # number of frames to repeat a chosen action
    num_frames_stack: int = 4  # number of frames to stack #5
    stack_every: int = 1  # 3
    num_actions_stack: int = 10  # number of actions to stack

    # 0-no_op; 1-move_forward; 2-turn_left; 3-turn_right; 4-move_back; 5-pickup; 6-drop; 7-toggle
    actions: List[int] = [1, 2, 3, 7]  # actions available to agent
    env_seed: bool = False

    # training/learner arguments
    agent_type: str = 'DQN'
    nn_model: Literal['DQN', 'DQN_Dueling'] = 'DQN_Dueling'  # architecture of the neural network
    dueling: bool = False
    num_learning_steps: int = int(2e6)  # the duration of the experiment in terms of learning steps
    target_update: int = int(2e3)  # interval at witch the target network is set to the q-network
    learning_rate: float = 0.00003  # learning rate, also known as alpha
    gamma: float = 0.99  # discount rate
    batch_size: int = 32
    distributed: bool = False  # enables distributed learning
    distributional_dqn: bool = False  # enables distributional learning
    double_dqn: bool = False  # enables double dqn
    device = None  # device used for training. Default is GPU
    actor_device = None  # device used for training. Default is GPU
    target_replay_ratio: int = 0  # target replay ratio

    # actor arguments
    epsilon: float = 0.01  # for non distributed agent
    actor_buffer_size: int = 50  # the local buffer size of each actor
    num_actors: int = 9
    exp_eps_start: float = 0
    exp_eps_stop: float = 0.1

    # replay buffer arguments
    replay_buffer_capacity: int = int(2 ** 19)  # the max capacity of the replay buffer
    buffer_preload_size: int = int(50e3)  # the amount of transitions generated and stored in the buffer before training
    prioritised_replay: bool = False  # enables prioritised experience replay
    per_alpha: float = 0.6  # alpha for per
    per_beta0: float = 0.4
    per_eps: float = 1e-5
    n_step: int = 7  # multi step update, number of steps stored in a transition

    # experiment and logging arguments
    log_wandb: bool = False  # log to wandb

    num_runs: int = 10  # number of times to run the experiment
    save_results: bool = False  # save results to csv files, default is true
    save_interval: int = int(5e3)  # interval the model is saved
    experiment_name: str = 'default_experiment_name'  # 'fetch_multi_room_distributed_per'  # an unique experiment name required
    eval_actor: bool = False  # deploys an evaluation actor every n learning steps to evaluate the agent's policy

    # enjoy arguments
    render: bool = True
    use_config_file: bool = False
    transfer_learning: bool = False

    def add_arguments(self):
        self.add_argument('-en', '--experiment_name')

    def process_args(self, enjoy=False, manual_control=False) -> None:
        if manual_control:
            return

        from utils.utils import create_dir
        self.device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
        self.actor_device = torch.device('cpu')
        sim_folder = 'distributed' if self.distributed else 'non_distributed'

        if enjoy:
            project_folder = Path(__file__).parent.parent
            if not self.eval_actor:
                self.model_dir = project_folder / 'results' / self.env_name / sim_folder / self.experiment_name / 'models' / 'model_0.pt'
            else:
                self.model_dir = project_folder / 'results' / self.env_eval_name / sim_folder / self.experiment_name / 'models' / 'model_0.pt'
            assert Path.exists(self.model_dir), f'path: {self.model_dir}, does not exist'
            return
        else:
            if self.save_results:
                if not self.eval_actor:
                    self.exp_result_dir = create_dir(['results', self.env_name, sim_folder, self.experiment_name])
                else:
                    self.exp_result_dir = create_dir(['results', self.env_eval_name, sim_folder, self.experiment_name])


if __name__ == '__main__':
    args = Arguments()
    args.parse_args()
    args.process_args()
    print(args)

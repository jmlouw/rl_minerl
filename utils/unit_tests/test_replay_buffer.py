from unittest import TestCase

from old.replay_buffer import ReplayMemory


class TestReplayMemory(TestCase):
    @classmethod
    def setUp(self) -> None:
        self.replay_mem = ReplayMemory(7)

    def test_add(self):
        self.replay_mem.add('Cobus0')
        self.replay_mem.add('Cobus1')
        self.replay_mem.add('Cobus2')
        self.replay_mem.add('Cobus3')
        self.replay_mem.add('Cobus4')
        self.replay_mem.add('Cobus5')
        self.replay_mem.add('Cobus6')
        self.replay_mem.add('Cobus7')
        self.replay_mem.add('Cobus8')

        print(self.replay_mem.memory)
        print(self.replay_mem.sample(5))


    def test_sample(self):
        self.fail()

    def test_can_provide_sample(self):
        self.fail()

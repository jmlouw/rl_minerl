import gym
import torch
import numpy as np
import os
from pathlib import Path
# user imports #
from torch_nn_classes.DQN_Models import DQN_Dueling
from wrappers.miniworld_env_wrappers import LazyFrames


def save_model(path, step, model_file_name, nn_name, q_net_state_dict, optimizer_state_dict):
    file_path = path / (model_file_name + '.pt')
    model_dict = {
        'nn_name': nn_name,
        'step': step,
        'model_state_dict': q_net_state_dict,
        'optimizer_state_dict': optimizer_state_dict,
    }
    torch.save(model_dict, file_path)


def create_dir(pathlist):
    project_folder = Path(__file__).parent.parent
    cur_dir = project_folder
    for folder in pathlist:
        cur_dir /= folder
        try:
            os.makedirs(cur_dir)
        except OSError:
            pass
    return cur_dir


def to_tensor(obs, device):
    if isinstance(obs, list):
        obs = np.array([np.array(ob) for ob in obs])
        obs = torch.from_numpy(obs)
        return obs.to(device).float().div(255)
    if isinstance(obs, LazyFrames):
        obs = np.array(obs)
    if isinstance(obs, np.ndarray):
        if obs.ndim == 2:
            obs = obs[:, :, None]
        assert 2 < obs.ndim < 5, f'Observation ndims is not within the range [3, 4], got {obs.ndim}'
        if obs.ndim == 3:  # Single image observation
            obs = torch.from_numpy(obs).to(device).float().div(255)
            return obs.unsqueeze(0)
    return obs  # Should be a tensor


def unique_path(directory, name_pattern):
    counter = 0
    while True:
        counter += 1
        path = directory / name_pattern.format(counter)
        if not path.exists():
            return path


def state_np2tensor(state, device, new_state={}):
    new_state.update(state)
    for key in state.keys():
        if key == 'pov':
            new_state['pov'] = to_tensor(state['pov'], device)
        elif key == 'act_hist':
            new_state[key] = torch.tensor(np.array([np.array(act_hist) for act_hist in new_state[key]])).to(
                device).unsqueeze(0).float()
        elif key == 'carrying':
            new_state[key] = torch.tensor(state[key]).to(device).unsqueeze(0).unsqueeze(0).float()
        else:
            new_state[key] = torch.tensor(state[key]).to(device).unsqueeze(0).float()
    # print(new_state['pov'].shape)
    # print(new_state['carrying'].shape)
    return new_state


from wrappers.miniworld_env_wrappers import MiniWolrdActionWrapper, MiniWorldObsWrapperSkippingStacking
from utils.gen_args import Arguments


def make_miniworld_env(args: Arguments, eval_mode = False):
    """
    creates a miniworld env and wraps with the actions wrapper and observation wrapper
    """
    import levels
    level_name = args.env_name if not eval_mode else args.env_eval_name
    print(level_name)
    env = gym.make(level_name, max_episode_steps=args.max_steps,
                   intrinsic_step_reward=args.intrinsic_step_reward, depth_channel_enabled = args.depth_channel_enabled)

    env = MiniWolrdActionWrapper(env, args.actions)
    env = MiniWorldObsWrapperSkippingStacking(env, num_frames_skip=args.num_frames_skip,
                                              frame_stack_size=args.num_frames_stack,
                                              action_stack_size=args.num_actions_stack,
                                              stack_every=args.stack_every,
                                              compass_num_directions=args.compass_directions)
    return env

def make_miniworld_env1(env_name, args: Arguments):
    """
    creates a miniworld env and wraps with the actions wrapper and observation wrapper
    """
    import levels
    env = gym.make(env_name, max_episode_steps=args.max_steps,
                   intrinsic_step_reward=args.intrinsic_step_reward, depth_channel_enabled = args.depth_channel_enabled)

    env = MiniWolrdActionWrapper(env, args.actions)
    env = MiniWorldObsWrapperSkippingStacking(env, num_frames_skip=args.num_frames_skip,
                                              frame_stack_size=args.num_frames_stack,
                                              action_stack_size=args.num_actions_stack,
                                              stack_every=args.stack_every,
                                              compass_num_directions=args.compass_directions)
    return env


nn_dict = {'DQN_Dueling': DQN_Dueling}
from agents.agent_dqn import DQN_Agent
from agents.agent_lfa import Agent_LFA

agents = {"DQN": DQN_Agent,
          "LFA": Agent_LFA}


class LinearSchedule(object):
    def __init__(self, schedule_timesteps, final_p, initial_p=1.0):
        """Linear interpolation between initial_p and final_p over
        schedule_timesteps. After this many timesteps pass final_p is
        returned.
        Parameters
        ----------
        schedule_timesteps: int
            Number of timesteps for which to linearly anneal initial_p
            to final_p
        initial_p: float
            initial output value
        final_p: float
            final output value
        """
        self.schedule_timesteps = schedule_timesteps
        self.final_p = final_p
        self.initial_p = initial_p

    def value(self, t):
        """See Schedule.value"""
        fraction = min(float(t) / self.schedule_timesteps, 1.0)
        return self.initial_p + fraction * (self.final_p - self.initial_p)

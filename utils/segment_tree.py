import operator
import numpy as np
# openai baseline https://github.com/openai/baselines/blob/ea25b9e8b234e6ee1bca43083f8f3cf974143998/baselines/common/segment_tree.py#L134

class SegmentTree(object):
    def __init__(self, capacity, operation, neutral_element):
        """Build a Segment Tree data structure.
       https://en.wikipedia.org/wiki/Segment_tree
       Can be used as regular array, but with two
       important differences:
           a) setting item's value is slightly slower.
              It is O(lg capacity) instead of O(1).
           b) user has access to an efficient ( O(log segment size) )
              `reduce` operation which reduces `operation` over
              a contiguous subsequence of items in the array.
       Paramters
       ---------
       capacity: int
           Total size of the array - must be a power of two.
       operation: lambda obj, obj -> obj
           and operation for combining elements (eg. sum, max)
           must form a mathematical group together with the set of
           possible values for array elements (i.e. be associative)
       neutral_element: obj
           neutral element for the operation above. eg. float('-inf')
           for max and 0 for sum.
       """

        assert capacity > 0 and capacity & (capacity - 1) == 0, 'capacity must be positive and a power of 2.'
        self._capacity = capacity
        self._value = np.array([neutral_element for _ in range(2 * capacity)])
        self._operation = operation

    def _reduce_helper(self, start, end, node, node_start, node_end):
        if start == node_start and end == node_end:
            return self._value[node]

        mid = (node_start + node_end) // 2
        if end <= mid:
            return self._reduce_helper(start, end, 2 * node, node_start, mid)
        else:
            if mid + 1 <= start:
                return self._reduce_helper(start, end, 2 * node + 1, mid + 1, node_end)
            else:
                return self._operation(
                    self._reduce_helper(start, mid, 2 * node, node_start, mid),
                    self._reduce_helper(mid + 1, end, 2 * node + 1, mid + 1, node_end)
                )

    def reduce(self, start=0, end=None):
        """Returns result of applying `self.operation`
        to a contiguous subsequence of the array.
            self.operation(arr[start], operation(arr[start+1], operation(... arr[end])))
        Parameters
        ----------
        start: int
            beginning of the subsequence
        end: int
            end of the subsequences
        Returns
        -------
        reduced: obj
            result of reducing self.operation over the specified range of array elements.
        """
        if end is None:
            end = self._capacity
        if end < 0:
            end += self._capacity
        end -= 1
        return self._reduce_helper(start, end, 1, 0, self._capacity - 1)

    def set_batch(self, idxs, vals):
        assert len(idxs) > 0, 'length of idxes should be greater than zero'
        assert len(idxs) == len(vals), f'length of idxes, {len(idxs)}, should be the same as length of vals, {len(vals)}.'

        idxs += self._capacity
        self._value[idxs] = vals
        while idxs[0] > 1:
            idxs = np.unique(idxs//2)
            self._value[idxs] = self._operation(
                self._value[2 * idxs],
                self._value[2 * idxs + 1]
            )

    def __setitem__(self, idx, val):
        # index of the leaf
        # print(f'\nsetting index {idx+self._capacity} to value {val}')
        idx += self._capacity
        self._value[idx] = val
        idx //= 2
        while idx >= 1:
            # print(f'updating parent at index {idx} with the sum of values at {2 * idx} and {2 * idx+1}')
            self._value[idx] = self._operation(
                self._value[2 * idx],
                self._value[2 * idx + 1]
            )
            idx //= 2
        # print(self._value)
        # print([i*1.0 for i in range(len(self._value))])

    def __getitem__(self, idx):
        assert 0 <= idx < self._capacity
        return self._value[self._capacity + idx]


class SumSegmentTree(SegmentTree):
    def __init__(self, capacity):
        super(SumSegmentTree, self).__init__(
            capacity=capacity,
            operation=operator.add,
            neutral_element=0.0
        )

    def sum(self, start=0, end=None):
        """Returns arr[start] + ... + arr[end]"""
        return super(SumSegmentTree, self).reduce(start, end)

    def find_prefixsum_idx(self, prefixsum):
        """Find the highest index `i` in the array such that
            sum(arr[0] + arr[1] + ... + arr[i - i]) <= prefixsum
        if array values are probabilities, this function
        allows to sample indexes according to the discrete
        probability efficiently.
        Parameters
        ----------
        perfixsum: float
            upperbound on the sum of array prefix
        Returns
        -------
        idx: int
            highest index satisfying the prefixsum constraint
        """
        assert 0 <= prefixsum < self.sum()
        idx = 1
        while idx < self._capacity:  # while non-leaf
            if self._value[2 * idx] > prefixsum:
                idx = 2 * idx
            else:
                prefixsum -= self._value[2 * idx]
                idx = 2 * idx + 1
        return idx - self._capacity


class MinSegmentTree(SegmentTree):
    def __init__(self, capacity):
        super(MinSegmentTree, self).__init__(
            capacity=capacity,
            operation=np.minimum,
            neutral_element=float('inf')
        )

    def min(self, start=0, end=None):
        """Returns min(arr[start], ...,  arr[end])"""

        return super(MinSegmentTree, self).reduce(start, end)


if __name__ == '__main__':
    import numpy as np
    import random
    from ttictoc import Timer

    my_list = np.arange(8)
    # with Timer():
    #     my_tree = MinSegmentTree(capacity=len(my_list))
    #     for i in my_list:
    #         my_tree[i] = i
    #     print('Min', my_tree.min())
    #     print(my_tree._value)

    with Timer():
        my_tree = SumSegmentTree(capacity=len(my_list))
        my_tree.set_batch(np.arange(len(my_list)), my_list)
        my_tree.set_batch(np.arange(2,6), np.ones((4))*15)
        my_tree.set_batch(np.array([3]), np.array([10]))
        # print(my_tree._value)
        # print(my_tree.find_prefixsum_idx(my_tree.sum()-0.1))
    # print(my_tree.sum())
    # print('\nprefixsum')
    #
    # prefix_list = np.zeros((8,))
    # # for i in range(10000000):
    # #     prefix_list[my_tree.find_prefixsum_idx(random.random()*28)] +=1
    # # print(np.random.rand(0))
    # # print((prefix_list/10000000)*28)
    # print(my_tree.find_prefixsum_idx(2))